var express = require('express');
var router = express.Router();
const sqlite3 = require('sqlite3');
var db = new sqlite3.Database('./DBs/TrainingDatabase');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/signup', function(req, res, next) {
	db.serialize( () => {
		console.log(req.body);
		//db.run('delete from users');
		db.run('CREATE table if not exists '
		+ 'users ('
		+ 'username varchar(10) UNIQUE, '
		+ 'email varchar(10) UNIQUE, '
		+ 'verified INTEGER DEFAULT 0, '
		+ 'verificationKey INTEGER, '
		+ 'isLogin INTEGER DEFAULT 0, '
		+ 'password varchar(10))',
		(err, result)=>
		{
			if(err){
				console.log(err);
			} else
			{
				var stmt = db.prepare('insert into users values (?,?,?,?,?,?)',
				(err)=>
				{	
					if(err)
					{
						console.log(err);
					}else
					{	
						stmt.run([0, req.body.email, 0, 0, 0, req.body.password ])
					    stmt.finalize(
						(err)=>
						{
							console.log('came');
							if(err){
								console.log(err);
							} else{
								console.log(result);
								res.json(
								{
									msg:'user added',
									showMsg:'new user registered',
									MessageResponse: true
								});
							}
						});
					}
				});
			}
		})
	});
});

router.post('/login', function(req, res, next) {
	console.log(this.body);
	db.get(`SELECT COUNT(*) AS users from users WHERE email=? AND password=?`, [req.body.email,req.body.password],
	(err, row)=>
	{	console.log(req.body.email+req.body.password)
		if(err){
			console.log(err);
		} else if(row.users==1){
			console.log(row);

			res.json(
			{
				Message:'Valid user',
				MessageResponse: true
			});
		} else if(row.users==0){
			console.log(row);
			res.json(
			{
				Message:'Check email or password',
				MessageResponse: false
			});
		}
	})
});

router.post('/check', function(req, res, next) {
	db.get(`SELECT COUNT(*) AS users from users WHERE email=?`, [req.body.email],
	(err, row)=>
	{	
		if(err){
			console.log(err);
		} else if(row.users==1){
			console.log(row);

			res.json(
			{
				Message1:'Valid user',
				Message1Response: true,
				Message2:'email not available',
				Message2Response: false
			});
		} else if(row.users==0){
			console.log(row);
			res.json(
			{
				Message1:'Invalid email',
				Message1Response: false,
				Message2:'email available',
				Message2Response: true
			});
		}
	})
});
router.get('/forgotPassword', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
module.exports = router;
