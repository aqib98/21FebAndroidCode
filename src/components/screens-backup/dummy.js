import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet, Text, View, TouchableHighlight } from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn } from '../../config/auth';
import { getExercises, getExerciseDetails } from '../template/api.js'

export default class Exercises extends Component {

constructor(props) {
    super(props);
    this.state={
      exercises:[],
    }
  }
  componentWillMount(){
    this._getAllExercises(this.props.navigation.state.params.workoutID);
  }
  _getAllExercises=(workoutID)=>{
    getExercises(workoutID).then(response=>{
      this.setState({exercises:response.data},()=>{
        console.log(this.state.exercises)
      });
    },err=>{
      console.log(err)
    })
  }
    /*authenticating starts*/
    _viewExercise = async (id, title) => {
        const { navigate } = this.props.navigation;
        var { userid } = this.props.navigation.state.params;
        navigate("ExerciseDetails", { exerciseID: id, exeTitle: title, userid:userid });
    }
    /*authenticating starts*/

    render() {
        return (
            <View style={styles.container}>

                {this.state.exercises.length?<FlatList style={styles.list} data={this.state.exercises}
                KeyExtractor={(x, i) => i}
                renderItem={({ item }) =>
                <View style={styles.lists}>
                <Text style={styles.leftitem} onPress={() => this._viewExercise(item.id, item.title) } > {item.title} </Text>
                <Text style={styles.rightitem}>
                { item.state == '0' ? 'Not Yet Started' : '' }

                { item.state == '1' ? 'In Progross' : '' }

                { item.state == '2' ? 'Completed' : '' }
                
                </Text>

                </View>                
                }
            />:<Text style={styles.textcenter}>No courses under this student or check your internet</Text>}

            </View>

        );
    }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   justifyContent: 'center',
  },
  list: {
    marginTop:10,
    margin: 1,
    borderColor: '#000',
    borderBottomWidth: 5,
    flex: 1
  },
  lists: {
    flex: 1, 
    flexDirection: 'row',
    borderColor: '#000',
    borderBottomWidth: 1,
    marginLeft: 10,
    width: '94%'
  },
  leftitem: {
    fontSize: 18,
    paddingVertical: 5,
    color: '#000',    
    marginTop: 5,
    textAlign: 'left',
    width: '67%',
    fontWeight: 'bold'
  },
  rightitem: {
    fontSize: 16,
    paddingVertical: 5,
    color: '#000',
    marginLeft: 10,
    marginTop: 5,
    textAlign: 'left',
    width: '34%',
    alignSelf: 'flex-end'
  },  
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  textcenter: {
    justifyContent: 'center',
    textAlign: 'center'
  }  
})

module.exports = Exercises;
