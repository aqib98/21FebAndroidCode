import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  ListView,
  ScrollView,
  AppState,
  TouchableOpacity, Image, ToastAndroid
} from 'react-native';
import Dimensions from 'Dimensions';
import BleManager from 'react-native-ble-manager';
import TimerMixin from 'react-timer-mixin';
import reactMixin from 'react-mixin';
import moment from "moment";
import { getProfileDetails } from '../template/SQLiteOperationsOffline.js';
import { addToTable, addSensorData } from '../template/api.js';
const window = Dimensions.get('window');
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import Video from 'react-native-video';
import { getExercises, getExerciseDetails, getSensorData, sendStatus } from '../template/api.js';
import { addToSQLiteTable } from '../template/SQLiteOperationsOffline.js'; 


const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

export default class BluetoothIntegrate extends Component {
  constructor(){
    super()

    this.state = {
      scanning:false,
      peripherals: new Map(),
      appState: '',
      heartrate: 0,
      screenchange: '0',
      time: moment().format("LTS"),
      date: moment().format("LL"),
      totsec: 2400,
      item: 0,
      array: [],
      sum: 0,
      average: 0,
      calories: 0,
      user_id:null,
      profile:[],
      height:'',
      weight:'',
      timepassed: false,
      rate: 1,
      volume: 1,
      duration: 0.0,
      currentTime: 0.0,
      paused: true,
      loader:false,
      times:0
    }

    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);
    this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(this);
    this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(this);
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
  }
  componentWillMount(){

    var { user_id, calories } = this.props.navigation.state.params;
    this.setState({user_id:user_id})
    this.setState({calories:parseInt(calories)})
    console.log(this.state.calories)
  }


  _getStudentDetails=(id)=>{
    getProfileDetails(id).then(response=>{
      console.log(response)
      if(response.status){
        this.setState({profile:response.data, height:response.data.height_in_feet, weight:response.data.current_weight, age:response.data.age});
        console.log(this.state)
      
      }else{
        ToastAndroid.show(response.show, ToastAndroid.SHORT);
      }
    },err=>{
      console.log(err)
    })
  }




  componentDidMount() {


    AppState.addEventListener('change', this.handleAppStateChange);

    BleManager.start({showAlert: false});

    this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );
    this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan );
    this.handlerDisconnect = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDisconnectedPeripheral );
    this.handlerUpdate = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic );



    if (Platform.OS === 'android' && Platform.Version >= 23) {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
            if (result) {
              console.log("Permission is OK");
            } else {
              PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                  console.log("User accept");
                } else {
                  console.log("User refuse");
                }
              });
            }
      });
    }


  }

  handleAppStateChange(nextAppState) {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
      BleManager.getConnectedPeripherals([]).then((peripheralsArray) => {
        console.log('Connected peripherals: ' + peripheralsArray.length);
      });
    }
    this.setState({appState: nextAppState});
  }




  componentWillUnmount() {
    this.handlerDiscover.remove();
    this.handlerStop.remove();
    this.handlerDisconnect.remove();
    this.handlerUpdate.remove();
  }

  handleDisconnectedPeripheral(data) {
    let peripherals = this.state.peripherals;
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      this.setState({peripherals});
    }
    console.log('Disconnected from ' + data.peripheral);
  }

  handleUpdateValueForCharacteristic(data) {
    //console.log('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, data.value);
    this.setState({heartrate: data.value[1]});

    //var { calories } = this.props.navigation.state.params;

    this.savedata(data.value[1])

  }

  savedata(data) {

    //console.log(calories)

    this.state.array[this.state.item] = data;

    if(this.state.item == 20)
    {  
      this.setState(prevState=>({times:prevState.times+20}))  
    for (var i = 0; i < this.state.array.length; i++) {
        this.state.sum += parseInt(this.state.array[i]);
        console.log('Value is ' + this.state.array.length);  
    }

    if (this.state.array.length > 0) {
        
        this.state.average = Math.round(this.state.sum/this.state.array.length);  

        if(this.state.age == null) { this.state.age = 25 }

        let calories = Math.round((0.4472 * this.state.average - 0.05741 * this.state.weight + 0.074 * this.state.age - 20.4022) * 0.333 / 4.184)

        this.state.calories = calories + this.state.calories;

        console.log(this.state.age)

        console.log(this.state.calories)

        this._storeInDB();
    }

    this.state.item = 0;

    this.state.array = [];

    this.state.sum = 0;

    }
    else {
    
    this.state.item = this.state.item + 1;

    }

  }  

  handleStopScan() {
    console.log('Scan is stopped');
    this.setState({ scanning: false });
  }

  startScan() {
    if (!this.state.scanning) {
      BleManager.scan([], 30, true).then((results) => {
        console.log('Scanning...');
        ToastAndroid.show('Scanning....' , ToastAndroid.SHORT);
        this.setState({scanning:true});
      });
    }
  }

  handleDiscoverPeripheral(peripheral){
    var peripherals = this.state.peripherals;
    if (!peripherals.has(peripheral.id)){
      console.log('Got ble peripheral', peripheral);


      peripherals.set(peripheral.id, peripheral);
      this.setState({ peripherals })
    }
  }

  test(peripheral) {
    //this.setState({loader:true})
    if (peripheral){
      if (peripheral.connected){
        BleManager.disconnect(peripheral.id);
        this.setState({buttontext: 'Start Exercise'})
      }else{
        BleManager.connect(peripheral.id).then(() => {
          let peripherals = this.state.peripherals;
          let p = peripherals.get(peripheral.id);
          if (p) {
            p.connected = true;
            peripherals.set(peripheral.id, p);
            this.setState({peripherals});
          }

          //this.setState({loader:false})
          this._sendStatus('In progress')

          this.setState({deviceinfo: peripheral})

          this.setState({buttontext: 'Pause Exercise'})

          console.log('Connected to ' + peripheral.id);

          this.setState({ screenchange: '1' });

          this.setTimeout(() => {

            /* Test read current RSSI value
            BleManager.retrieveServices(peripheral.id).then((peripheralData) => {
              console.log('Retrieved peripheral services', peripheralData);

              BleManager.readRSSI(peripheral.id).then((rssi) => {
                console.log('Retrieved actual RSSI value', rssi);
              });
            });*/

            // Test using bleno's pizza example
            // https://github.com/sandeepmistry/bleno/tree/master/examples/pizza
            BleManager.retrieveServices(peripheral.id).then((peripheralInfo) => {
              console.log(peripheralInfo);
              var service = '0000180d-0000-1000-8000-00805f9b34fb';
              var bakeCharacteristic = '00002a37-0000-1000-8000-00805f9b34fb';

              this.setTimeout(() => {
                BleManager.startNotification(peripheral.id, service, bakeCharacteristic).then(() => {
                  console.log('Started notification on ' + peripheral.id);
             
                }).catch((error) => {
                  console.log('Notification error', error);
                });
              }, 1000);
            });

          }, 1000);
        }).catch((error) => {
          console.log('Connection error', error);
          ToastAndroid.show('Connection error.... Please Try To Reconnect' , ToastAndroid.SHORT);
        });
      }
    }
  }
  _playpause=()=>{
    this.setState({ paused: !this.state.paused })
  }
  getCurrentTimePercentage() {
      if (this.state.currentTime > 0) {
        return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
      }
      return 0;
    };
  _timer(value)
  {
    console.log('Manikanta')
  }
  _storeInDB(){
    var { user_id, exerciseID, workoutID, courseID, workout_day } = this.props.navigation.state.params;
    var { average, calories, times } = this.state;
    var data={
      "user_id":user_id,
      "avg_heartrate" : average,
      "calories" : calories,
      "exercise_id":exerciseID,
      "course_id": courseID,
      "workout_id": workoutID,
      "workout_day":workout_day
    }
    var toSQL=[this.state.user_id, exerciseID, average, times];
    addToSQLiteTable({table_name:'sensor_data', table_data:toSQL}).then(response=>{
        console.log(response);
      },error=>{
        console.log(error);
      })
    addSensorData(data).then(response=>{
        console.log(response);
      },error=>{
        console.log(error);
      })
    }



    _openMenu = () => {

      peripheral = this.state.deviceinfo

    if (peripheral){
      if (peripheral.connected){
        BleManager.disconnect(peripheral.id);
    }  
  }

    //this.setState({ paused: !this.state.paused })

      var { user_id, exerciseID, exeTitle, status, workoutID, workoutTitle, courseTitle, courseID, course, workout_day } = this.props.navigation.state.params;
      const { navigate  } = this.props.navigation;

      console.log(this.props.navigation.state.params);

      var status = 'In progress'

      navigate("ExerciseDetails",{ exerciseID: exerciseID, exeTitle: exeTitle, user_id:user_id, status: status, workoutID: workoutID, workoutTitle: workoutTitle, courseID: courseID, courseTitle: courseTitle, course: course, workout_day: workout_day });
    }

  video: Video;


  onLoad = (data) => {
    this.setState({ duration: data.duration });
  };

  onProgress = (data) => {
      this.setState({ currentTime: data.currentTime });
  };

  onEnd = () => {
      this.setState({ paused: true })
      this.video.seek(0)
  };

  onAudioBecomingNoisy = () => {
      this.setState({ paused: true })
  };

  onAudioFocusChanged = (event: { hasAudioFocus: boolean }) => {
      this.setState({ paused: !event.hasAudioFocus })
  };


    _sendStatus(statusvalue){

    if(statusvalue == 'completed') { this.setState({loader:true}) }

    var { exerciseID, user_id, workoutID, courseID, status, exeTitle, workoutTitle, courseTitle, workout_day, course  } = this.props.navigation.state.params;
      let temp={
        "user_id":user_id,
        "exercise_id" : exerciseID,
        "workout_id" :workoutID,
        "course_id":courseID,
        "workout_day":workout_day,
        "exercise_status" : statusvalue
      };
      sendStatus(temp).then(res=>{
        console.log(res);
       
       if(statusvalue == 'completed')
        {
         const { navigate  } = this.props.navigation;

          this.setState({loader:false})

     peripheral = this.state.deviceinfo;

    if (peripheral){
      if (peripheral.connected){
        BleManager.disconnect(peripheral.id);
        console.log('disconnect')
    }      
  }

  //this.setState({ paused: !this.state.paused })
          
       navigate("ExerciseDetails",{ exerciseID: exerciseID, exeTitle: exeTitle, workout_day: workout_day, user_id:user_id, status: statusvalue, workoutID: workoutID, workoutTitle: workoutTitle, courseID: courseID, course: course, courseTitle: courseTitle });

        }      
      },
      err=>{
        console.log(err);
      })
    }


  render() {
    const list = Array.from(this.state.peripherals.values());
    const dataSource = ds.cloneWithRows(list);
    const flexCompleted = this.getCurrentTimePercentage() * 100;
    const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;
    let source = this.state.paused === true ? require( '../../Images/play.png' ) : require( '../../Images/pause.png' );

    setTimeout(() => {this.setState({timePassed: true})}, 2000)

    var { videoUrl } = this.props.navigation.state.params;
      
    let url = 'https://vjs.zencdn.net/v/oceans.mp4';
    

    if(this.state.screenchange == '0')  
    {  

    return (
      <View style={styles.container}>

            <View style={styles.header}>

              <View style={styles.menupartleft}>

                  <TouchableOpacity onPress={() => this._openMenu() } >
                    <Text>
                    <Image style={styles.avatar} source={require('./../../Images/1280_back_arrow.png')}/>
                    </Text>
                  </TouchableOpacity>

              </View>
              <View style={styles.menupartright}>

              <Text style={styles.title}>{this.props.navigation.state.params.exeTitle}</Text>

              </View>

            </View>   


        <TouchableHighlight style={{marginTop: 40,margin: 20, padding:20, backgroundColor:'#ccc'}} onPress={() => this.startScan() }>
          <Text>Scan Bluetooth ({this.state.scanning ? 'on' : 'off'})</Text>
        </TouchableHighlight>
        <ScrollView style={styles.scroll}>
          {(list.length == 0) &&
            <View style={{flex:1, margin: 20}}>
              <Text style={{textAlign: 'center'}}>No Devices</Text>
            </View>
          }
          <ListView
            enableEmptySections={true}
            dataSource={dataSource}
            renderRow={(item) => {
              const color = item.connected ? 'green' : '#333333';
              return (
                <TouchableHighlight onPress={() => this.test(item) }>
                  <View style={[styles.row, {backgroundColor: color}]}>
                    <Text style={{fontSize: 12, textAlign: 'center', color: 'white', padding: 10}}>{item.name}</Text>
                    <Text style={{fontSize: 8, textAlign: 'center', color: 'white', padding: 10}}>{item.id}</Text>
                  </View>
                </TouchableHighlight>
              );
            }}
          />

        </ScrollView>
      </View>
    );
    }
    else
    {

    return (
      <View style={styles.container}>

            <View style={styles.header}>

              <View style={styles.menupartleft}>

                  <TouchableOpacity onPress={() => this._openMenu() } >
                    <Text>
                    <Image style={styles.avatar} source={require('./../../Images/1280_back_arrow.png')}/>
                    </Text>
                  </TouchableOpacity>

              </View>
              <View style={styles.menupartright}>

              <Text style={styles.title}>{this.props.navigation.state.params.exeTitle}</Text>

              </View>

            </View>    


        <ScrollView style={styles.scroll}>



          <View style={styles.heartrate1}>
            <Text style={styles.heartrate1text}>{this.state.heartrate}</Text>
            <Text>Heart Rate</Text>
          </View>

          <View style={styles.avgrateblock}>

            <View style={styles.heartrate2}>
            <Text style={styles.heartrate2text}>{this.state.average}</Text>
            <Text style={styles.commenttext}>Avg. Heart Rate</Text>
            </View>
            <View style={styles.heartrate3}>
            <Text style={styles.heartrate2text}>{this.state.calories}</Text>
            <Text style={styles.commenttext}>Calories Burned</Text>
            </View>

          </View>  


 
            {videoUrl?
            <View style={styles.video}>

              <TouchableOpacity onPress={() => this._playpause(this.state.play)}>
                <Video
                  ref={(ref: Video) => { this.video = ref }}
                  source={{ uri: videoUrl }}/*this.state.exercise.streaming_uri*/
                  style={styles.fullScreen}
                  rate={this.state.rate}
                  paused={this.state.paused}
                  volume={this.state.volume}
                  muted={this.state.muted}
                  resizeMode = "stretch"
                  onLoad={this.onLoad}
                  onProgress={this.onProgress}
                  onEnd={this.onEnd}
                  onAudioBecomingNoisy={this.onAudioBecomingNoisy}
                  onAudioFocusChanged={this.onAudioFocusChanged}
                  repeat={false}
                />
              </TouchableOpacity>

                <View style={styles.controls}>
                  <View style={styles.trackingControls}>
                    <View style={styles.progress}>
                      <View style={[styles.innerProgressCompleted, { flex: flexCompleted }]} />
                      <View style={[styles.innerProgressRemaining, { flex: flexRemaining }]} />
                  </View>
                  <View style={styles.botttomcontrols}>
                    <TouchableHighlight style={styles.playpause} onPress={() => this._playpause(this.state.play)}>
                      <Image style={styles.playpauseimage}  source={ source } />
                    </TouchableHighlight>
                    <View style={styles.duration}>
                      <Text style={styles.controlercolor}>{Math.round(this.state.currentTime)} / {Math.round(this.state.duration)}</Text>
                    </View>
                  </View>

                  </View>
                </View>              

            </View>:<View style={styles.video}><Text style={{textAlign:'center',marginTop: 20,color: 'white'}}>No Video Avaliable</Text></View>}
                  
                        <Button
                          buttonStyle={{marginTop:20}}
                          title={this.state.buttontext}
                          fontSize={20}
                          
                          disabledStyle={{backgroundColor:'#ef7e2d'}}
                          
                          backgroundColor="#ef7e2d"
                          width="100"
                          onPress={() => this.test(this.state.deviceinfo) }
                        />

                                                          
                        <Button
                          buttonStyle={{marginTop:20}}
                          title="Complete Exercise"
                          fontSize={20}
                          disabled={this.state.loader}
                          disabledStyle={{backgroundColor:'#ef7e2d'}}
                          loading={this.state.loader}
                          backgroundColor="#ef7e2d"
                          width="100"
                          onPress={() => this._sendStatus('completed') }
                        />

        </ScrollView>        
      </View>
    );      
    }
  }
}
reactMixin(BluetoothIntegrate.prototype, TimerMixin);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    width: window.width,
    height: window.height
  },
  scroll: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    margin: 10,
  },
  row: {
    margin: 10
  },
  hearbeat: {
    fontSize: 24,
    alignSelf: 'center'
  },
  heartrate1: {
    alignSelf: 'center',
    marginTop: 20,
    borderBottomWidth: 2
  },
  heartrate1text: {
    textAlign: 'center',
    fontSize: 50,
    color: '#ef7e2d'    
  },
  avgrateblock: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center'
  },
  heartrate2: {
    width: '50%',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  heartrate3: {
    width: '50%',
    alignSelf: 'flex-end',
  },
  heartrate2text: {
    textAlign: 'center',
    fontSize: 50,
    color: '#ef7e2d',
    alignSelf: 'center'    
  },
  heartrate3text: {
    textAlign: 'center',
    fontSize: 50,
    color: '#ef7e2d',
    alignSelf: 'center'  
  },
  commenttext: {
    textAlign: 'center',
  },
  timeText: {
    color: '#999999',
    fontSize: 20,
  },
  dateText: {
    color: '#999999',
    fontSize: 20,
  } ,
  scroll: {
    flex: 1
  },
  firsttext: {
    fontSize: 20,
    fontWeight:'bold'
  },
  innertext: {
    marginTop: 0,
    width: '50%'
  },
  body: {
    flex : 1,
    justifyContent: 'center'
  },
  list: {
    margin: 1,
    flex: 1,   
  },
  lists: {
    flex: 1, 
    flexDirection: 'row',
    borderColor: 'red',
    borderBottomWidth: 2,
    shadowOffset: { width: 100, height: 100 },
    shadowColor: '#000',
    shadowOpacity: 10,
    shadowRadius: 10
  },
  lowertext: {
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    margin: 4
  },
  length: {
    width: '65%',
    alignItems: 'flex-start',
    
  },
  leftitem: {
    fontSize: 22,
    color: '#000',    
    textAlign: 'left',
    width: '67%'
  },
  rightitem: {
    fontSize: 16,
    paddingVertical: 5,
    color: '#000',
    marginLeft: 10,
    textAlign: 'left',
    width: '34%',
    alignSelf: 'flex-end'
  },   
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  header: {
    height: 60,
    backgroundColor: '#262626',
    width: '100%',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOpacity: 0.2 
  },
  progressContainer: {
    alignItems: "flex-end", 
    width: '48%',
    marginTop: -5,
  },  
  avatar: {
    height: 40,
    width: 60,
    marginLeft: 35,
    marginTop: 10
  },
  title: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: -10,
    textAlign: 'left'

  },
  menupartleft: {
    width: '13%',
    marginLeft: 15,
    marginTop: 20
  },
  menupartright: {
    width: '87%',
    marginTop: 24,
  },
  textcenter: {
    justifyContent: 'center',
    textAlign: 'center'
  },
  container: {
    flex: 1,
   justifyContent: 'center',
  },
  scroll: {
    flex: 1,
    height: 400,
  },
  firsttext: {
    fontSize: 20
  },
  innertext: {
    marginTop: 0,
    width: '50%'
  },
  body: {
    flex : 1,
    justifyContent: 'center'
  },
  fullScreen: {
    width: '100%',
    height: '100%',
  },
  tabcontent: {
    margin: 8,
    flex: 1,
    flexDirection: 'row', 
    width: '100%',
    alignSelf: 'center'  
  },
  leftcontent: {
    width: '50%',
    backgroundColor: 'red'
  },
  rightcontent: {
    width: '46%',
    backgroundColor: 'yellow'
  },
  avgrateblock: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center',
    margin: 10
  },
  heartrate2: {
    width: '50%',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  heartrate3: {
    width: '50%',
    alignSelf: 'flex-end',
  },
  heartrate4: {
    width: '50%',
    alignSelf: 'flex-end',
    borderColor: '#000',
    backgroundColor: 'lightyellow',
    borderBottomWidth: 2    
  },  

 
  heartrate4text: {
    textAlign: 'right',
    fontSize: 30,
    color: '#ef7e2d',
    marginRight: 10
  },  
  video: {
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: 250,
    width: '100%',
    backgroundColor: '#000',
    marginTop: 20
  },
  thirdblock: {
    flex: 1,
    height: 220
  },
  firstlock: {
  },  
  list: {
    margin: 1,
    flex: 1,
  },
  lists: {
    flex: 1, 
    flexDirection: 'row',
    borderColor: '#000',
    borderBottomWidth: 1,
  },
  innerlist: {
    margin: 8,
    marginLeft: 10,
  },
  lowertext: {
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    margin: 4
  },
  length: {
    width: '65%',
    height: 10,
    alignItems: 'flex-start',
    
  },
  leftitem: {
    fontSize: 22,
    color: '#000',    
    textAlign: 'left',
    width: '67%'
  },
  rightitem: {
    fontSize: 16,
    paddingVertical: 5,
    color: '#000',
    marginLeft: 10,
    textAlign: 'left',
    width: '34%',
    alignSelf: 'flex-end'
  },   
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  header: {
    height: 60,
    backgroundColor: '#262626',
    width: '100%',
    flexDirection: 'row',
    shadowOffset: { width: 10, height: 10 },
    shadowColor: '#000',
    shadowOpacity: 1,
    elevation: 3
  },
  progressContainer: {
    alignItems: "flex-end", 
    width: '48%',
    height: 10,
    marginTop: 5,
  },  
  avatar: {
    height: 40,
    width: 60,
    marginLeft: 35,
    marginTop: 10
  },
  title: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: -10

  },
  menupartleft: {
    width: '13%',
    marginLeft: 15,
    marginTop: 20
  },
  menupartright: {
    width: '87%',
    marginTop: 24,
  },
  textcenter: {
    justifyContent: 'center',
    textAlign: 'center'
  },
  controls: {
    backgroundColor: "transparent",
    borderRadius: 5,
    position: 'absolute',
    bottom: 10,
    left: 4,
    right: 4,
    marginTop: 15
  },
  progress: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 3,
    overflow: 'hidden',
  },
  controlercolor: {
    color: '#fff',
    fontSize: 20
  },
  innerProgressCompleted: {
    height: 5,
    backgroundColor: '#cccccc',
  },
  innerProgressRemaining: {
    height: 5,
    backgroundColor: '#2C2C2C',
  },
  generalControls: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 4,
    overflow: 'hidden',
    paddingBottom: 10,
  },
  rateControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  volumeControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  resizeModeControl: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  controlOption: {
    alignSelf: 'center',
    fontSize: 11,
    color: 'white',
    paddingLeft: 2,
    paddingRight: 2,
    lineHeight: 12,
  },
  playpause: {
    flex: 1,
  },
  duration: {
    justifyContent: 'flex-end',
  },
  botttomcontrols: {
    marginTop:10,
    flex: 1,
    flexDirection: 'row',
    width: '100%'
  },
  playpauseimage: {
    width: 20,
    height: 20
  }         
});