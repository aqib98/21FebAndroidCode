import React, { Component } from "react";
import { View, ToastAndroid, Image, StyleSheet, Text} from "react-native";
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn,onSignOut } from '../../config/auth';
import { getSensorData } from '../template/SQLiteOperationsOffline.js'
import { AsyncStorage } from "react-native";

export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      ecercise_id:null,
      sensor_data:null
    };
  }

  componentWillMount(){
    const { exercise_id } = this.props.navigation.state.params;
    this.setState({exercise_id:exercise_id})
    this._getSensor(exercise_id);
  }

/*get sensor data starts*/
  _getSensor(exercise_id){
    getSensorData(exercise_id)
    .then(response=>{
      console.log("response", response);
      this.setState({sensor_data:response.data})
    }, reject=>{
      console.log("reject", reject);
      
    })
    .catch(err=>{
      console.log("error", err);
    })
  }
/*getting sendor data endss*/

  render(){
    const { navigate } = this.props.navigation;
    return(
      <View>
      </View>

    );
  }
}


const styles = StyleSheet.create({

  backgroundImage: {
    alignSelf: 'stretch',
    width: '100%',
    height: '100%'
  },
  loginbox: {
    position: 'absolute',
    bottom:100,
    left:0,
  }


});
