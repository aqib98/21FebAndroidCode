import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet,TextInput, Picker, ScrollView, TouchableOpacity, Text, Image, View, TouchableHighlight, ToastAndroid } from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn } from '../../config/auth';
import { getProfileDetails } from '../template/SQLiteOperationsOffline.js';
import { updateProfile } from '../template/api.js';
import { AsyncStorage } from "react-native";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
export default class Profile extends Component {

    constructor(props) {
      super(props);
      this.state={
        profile:[],
        height:null,
        weight:null,
        age:null,
        gender:'Male',
        userid:null,
        genderIndex:null,
        enable:null,
        timePassed: false
      }
  }
  componentWillMount(){
    const { navigate  } = this.props.navigation;
    this.getStudentID();
    
  }
  async getStudentID () {
  console.log("ff") 
    let USER_ID = await AsyncStorage.getItem("USER_ID"); 
    this.setState({userid:USER_ID,enable:false},()=>{
      this._getStudentDetails(this.state.userid);
    });
  }
  _getStudentDetails=(id)=>{
    getProfileDetails(id).then(response=>{
      console.log(response)
      if(response.status){
        this.setState({profile:response.data},()=>{
          this.setState({height:this.state.profile.height_in_feet, age:this.state.profile.age, weight:this.state.profile.current_weight,gender:this.state.profile.gender},()=>{
            if(this.state.gender==="Male"){
              this.setState({genderIndex:0});
            }else if(this.state.gender==="Female"){
              this.setState({genderIndex:1});
            }else{
              this.setState({genderIndex:null});
            }
          });
        });
        console.log(this.state)
      }else{
        ToastAndroid.show(response.show, ToastAndroid.SHORT);
      }
    },err=>{
      console.log(err)
    })
  }
  _savedata=()=>{
    console.log(this.state)
    if(this.state.enable&&this.state.weight!=""&&this.state.height!=""){
      updateProfile({weight:this.state.weight, age:this.state.age, height:this.state.height, gender:this.state.gender, id:this.state.userid})
      .then(res=>{
        console.log(res);
        if(res.status){
          this.getStudentID();
          ToastAndroid.show(res.show, ToastAndroid.SHORT); 
        }else{
         ToastAndroid.show(res.show, ToastAndroid.SHORT);
        }
      },err=>{
        console.log(err);
        ToastAndroid.show(err.show, ToastAndroid.SHORT);
      })  
    }else{
      ToastAndroid.show("Edit anything", ToastAndroid.SHORT);
    }
  }
  onSelect(index, value){
    console.log(index)
    if(!index){
      this.setState({enable:true,gender:"Male"})
    }else{
      this.setState({enable:true, gender:"Female"})
    }
  }

  openMenu = async() => {

    const { navigate  } = this.props.navigation;
    navigate("Menu");

  }

    render() {
      var radio_props = [
        {label: 'Male', value: 0 },
        {label: 'Female', value: 1 }
      ];

      if(this.state.weight == null)
      {
        var weight = '0';
      }
      else
      {
        var weight = this.state.weight;
      }

      if(this.state.height == null)
      {
        var height = '0';
      }
      else
      {
        var height = this.state.height;
      }

      if(this.state.age == null)
      {
        var age = '0';
      }
      else
      {
        var age = this.state.age;
      }


      setTimeout(() => {this.setState({timePassed: true})}, 2000)

      return (

              <View style={styles.container}>

                <View style={styles.header}>

                  <View style={styles.menupartleft}>

                      <TouchableOpacity onPress={() => this.openMenu() } >
                        <Text>
                        <Image style={styles.avatar} source={require('./../../Images/1280_back_arrow.png')}/>
                        </Text>
                      </TouchableOpacity>

                  </View>
                  <View style={styles.menupartright}>

                  <Text style={styles.title}>Edit Profile</Text>

                  </View>

                </View>

        {this.state.timePassed == false ? <Image style={{alignSelf:'center',width: 100, height: 100,marginTop: 20, top:0}} source={require('./../../Images/loading.gif')} />:          

        <ScrollView style={styles.scroll}>

          <View >
          </View>

          <View style={{height:600}}>
          
            <FormLabel>Name</FormLabel>
            <FormInput
              value={this.state.profile.first_name}
              editable={false} selectTextOnFocus={false}
              style={styles.textInput}
              readonly
            />

            <FormLabel>Email</FormLabel>
            <FormInput
              value={this.state.profile.email}
              editable={false} selectTextOnFocus={false}
              style={styles.textInput}
              readonly
            />
            <FormLabel>Weight</FormLabel>
            <FormInput
              placeholder="Weight..."
              value={2}
              style={styles.textInput}
              keyboardType = 'numeric'
              onChangeText = {(text)=> this.setState({weight:text, enable:true})}
              value = {String(weight)}
              maxLength = {3}
            />

            <FormLabel>Height</FormLabel>
            <FormInput
              placeholder="Height..."
              value={2}
              style={styles.textInput}
              keyboardType = 'numeric'
              onChangeText = {(text)=> this.setState({height:text, enable:true})}
              value = {String(height)}
              maxLength = {3}
            />

            <FormLabel>Age</FormLabel>
            <FormInput
              placeholder="Age..."
              value={2}
              style={styles.textInput}
              keyboardType = 'numeric'
              onChangeText = {(text)=> this.setState({age:text, enable:true})}
              value = {String(age)}
              maxLength = {3}
            />


            <FormLabel>Gender</FormLabel>
            <RadioGroup
              selectedIndex={this.state.genderIndex}
              style={styles.radioGroup}
              onSelect = {(index, value) => this.onSelect(index, value)}
            >
              <RadioButton value={'Male'} style={styles.radioButton}>
                <Text>Male</Text>
              </RadioButton>

              <RadioButton value={'Female'} style={styles.radioButton}>
                <Text>Female</Text>
              </RadioButton>
            </RadioGroup>
            <Button
              buttonStyle={{ marginTop: 20 }}
              backgroundColor="#03A9F4"
              title="UPDATE"
              onPress={() => this._savedata() }
            />

           </View> 

           </ScrollView>

         }  

        </View>


      );
    }
}

const styles = StyleSheet.create({
  container: {
   justifyContent: 'center',
  },
  item: {
    fontSize: 20,
    paddingVertical: 5,
    color: '#000',
    borderColor: '#000',
    borderBottomWidth: 1,
    marginBottom:2,
  },
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  radioGroup:{
    flexDirection:'row',
    marginLeft: 10
  },
  profilepic:{
    height: 200,
    width: '100%'
  }, 
  title: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: -10,
    textAlign: 'left'

  },
  header: {
    height: 60,
    backgroundColor: '#262626',
    width: '100%',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOpacity: 0.2 
  },
  progressContainer: {
    alignItems: "flex-end", 
    width: '48%',
    marginTop: -5,
  },  
  avatar: {
    height: 40,
    width: 60,
    marginLeft: 35,
    marginTop: 10
  },
  title: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: -10,
  },
  menupartleft: {
    width: '15%',
    marginLeft: 15,
    marginTop: 20
  },
  menupartright: {
    width: '85%',
    marginTop: 24,
  },
  textcenter: {
    justifyContent: 'center',
    textAlign: 'center'
  }
})

module.exports = Profile;
