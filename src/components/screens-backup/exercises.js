import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet,ScrollView, Text, View, TouchableHighlight, Animated ,TouchableOpacity, Image, ToastAndroid } from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn } from '../../config/auth';
import { getExercises, getExerciseDetails } from '../template/api.js'


class ProgressBar extends Component {
  
  componentWillMount() {
    this.animation = new Animated.Value(this.props.progress);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.progress !== this.props.progress) {
      Animated.timing(this.animation, {
        toValue: this.props.progress,
        duration: this.props.duration
      }).start();
    }
  }
  
  
  render() {
    const {
      height,
      borderColor,
      borderWidth,
      borderRadius,
      barColor,
      fillColor,
      row
    } = this.props;

    const widthInterpolated = this.animation.interpolate({
      inputRange: [0, 1],
      outputRange: ["0%", "100%"],
      extrapolate: "clamp"
    })

    return (
      <View style={[{flexDirection: "row", height }, row ? { flex: 1} : undefined ]}>
        <View style={{ flex: 1, borderColor, borderWidth, borderRadius}}>
          <View
            style={[StyleSheet.absoluteFill, { backgroundColor: fillColor }]}
          />
          <Animated.View
            style={{
              position: "absolute",
              left: 0,
              top: 0,
              bottom: 0,
              width: widthInterpolated,
              backgroundColor: barColor
            }}
          />
        </View>
      </View>
    )
  }
}  

ProgressBar.defaultProps = {
  height: 6,
  borderWidth: 0,
  borderRadius: 0,
  barColor: "#3bafda",
  fillColor: "#d8d8d8",
  duration: 70
}




export default class Exercises extends Component {

constructor(props) {
    super(props);
    this.state={
      exercises:[],
      userid:'',
      timePassed: false,
      loader:null
    }
  }
  componentWillMount(){
    var { workoutID, user_id, workout_day } = this.props.navigation.state.params;
    this.setState({userid:user_id})
    this._getAllExercises(workoutID, user_id, workout_day);
    console.log(this.props.navigation.state.params)
  }
  _getAllExercises=(workoutID, user_id, workout_day)=>{
    this.setState({loader:true})
    getExercises(workoutID, user_id, workout_day).then(response=>{
      this.setState({exercises:response.data},()=>{
        console.log(this.state.exercises.length)

          for(let j=0;j<this.state.exercises.length;j++)
          {

            if(j%2==0)
            {
              this.state.exercises[j].color = '#FFF';
            }
            else
            {
              this.state.exercises[j].color = '#f8f8f8';
            }

          }

          this.setState(this.state.exercises);
          this.setState({loader:false})

      });
    },err=>{
      console.log(err)
    })
  }
    /*authenticating starts*/
    _viewExercisedetails = async (id, title, status) => {
      console.log('status',status)
        const { navigate } = this.props.navigation;
        var { user_id, workoutID, courseTitle, courseID, workoutTitle, workout_day, course } = this.props.navigation.state.params;
        console.log(this.props.navigation.state.params)
        
        navigate("ExerciseDetails", { exerciseID: id, exeTitle: title, status:status, user_id:user_id, workoutID: workoutID, workoutTitle: workoutTitle, courseID: courseID, courseTitle: courseTitle, course: course, workout_day:workout_day });
    }
    /*authenticating starts*/

  openMenu = async() => {

    const { navigate  } = this.props.navigation;
    var { user_id, courseTitle, courseID, course } = this.props.navigation.state.params;

    console.log(this.props.navigation.state.params)

    navigate("Workouts", { courseID: courseID, courseTitle: courseTitle, user_id: user_id, course: course });;

  }



    render() {

      setTimeout(() => {this.setState({timePassed: true})}, 2000)

        return (

          <View style={styles.container}>

            <View style={styles.header}>

              <View style={styles.menupartleft}>

                  <TouchableOpacity onPress={() => this.openMenu() } >
                    <Text>
                    <Image style={styles.avatar} source={require('./../../Images/1280_back_arrow.png')}/>
                    </Text>
                  </TouchableOpacity>

              </View>
              <View style={styles.menupartright}>

              <Text style={styles.title}>{this.props.navigation.state.params.workoutTitle}</Text>

              </View>

            </View>


            <View style={styles.body}>


            <ScrollView style={styles.scroll}>

            {this.state.loader? <Image style={{flex:1,alignSelf:'center',width: 100, height: 100,marginTop: 50}} source={require('./../../Images/loading.gif')} />:this.state.exercises.length?<FlatList style={styles.list} data={this.state.exercises}
            KeyExtractor={(x, i) => i}
            renderItem={({ item, j }) =>
            <View style={styles.lists} style={{backgroundColor:item.color,flex: 1,flexDirection: 'row',borderColor: '#f5f4f4',borderBottomWidth:1,borderTopWidth: 1,shadowOffset: { width: 100, height: 100 },shadowColor: '#000',shadowOpacity: 10,shadowRadius: 10}}>

            <View style={styles.innerlist} style={{margin: 12,marginLeft: 10}}>            
              
              <TouchableOpacity  onPress={() => this._viewExercisedetails(item.id, item.title, item.exercise_status) }>

              <Text style={styles.firsttext}> {item.title.length<20?item.title:item.title.substring(0,20)+'...'}   </Text>

              <View style={styles.lowertext}>

                <View style={styles.innertext}> 

                  <Text>

                  { item.exercise_status == '0' ? ' Not Yet Started ' : item.exercise_status === 'completed' ? ' Completed ' : ' In Progress ' }

                </Text> 

                </View>

                <View style={styles.progressContainer}>

                  <View style={styles.length}>

                    <ProgressBar
                      row
                      progress={item.exercise_status == '0' ? 0 : item.exercise_status === 'completed' ? 1 : 0.5}
                    />


                    <Text style={{alignSelf: 'flex-end'}}>{item.exercise_status == '0' ? 0 : item.exercise_status === 'completed' ? 100 : 50}%</Text>  
                  </View>  
                </View>     

               </View> 

              </TouchableOpacity> 

             </View> 
                        
            </View>
            }
            />:<View style={{flex: 1,margin: 10, marginTop: 30}}><Text style={{fontSize: 28, textAlign: 'center'}}>No Exercises Under This Workout or check your internet connection</Text></View>}

            </ScrollView>

            </View>

          </View>  

        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   justifyContent: 'center',
  },
  scroll: {
    flex: 1
  },
  firsttext: {
    fontSize: 20,
    fontWeight:'bold'
  },
  innertext: {
    marginTop: 0,
    width: '50%'
  },
  body: {
    flex : 1,
    justifyContent: 'center'
  },
  list: {
    margin: 1,
    flex: 1,   
  },
  lists: {
    flex: 1, 
    flexDirection: 'row',
    borderColor: 'red',
    borderBottomWidth: 2,
    shadowOffset: { width: 100, height: 100 },
    shadowColor: '#000',
    shadowOpacity: 10,
    shadowRadius: 10
  },
  lowertext: {
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    margin: 4
  },
  length: {
    width: '65%',
    alignItems: 'flex-start',
    
  },
  leftitem: {
    fontSize: 22,
    color: '#000',    
    textAlign: 'left',
    width: '67%'
  },
  rightitem: {
    fontSize: 16,
    paddingVertical: 5,
    color: '#000',
    marginLeft: 10,
    textAlign: 'left',
    width: '34%',
    alignSelf: 'flex-end'
  },   
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  header: {
    height: 60,
    backgroundColor: '#262626',
    width: '100%',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOpacity: 0.2 
  },
  progressContainer: {
    alignItems: "flex-end", 
    width: '48%',
    marginTop: -5,
  },  
  avatar: {
    height: 40,
    width: 60,
    marginLeft: 35,
    marginTop: 10
  },
  title: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: -10,
    textAlign: 'left'

  },
  menupartleft: {
    width: '13%',
    marginLeft: 15,
    marginTop: 20
  },
  menupartright: {
    width: '87%',
    marginTop: 24,
  },
  textcenter: {
    justifyContent: 'center',
    textAlign: 'center'
  }
})


module.exports = Exercises;
