import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet,TextInput, Picker, ScrollView, TouchableOpacity, Text, Image, View, TouchableHighlight, ToastAndroid } from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn } from '../../config/auth';
import { getProfileDetails } from '../template/SQLiteOperationsOffline.js';
import { updateProfile } from '../template/api.js';
import { AsyncStorage } from "react-native";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
export default class Instructer extends Component {

    constructor(props) {
      super(props);
      this.state={
        profile:[],
        name: null,
        email:null,
        message:'',
        timePassed: false
      }
  }
  componentWillMount(){
    const { navigate  } = this.props.navigation;
    var { id } = this.props.navigation.state.params;
    console.log(id)
    this._getProfileDetails(id);
    
  }
  _getProfileDetails=(id)=>{
    getProfileDetails(id).then(response=>{
      console.log(response)
      if(response.status){
        this.setState({name:response.data.first_name+" "+response.data.last_name, email:response.data.email})
      }else{
        ToastAndroid.show(response.show, ToastAndroid.SHORT);
      }
    },err=>{
      console.log(err)
    })
  }
  _sendMessageToInstructor=()=>{
    if(this.state.message === ''){
      ToastAndroid.show('Cannot send empty message', ToastAndroid.SHORT);
    }else{
      ToastAndroid.show('message Sent', ToastAndroid.SHORT);
      this.setState({message:''})
    }

  }

       _moveCourse = async () => {
           const { navigate  } = this.props.navigation;
           navigate("Courses");
       }


    render() {
      const { name, email } = this.state;

      setTimeout(() => {this.setState({timePassed: true})}, 2000)

      return (

        <View style={styles.container}>

                <View style={styles.header}>

                  <View style={styles.menupartleft}>

                      <TouchableOpacity onPress={() => this._moveCourse() } >
                        <Text>
                        <Image style={styles.avatar} source={require('./../../Images/1280_back_arrow.png')}/>
                        </Text>
                      </TouchableOpacity>

                  </View>
                  <View style={styles.menupartright}>

                  <Text style={styles.title}>Send Message To Instructer</Text>

                  </View>

                </View>

                  

        <ScrollView style={styles.scroll}>

  

          <View style={{height:300}}>

          <Text style={{fontSize: 30, margin: 10, fontWeight: 'bold'}}>Name : {name}</Text>
          
          <Text style={{fontSize: 30, margin: 10, fontWeight: 'bold'}}>Email : {email}</Text>




            <FormInput
              placeholder="Send your message..."
              value={2}
              style={styles.textInput}
              multiline={true}
              onChangeText = {(text)=> this.setState({message:text})}
            />



            <Button
              buttonStyle={{ marginTop: 20 }}
              backgroundColor="#03A9F4"
              title="UPDATE"
              onPress={() => this._sendMessageToInstructor() }
            />

           </View> 

           </ScrollView>

        </View>


      );
    }
}

const styles = StyleSheet.create({
  container: {
   justifyContent: 'center',
  },
  item: {
    fontSize: 20,
    paddingVertical: 5,
    color: '#000',
    borderColor: '#000',
    
    marginBottom:2,
  },
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  radioGroup:{
    flexDirection:'row',
    marginLeft: 10
  },
  profilepic:{
    height: 200,
    width: '100%'
  },   
  title: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: 30,
    textAlign: 'left'

  },
  menupartleft: {
    width: '30%',
    marginLeft: 15,
    marginTop: 15
  },
  menupartright: {
    width: '70%',
    marginTop: 24,
    marginLeft: 10
  },
  header: {
    height: 60,
    backgroundColor: '#262626',
    width: '100%',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOpacity: 0.2 
  },
  progressContainer: {
    alignItems: "flex-end", 
    width: '48%',
    marginTop: -5,
  },  
  avatar: {
    height: 40,
    width: 60,
    marginLeft: 35,
    marginTop: 10
  },
  title: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: -10,
  },
  menupartleft: {
    width: '15%',
    marginLeft: 15,
    marginTop: 20
  },
  menupartright: {
    width: '85%',
    marginTop: 24,
  },
  textcenter: {
    justifyContent: 'center',
    textAlign: 'center'
  }
})

module.exports = Instructer;
