import React from "react";
import { View } from "react-native";
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn } from '../../config/auth';
const PORT = "http://192.168.0.16:3000";//system

export default class Signup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      Password: "",
      confirmPassword: "",
      showErrorEmail: false,
      showErrorPassword: false,
      showErrorConfirmPassword: false,
      errorEmailMessage: "",
      errorPasswordMessage: "",
      errorConfirmPasswordMessage: ""

    };
  }
  componentWillMount(){
    console.log(this.props.navigation);
  }

/*authenticating starts*/
  _authenticateSignup = async () => {
    const { navigate } = this.props.navigation;
    /*if(this.state.email!==""&&this.state.password!==""&&this.state.confirmPassword!=="")
    {
      console.log('came')
      if(this.state.password!==this.state.confirmPassword)
      {
        try 
        {
          const response = await fetch(PORT+'/users/signup',{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              email: this.state.email,
              password: this.state.password,
            })
          })
          const auth = await response.json()
          console.log(auth)
          if(auth.MessageResponse){
            onSignIn().then(() => navigate("SignedIn"));
          }else
          {
            this.setState(
              {
                showErrorEmail:true,
                showErrorPassword:true,
                showErrorConfirmPassword:true,
                errorEmailMessage:"check Email",
                errorPasswordMessage:"check Password",
                errorConfirmPasswordMessage:"check Confirm Password"
              }
            )
          }
        } catch (e) 
        {
          console.log(e);
        }
      }else
      {
        this.setState(
          {
            showErrorConfirmPassword:true,
            errorConfirmPasswordMessage:"Password not match" 
          }
        )
      }
    }else
    {
      this.setState(
        {
          showErrorEmail:true,
          showErrorPassword:true,
          showErrorConfirmPassword:true,
          errorEmailMessage:"check Email",
          errorPasswordMessage:"check Password",
          errorConfirmPasswordMessage:"check Confirm Password"          
        }
      )
    }*/onSignIn().then(() => navigate("test"));
  }
/*authenticating starts*/

  render(){
    const { navigate } = this.props.navigation;
    return(
      <View style={{ paddingVertical: 20 }}>
        <Card>
          <FormLabel>Email</FormLabel>
          <FormInput 
            placeholder="Email address..." 
            onChange={ (event) => this.setState({email:event.nativeEvent.text}) }
            onFocus={ () => this.setState({showErrorEmail:false, showErrorPassword:false, showErrorConfirmPassword:false}) }
          />
          {this.state.showErrorEmail?
            <FormValidationMessage>
              {this.state.errorEmailMessage}
            </FormValidationMessage>
            :
            null
          }
          <FormLabel>Password</FormLabel>
          <FormInput secureTextEntry 
            placeholder="Password..."
            onChange={ (event) => this.setState({password:event.nativeEvent.text}) }
            onFocus={ () => this.setState({showErrorPassword:false, showErrorEmail:false, showErrorConfirmPassword:false}) } 
          />
          {this.state.showErrorPassword?
            <FormValidationMessage>
              {this.state.errorPasswordMessage}
            </FormValidationMessage>
            :
            null
          }
          <FormLabel>Confirm Password</FormLabel>
          <FormInput secureTextEntry 
            placeholder="Confirm Password..."
            onChange={ (event) => this.setState({confirmPassword:event.nativeEvent.text}) }
            onFocus={ () => this.setState({showErrorConfirmPassword:false, showErrorPassword:false,showErrorEmail:false}) } 
          />
          {this.state.showErrorConfirmPassword?
            <FormValidationMessage>
              {this.state.errorConfirmPasswordMessage}
            </FormValidationMessage>
            :
            null
          }
          <Button
            buttonStyle={{ marginTop: 20 }}
            backgroundColor="#03A9F4"
            title="SIGN UP"
            onPress={ () => this._authenticateSignup() }
          />
        </Card>
      </View>
    );
  }
}
