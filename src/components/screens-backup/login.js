import React, { Component } from "react";
import { View, ToastAndroid, Image, StyleSheet, Text, TouchableOpacity, Keyboard} from "react-native";
import { Card, Button, FormLabel, FormInput, labelStyle, FormValidationMessage  } from "react-native-elements";
import { onSignIn,onSignOut } from '../../config/auth';
import { signin, getCourses } from '../template/api.js'
import { AsyncStorage } from "react-native";
import { Dimensions } from 'react-native';
import {NativeModules} from 'react-native';

import axios from 'axios';

import {Platform} from 'react-native';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';

// this shall be called regardless of app state: running, background or not running. Won't be called when app is killed by user in iOS
FCM.on(FCMEvent.Notification, async (notif) => {
    // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
    
    console.log(notif);

      //this is a local notification
      FCM.presentLocalNotification({
            id: Date.now().toString(),                               // (optional for instant notification)
            title: notif.fcm.title,                     // as FCM payload
            body: notif.fcm.body,                    // as FCM payload (required)
            sound: "default",                                   // as FCM payload
            priority: "high",                                   // as FCM payload
            click_action: "ACTION",                             // as FCM payload
            // badge: 10,                                          // as FCM payload IOS only, set 0 to clear badges
                 // Android only
            auto_cancel: true,                                  // Android only (default true)
            icon: "ic_launcher",                                // as FCM payload, you can relace this with custom icon you put in mipmap                      // Android only
            color: "red",                                       // Android only
            vibrate: 300,                                       // Android only default: 300, no vibration if you pass 0
            lights: true,                                       // Android only, LED blinking (default false)
            show_in_foreground : true                                 // notification when app is in foreground (local & remote)
        });


    if(notif.opened_from_tray){
      //iOS: app is open/resumed because user clicked banner
      //Android: app is open/resumed because user clicked banner or tapped app icon

      console.log("Opened from Tray")
    }
    // await someAsyncCall();

    // if(Platform.OS ==='ios'){
    //   //optional
    //   //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
    //   //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
    //   //notif._notificationType is available for iOS platfrom
    //   switch(notif._notificationType){
    //     case NotificationType.Remote:
    //       notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
    //       break;
    //     case NotificationType.NotificationResponse:
    //       notif.finish();
    //       break;
    //     case NotificationType.WillPresent:
    //       notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
    //       break;
    //   }
    // }



});
FCM.on(FCMEvent.RefreshToken, (token) => {
    console.log(token)
    // fcm token may not be available on first load, catch it here
});


var { width, height } = Dimensions.get('window');
export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      showErrorUsername: false,
      showErrorPassword: false,
      errorUsernameMessage: "",
      errorPasswordMessage: "",
      loader:false
    };
  }

  componentWillMount(){
    onSignOut();  
   // onPressHandle(); 
// iOS: show permission prompt for the first call. later just check permission in user settings
        // Android: check permission in user settings
        FCM.requestPermissions().then(()=>console.log('granted')).catch(()=>console.log('notification permission rejected'));
        
        FCM.getFCMToken().then(token => {
            console.log(token)
            // store fcm token in your server
            this.setState({
              token : token
            })
            console.log(this.state.token);
        });
        
        // this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
        //     // optional, do some component related stuff

        //      console.log(notif)
        // });
        
        // initial notification contains the notification that launchs the app. If user launchs app by clicking banner, the banner notification info will be here rather than through FCM.on event
        // sometimes Android kills activity when app goes to background, and when resume it broadcasts notification before JS is run. You can use FCM.getInitialNotification() to capture those missed events.
   
  }

/*authenticating starts*/
  _authenticateLoign = async () => {
    console.log(this.state)
    const { navigate } = this.props.navigation;
    Keyboard.dismiss();
    if(this.state.username!==""||this.state.password!=="")
    {
      this.setState({loader:true})
      signin(this.state.username, this.state.password)
      .then(response=>
      {
        console.log(response);
        if(response.status){
          this.setState({loader:false})
          ToastAndroid.show(response.show, ToastAndroid.SHORT);

          console.log("Notification Regitration started");
          axios.post("http://resoltz.azurewebsites.net/api/v1/notification/deviceregistration",{
            "user_id":response.data.id,
            "token" : this.state.token
          })
          .then((res)=>{
            console.log(res);
          })
          .catch((err)=>{
            console.log('error');
          });
          navigate("SignedIn")
        }
        else{
          this.setState({loader:false})
          ToastAndroid.show(response.show, ToastAndroid.SHORT);
        }
      },error=>{
        console.log(error)
        this.setState({loader:false})
        ToastAndroid.show(error.show, ToastAndroid.SHORT);
      })

    }else{
      this.setState(
        {
          showErrorUsername:true,
          showErrorPassword:true,
          errorUsernameMessage:"Enter Username",
          errorPasswordMessage:"Enter Password"
        }
      )
    }
  }
/*authenticating starts*/

  render(){
    const { navigate } = this.props.navigation;
    return(
      <View>
          <View>
            <View style={styles.formbox}>
            <FormLabel></FormLabel>
            <FormInput
              placeholder="Username"
              onChange={ (event) => this.setState({username:event.nativeEvent.text}) }
              onFocus={ () => this.setState({showErrorUsername:false,showErrorPassword:false}) }
              style={styles.formelement}
            />
            {this.state.showErrorUsername?
              <FormValidationMessage labelStyle ={styles.errorText}>
                {this.state.errorUsernameMessage}
              </FormValidationMessage>
              :
              null
            }
            <FormLabel></FormLabel>
            <FormInput secureTextEntry
              placeholder="Password"
              onChange={ (event) => this.setState({password:event.nativeEvent.text}) }
              onFocus={ () => this.setState({showErrorPassword:false,showErrorUsername:false}) }
              style={styles.formelement}
            />
            {this.state.showErrorPassword?
              <FormValidationMessage labelStyle ={styles.errorText}>
                {this.state.errorPasswordMessage}
              </FormValidationMessage>
              :
              null
            }
            </View>
            <Button
              buttonStyle={{marginTop:50}}
              title="LOGIN"
              fontSize={20}
              disabled={this.state.loader}
              disabledStyle={{backgroundColor:'#ef7e2d'}}
              loading={this.state.loader}
              backgroundColor="#ef7e2d"
              onPress={() => this._authenticateLoign() }
            />
            <TouchableOpacity>
            <Text style = {styles.buttonFp}>
               Forgot password?
            </Text>
         </TouchableOpacity>
          </View>
      </View>

    );
  }
}


const styles = StyleSheet.create({

  backgroundImage: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover'
  },
  loginbox: {
    bottom:30,
    left:0,
    position: 'absolute'
  },
  formbox: {
    width: '100%',
    marginTop: 50
  },
  formelement: {
    zIndex:9999,
    width:width-30,
    fontSize: 20,
    color: '#262626'
  },
  buttontext: {
    fontSize: 800,
    marginTop: 30
  },
  buttonFp: {
      alignSelf:'center',
      padding: 10,
      fontSize:16,
      marginTop: 10
  },
  errorText:{
    fontSize:15,
    color:'#ce0707',

  }


});
