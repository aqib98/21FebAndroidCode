import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import Ionicons from 'react-native-vector-icons/Ionicons'

import { TabNavigator } from 'react-navigation';


import styles from './styles.js'



export default class FinishExercise extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      width:0,
      height : 0
    };
  }
  componentDidMount () {


  }



  render() {
    const {navigation} = this.props

    return (
      <View style={styles.upcomingWorkouts_main_body}>
        <View style={styles.upcomingWorkouts_bg_img_view}>
          <Image style={styles.upcomingWorkouts_bg_img} source={{ uri: 'gradientbg' }}/>
        </View>
        <View>
        <Text style={styles.finished_heading}>
          You are finished
        </Text>
        <Text style={styles.wellness}>
          Rate your wellness actvity
        </Text>
        <View style={styles.star_icons}>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <Image style={{ width: 25, height: 25 }} source={{uri:'star_filed'}} />
          </View>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <Image style={{ width: 25, height: 25 }} source={{uri:'star_unfiled'}} />
          </View>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <Image style={{ width: 25, height: 25 }} source={{uri:'star_unfiled'}} />
          </View>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <Image style={{ width: 25, height: 25 }} source={{uri:'star_unfiled'}} />
          </View>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <Image style={{ width: 25, height: 25 }} source={{uri:'star_unfiled'}} />
          </View>
        </View>
        <Text style={styles.keepitup}>
          Keep it up!
        </Text>
        <Text style={styles.keepitup2}>
          You are one activity closer to achieving your best week this month.
        </Text>
        <TouchableOpacity onPress={()=>{
          const { navigate } = this.props.navigation;

        navigate("Tabs");
        }}>
          <View style={styles.saveBtn}>
            <Text style={styles.saveTxtBtn}>
              Save
            </Text>
            <Image style={{ width: 200, height: 50 }} source={{uri:'white_btn_bg'}} />
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.trans_bg}>
      </View>
      <View style={styles.btm_tabNavigation}>
        <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
          <Image style={{ width: 32, height: 28, top:3, position:'absolute' }} source={{uri:'feedicon'}} />
          <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
            FEED
          </Text>
        </View>
        <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
          <Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'profileicon'}} />
          <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
            PROFILE
          </Text>
        </View>
        <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
          <Image style={{ width: 45, height: 45, marginTop:3 }} source={{uri:'bottom_tab_middlelogo'}} />
          <Text style={{fontSize:10,color:'#fff', marginTop:12}}> </Text>
        </View>
        <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
          <Image style={{ width: 28, height: 31, top:3, position:'absolute'  }} source={{uri:'notificationicon'}} />
          <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
            NOTIFICATION
          </Text>
        </View>
        <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
          <Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'btnmenuicon'}} />
          <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
            MORE
          </Text>
        </View>
      </View>
    </View>
  );
  }
}
