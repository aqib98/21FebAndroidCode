/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
Platform,
StyleSheet,
Text,
View,
Switch,
Image,
PixelRatio,
KeyboardAvoidingView,
ScrollView,
TouchableHighlight,
TouchableOpacity,
AsyncStorage,
ActivityIndicator
} from 'react-native';
import {ApolloProvider,graphql,createNetworkInterface,ApolloClient} from 'react-apollo';
//import ApolloClient , { createNetworkInterface } from 'apollo-client';
import gql from 'graphql-tag'


import Carousel, { Pagination,ParallaxImage } from 'react-native-snap-carousel';
import PTRView from 'react-native-pull-to-refresh';

import { NavigationActions } from 'react-navigation';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'

import { LineChart, YAxis } from 'react-native-svg-charts'
import { LinearGradient, Stop } from 'react-native-svg'

import * as shape from 'd3-shape'
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');


import SplashScreen from 'react-native-smart-splash-screen'

import styles from './styles.js'
import { ENTRIES2 } from './entries';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}
import { getFeed } from '../../template/Workout/workout.js'
const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;



class FeedApp extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      width:0,
      height : 0,
      user:{id:'', name:''},
      slider1Ref :null,
      slider1ActiveSlide: 1,
      feeds:[],
      loader:null
    };
  }

  async componentWillMount(){
    console.log('hihi')
    var state = this.state;
    let USER_ID = await AsyncStorage.getItem("USER_ID");
    let USER_NAME = await AsyncStorage.getItem("USER_NAME");
    state.user.id=USER_ID;
    state.user.name = USER_NAME;
    this.setState(state,()=>{console.log(this.state.user)});

    this._getFeed(USER_ID)

    }

    _getFeed=(user_id)=>{
      this.setState({loader:true})
      getFeed(user_id).then(response=>{
        console.log(response);
        var data = response.data
        users = data.feeds
        var arr = []
        var mani = []
        var feeddata = []

          for(i=0;i<users.length;i++)
          {

            userwrokout_data = users[i].workouts
            useractivity_data = users[i].activitydata
            for(j=0;j<userwrokout_data.length;j++)
            {
              userexcierce_data = userwrokout_data[j].exercises;
              //arr.push([users[i].user_id, users[i].name,userwrokout_data[j].title,userwrokout_data[j].completion_date,userwrokout_data[j].thumbnail_url,userwrokout_data[j].intensity,userexcierce_data[0].avg_heart_rate])
              arr["user_id"] = users[i].user_id;
              arr["name"] = users[i].name
              arr["title"] = userwrokout_data[j].title
              arr["completion_date"] = userwrokout_data[j].completion_date
              arr["thumbnail_url"] = userwrokout_data[j].thumbnail_url
              arr["intensity"] = userwrokout_data[j].intensity
              arr["heartrate"] = userexcierce_data[0].avg_heart_rate

              var today = new Date();
              var Christmas = new Date(userwrokout_data[j].completion_date);
              var diffMs = (today-Christmas); // milliseconds between now & Christmas
              var diffDays = Math.floor(diffMs / 86400000); // days
              var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
              var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
              if(diffDays > 0)
              {
                arr["completion_date"] = diffDays + " days ago";
              } else if( diffHrs > 0) { arr["completion_date"] = diffHrs + " hours ago"; } else { arr["completion_date"] = diffMins + " minutes ago"; }
              var obj = {"user_id":arr["user_id"],"name":arr["name"],"title":arr["title"],"completion_date":arr["completion_date"],"thumbnail_url":arr["thumbnail_url"],"intensity":arr["intensity"],"heartrate":arr["heartrate"]};
              feeddata.push(obj)
              arr =[]

            }
            for(j=0;j<useractivity_data.length;j++)
            {
              //arr.push([users[i].user_id, users[i].name,useractivity_data[j].title,useractivity_data[j].completed_on,'','',''])
              arr["user_id"] = users[i].user_id;
              arr["name"] = users[i].name
              arr["title"] = useractivity_data[j].title
              arr["completion_date"] = useractivity_data[j].completed_at
              arr["thumbnail_url"] = 'https://cdn-resoltz-assets.azureedge.net/content/images/corporate/onepage/classes-4.jpg'
              arr["intensity"] = '0'
              arr["heartrate"] = useractivity_data[j].avg_heart_rate

              var today = new Date();
              var Christmas = new Date(useractivity_data[j].completed_on);
              var diffMs = (today-Christmas); // milliseconds between now & Christmas
              var diffDays = Math.floor(diffMs / 86400000); // days
              var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
              var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
              if(diffDays > 0)
              {
                arr["completion_date"] = diffDays + " days ago";
              } else if( diffHrs > 0) { arr["completion_date"] = diffHrs + " hours ago"; } else { arr["completion_date"] = diffMins + " minutes ago"; }

              var obj = {};
              var len = arr.length;
              var obj = {"user_id":arr["user_id"],"name":arr["name"],"title":arr["title"],"completion_date":arr["completion_date"],"thumbnail_url":arr["thumbnail_url"],"intensity":arr["intensity"],"heartrate":arr["heartrate"]};
              feeddata.push(obj)
              arr =[]

            }

            mani.push(['user'+i+'', feeddata])



            feeddata=[]

          }

          this.setState({feeds:mani})
          console.log(mani)
          this.setState({loader:false})

      },err=>{
        console.log(err)
      })

    }


  _renderWorkout= ({item, index},parallaxProps)=> {

    heartrate = []
    high_heart_rate = ''
    if(item.heartrate)
    {
      heartrate = item.heartrate
      high_heart_rate = Math.max.apply(null, heartrate)
    }
    const resizeMode = 'center';
    const text = 'This is some text inlaid in an <Image />';

    if(item.intensity < 5)
    {
      var level = "LOW"
    }
    else {
      var level = "HIGH"
    }

    return (
        <View >
        <View style={styles.feed_img_mas}>
          <Image style={{ width: 45, height: 45, marginTop:15 }} source={{uri:'bottom_tab_middlelogo'}} />
          <Text style={styles.feed_title1}>{item.name}</Text>
          <View style={styles.feedrgt_mas}>
            <Text style={styles.feedrgt1}>{item.title}</Text>
            <Text style={styles.feedrgt2}>{item.completion_date}</Text>
          </View>
        </View>
          <TouchableOpacity
              activeOpacity={1}
              style={styles.carousel_style_workout}
              onPress={() => { console.log('hi'); }}
              >

                <View style={{flex: 1,backgroundColor: 'white',}}>

                    <Image
                      style={{
                        backgroundColor: '#ccc',
                        flex: 1,
                        resizeMode,
                        position: 'absolute',
                        width: '100%',
                        height: '100%',
                        flexDirection: 'column',
                        justifyContent: 'flex-end',
                      }}
                      source={{ uri: item.thumbnail_url }}
                    >

                      <LineChart
                          style={ { height: 80, opacity:0.85, backgroundColor: '#333333', marginTop: 20 } }
                          dataPoints={ heartrate }
                          renderGradient={ ({ id }) => (
                              <LinearGradient id={ id } x1={ '0%' } y={ '0%' } x2={ '0%' } y2={ '100%' }>
                                  <Stop offset={ '0%' } stopColor={ 'rgb(228, 35, 89)' } stopOpacity={ 0.8 }/>
                                  <Stop offset={ '100%' } stopColor={ 'rgb(255, 156, 0)' } stopOpacity={ 0.9 }/>
                              </LinearGradient>
                          ) }
                      />


                    </Image>

                </View>
                <View>
                  <Text style={styles.feedrgt2}>{level}</Text>
                  <Text style={styles.feedrgt2}>MAX Heart Rate: {Math.max(high_heart_rate)}</Text>
                </View>

            </TouchableOpacity>

        </View>
    );
}
_refresh () {

  return new Promise((resolve) => {
    setTimeout(()=>{
      resolve();
    }, 4000)
  })
}
render() {
  console.log(this.props.data)
  const {navigation} = this.props;
  const { loader } = this.state;
  const Msni = ENTRIES2
  const resizeMode = 'center';
  const text = 'I am some centered text';
  return (
      <View style={styles.mainBody} >
            <View style={styles.listofWrkouts1}>
              <View style={styles.chevron_left_icon}>
                <TouchableOpacity >
                    <FontAwesome name="chevron-left" size={25} color="#FF7E00"   />
                </TouchableOpacity>
              </View>
              <View style={styles.header}>
                    <Text style={styles.topSignupTxt}>
                      Feed
                    </Text>
              </View>
              <View>
                <Text style = {styles.text_workout_heading}>
                  Hey {this.state.user.name}!
                </Text>
                <Text style = {styles.text_workout_sub_heading}>
                  REMAINDER TO STAY STRONG AND KEEP YOUR GOALS IN MIND
                </Text>
              </View>
              <View style={styles.slidertop_btns}>
                <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                  <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                  <TouchableOpacity>
                    <View style={styles.gradi_btns}>
                      <Text style={styles.gradi_NormalbtnsTxtBtn}>
                          Distance
                      </Text>
                      <View style={styles.gradiPagBtns}></View>
                    </View>
                  </TouchableOpacity>
                  </View>
                  <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                  <TouchableOpacity>
                  <View style={styles.gradi_btns}>
                    <Text style={styles.gradi_NormalbtnsTxtBtn}>
                        Weights
                    </Text>
                    <View style={styles.gradiPagBtns}></View>
                  </View>
                  </TouchableOpacity>
                  </View>
                  <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                  <TouchableOpacity>
                  <View style={styles.gradi_btns}>
                    <Text style={styles.gradi_btnsTxtBtn}>
                        Conditioning
                    </Text>
                    <Image style={{ width: 86, height: 32 }} source={{uri:'gradi_btn'}} />
                    </View>
                  </TouchableOpacity>
                  </View>
                  <View style = {{flex:0.16,alignItems:'center',justifyContent:'center',}}>
                    <TouchableOpacity >
                        <View>
                          <Feather name="sliders" size={25} color='#fff' />
                        </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
            {loader?
              <ActivityIndicator
                animating = {this.state.contentLoader}
                color = '#bc2b78'
                size = "large"
                style = {styles.activityIndicator}
              />
            :

            <ScrollView
              style={{flex:1, paddingBottom: 10, marginBottom: 50}}
              contentContainerStyle={{paddingBottom: 50}}
              indicatorStyle={'white'}
              scrollEventThrottle={200}
              directionalLockEnabled={true}
            >
            {this.state.feeds.map((data1)=>{
              console.log(data1)
              console.log(this.state.feeds)
              const feed_data = data1[1]
              console.log(data1[1])
              return(
                <View style={{borderBottomWidth:4}}>
                    <Carousel
                      ref={(c) => { if (!this.state.slider1Ref) { this.setState({ slider1Ref: c }); } }}
                      data={feed_data}
                      renderItem={this._renderWorkout}
                      sliderWidth={sliderWidth}
                      itemWidth={230}
                      itemHeight={300}
                      hasParallaxImages={false}
                      firstItem={0}
                      containerCustomStyle={{marginTop: 15}}
                      itemWidth={itemWidth}
                      enableSnap = {true}
                      lockScrollWhileSnapping = {false}
                      onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                    />
                  </View>
              )
            }
            )}
            </ScrollView>
            }
            <View style={styles.btm_tabNavigation}>
                <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 32, height: 28, top:3, position:'absolute' }} source={{uri:'feedicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>FEED</Text></View>
                <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'profileicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>PROFILE</Text></View>
                <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 45, height: 45, marginTop:3 }} source={{uri:'bottom_tab_middlelogo'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}> </Text></View>
                <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 31, top:3, position:'absolute'  }} source={{uri:'notificationicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>NOTIFICATION</Text></View>
                <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'btnmenuicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>MORE</Text></View>
            </View>
          </View>

  );
}
}





const App = graphql(gql`
    query allposts {
      Users{
        id
      }
    }
    `)(FeedApp)


    export default class Feed extends Component<{}> {
        constructor(props) {
          super(props);
          this.state = {

          };
          const networkInterface = createNetworkInterface('http://localhost:4000/graphql');
          this.client = new ApolloClient({
            networkInterface
          })
        }
        render(){
          <ApolloProvider client = {this.client}>
            <App/>
          </ApolloProvider>
        }
      }
