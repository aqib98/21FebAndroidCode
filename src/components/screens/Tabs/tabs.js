
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { TabNavigator } from 'react-navigation';

import styles from './styles.js';
import ProfileDetails from './profiledetails.js';
import Feed from './feed.js';
import More from './more.js'
import Notification from './notification.js';
import ConnectToBle from '../Landing/connecttoble.js'
import SearchDevices from '../Landing/searchdevices.js';
import FinishExercise from '../finishexercise.js';
import Courses from '../Courses/courses.js';
export default class Tabs extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      workouts:[],
      loader:null,
      showProfile:null,
      showFeed:true,
      showNotification:null,
      showMore:null
    };
  }
  componentWillMount () {
    console.log('tabs')
  }
  route(routeName){
    const { navigate  } = this.props.navigation;
    this.setState({})
    navigate(routeName)
  }
  render() {
    const {navigation} = this.props;
    const {
      showFeed,
      showMore,
      showNotification,
      showProfile } = this.state;
    return (
      <View style={styles.upcomingWorkouts_main_body} >
        {showProfile?<ProfileDetails from={'tabs'}/>:null}
        {showNotification?<Notification />:null}
        {showFeed?<Feed />:null}
        {showMore?<More navigation={navigation}/>:null}
        <View style={styles.btm_tabNavigation}>
          <TouchableOpacity
            style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}
            activeOpacity={1}onPress={() => {
              this.setState({showProfile:false, showMore:false, showNotification:false, showFeed:true});
            }}>
            <Image style={{ width: 32, height: 28, top:3, position:'absolute' }} source={{uri:'feedicon'}} />
            <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
              FEED
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}
            activeOpacity={1}
            onPress={() => {
              this.setState({showProfile:true, showMore:false, showNotification:false, showFeed:false});
            }}>
            <Image style={{ width: 32, height: 32, top:1, position:'absolute'  }} source={{uri:'profileicon'}} />
            <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
              PROFILE
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}
            activeOpacity={1} onPress={() => {
              this.route('UpcomingWorkouts');
            }}>
            <Image style={{ width: 45, height: 45, marginTop:0, marginBottom:4 }} source={{uri:'bottom_tab_middlelogo'}} />
            <Text style={{fontSize:10,color:'#fff', marginTop:12}}> </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}
            activeOpacity={1}onPress={() => {
              this.setState({showProfile:false, showMore:false, showNotification:true, showFeed:false});
            }}>
            <Image style={{ width: 28, height: 31, top:2, position:'absolute'  }} source={{uri:'notificationicon'}} />
            <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
              NOTIFICATION
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}
            activeOpacity={1}onPress={() => {
              this.setState({showProfile:false, showMore:true, showNotification:false, showFeed:false});
            }}>
            <Image  style={{ width: 32, height: 32, top:1, position:'absolute'  }}  source={{uri:'btnmenuicon'}} />
            <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
              MORE
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
