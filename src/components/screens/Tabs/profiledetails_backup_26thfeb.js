import React, { Component } from 'react';
import {
  AppRegistry,
  FlatList,
  StyleSheet,
  TextInput,
  Picker,
  ScrollView,
  TouchableOpacity,
  Text,
  Image,
  KeyboardAvoidingView,
  View,
  TouchableHighlight,
  ToastAndroid,
  Alert,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

import { onSignIn, getAllAsyncStroage } from '../../../config/auth';
import { getProfileDetails } from '../../template/SQLiteOperationsOffline.js';
import { updateProfile } from '../../template/api.js';
import API from '../../template/constants.js';

import { AsyncStorage } from "react-native";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';

import PhotoUpload from 'react-native-photo-upload'
import axios from 'axios';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');
import styles from '../Landing/styles.js';
export default class ProfileDetails extends Component {

    constructor(props) {
      super(props);
      this.state={
        profile:[],
        height_in_feet:null,
        height_in_centimeter:null,
        height_in_inches:null,
        weight:null,
        metricIndex : null,
        age:null,
        gender:'Male',
        userid:null,
        genderIndex:null,
        enable:null,
        Loader: false,
        contentLoader:true,
        editable:null,
        visible: true,
        profile_img_url:null,
        upload_uri: null,
        upload_uri_check : false,
        visible: true,
        imperial:0,
        weight_in_kgs: null
      }
  }
  componentWillMount(){
    getAllAsyncStroage()
    .then(res =>
      console.log(res)
    )
    .catch(err =>
      console.log(err)
    );
    this.getStudentID();
  }


  photouploadserver = (file) => {
    console.log('response',file)

      let formData = new FormData();

      if(file.fileName)
      {
         name = file.fileName
      }
      else {
         image_source = file.uri
         image = image_source.split("/");
         image_length = image.length
         name = image[image_length-1];
      }

      console.log(name)

      formData.append('myfile',{type:'image/png',name: name,uri:file.uri.toString()})

        axios.post(API.Image_url, formData ).then(response => {
        updated_img = response.data
        console.log(updated_img)

             this.setState({visible:true})
              this.setState({upload_uri: updated_img.url})
              this.setState({enable:true})


        }).catch(err => {
          console.log('err')
          console.log(err)

        })

  }


  getStudentID =async()=> {
  console.log('profile view', await AsyncStorage.getItem("DEVICE_NAME"))
    let USER_ID = await AsyncStorage.getItem("USER_ID");
    console.log(USER_ID)
    this.setState({userid:USER_ID,enable:false, Loader:true},()=>{
      this._getStudentDetails(this.state.userid);
    });
  }
  _getStudentDetails=(id)=>{
    getProfileDetails(id).then(response=>{
      console.log(response)
      if(response.status){
        this.setState({profile:response.data},()=>{

          console.log(this.state.profile)
          this.setState({height_in_centimeter: this.state.profile.height_in_centimeter, parameter_type: this.state.profile.parameter_type, profile_img_url: this.state.profile.profile_img_url, height_in_feet:this.state.profile.height_in_feet, height_in_inches:this.state.profile.height_in_inches, age:this.state.profile.age, gender:this.state.profile.gender},()=>{

            if(this.state.gender==="Male"){
              this.setState({genderIndex:0});
            }else if(this.state.gender==="Female"){
              this.setState({genderIndex:1});
            }else{
              this.setState({genderIndex:null});
            }

            if(this.state.parameter_type == 0 ){
              this.setState({imperial:0});
              this.setState({weight:this.state.profile.current_weight})
            }else{
              this.setState({imperial:1});
              this.setState({weight_in_kgs:this.state.profile.current_weight})
            }

          });
        });
        console.log(this.state)
        this.setState({Loader:false, contentLoader:false})
      }else{
        this.setState({Loader:false, contentLoader:false})
        this.refs.toast.show(response.show, DURATION.LENGTH_LONG);
      }
    },err=>{
      console.log(err)
    })
  }

  addBLERoute=async ()=>{
    const { navigate } = this.props.navigation;
    if(await AsyncStorage.getItem("DEVICE_NAME")&& await AsyncStorage.getItem("DEVICE_ID")){
      navigate('UpcomingWorkouts')
    }else{
      Alert.alert('Do you want to save BLE device?','You can add later also.',
        [
          {text: 'Add later', onPress: () => navigate('UpcomingWorkouts')},
          {text: 'Add', onPress: () => navigate('SearchDevices', {fromRoute:'ProfileDetails', toRoute:'SearchDevices', enableBack:false})},
        ],
        { cancelable: false }
      )
    }
  }
  _savedata=()=>{

    console.log(this.state)

    //console.log(this.props, typeof this.props.navigation)
    if(typeof this.props.navigation =='undefined'){
      if(this.state.gender&&this.state.age!==''&&(this.state.weight!=""||this.state.weight_in_kgs!="")&&(this.state.height_in_feet!=""||this.state.height_in_centimeter!=""||this.state.height_in_inches!="")&&this.state.upload_uri!=""){
        if(this.state.enable){

          console.log(this.state.weight_in_kgs)

          if(this.state.age<=18||this.state.age>=70){
            alert("Age should be greater than 18 and less than 70")
          } else {
             if(this.state.imperial == 0) {

              console.log(this.state.weight)

              if(this.state.height_in_inches != null && this.state.height_in_feet != null && this.state.weight != null){
                 data = {"gender": this.state.gender, "imagePath": this.state.upload_uri, "weight": this.state.weight, "age": this.state.age, "height_in_inches": this.state.height_in_inches, "height_in_feet": this.state.height_in_feet, "height_in_centimeter": null, "id": this.state.userid, "parameter_type": this.state.imperial}

                 console.log(data)



                  updateProfile(data)
                  .then(async res=>{
                    console.log(res);
                    if(res.status){
                      this.getStudentID();
                      //this.refs.toast.show(res.show, DURATION.LENGTH_LONG);
                      alert("Updated Sucessfully")
                      console.log(await AsyncStorage.getItem("DEVICE_NAME"), await AsyncStorage.getItem("DEVICE_ID"))
                      //fromRoute?this.addBLERoute():this.setState({enable:false})
                    }else{
                      alert(res.show)
                      //fromRoute?this.addBLERoute():this.setState({enable:false})
                    }
                  },err=>{
                    console.log(err);
                    this.refs.toast.show(err.show, DURATION.LENGTH_LONG);
                    alert(err.show)
                  })




              } else {
                alert("Please fill all the fields")
              }
            } else {
              if(this.state.height_in_centimeter != null && this.state.weight_in_kgs != null){
                data = {"gender": this.state.gender, "imagePath": this.state.upload_uri, "weight": this.state.weight_in_kgs, "age": this.state.age, "height_in_inches": null, "height_in_feet": null, "height_in_centimeter": this.state.height_in_centimeter, "id": this.state.userid, "parameter_type": this.state.imperial}



                  updateProfile(data)
                  .then(async res=>{
                    console.log(res);
                    if(res.status){
                      this.getStudentID();
                      //this.refs.toast.show(res.show, DURATION.LENGTH_LONG);
                      alert("Updated Sucessfully")
                      console.log(await AsyncStorage.getItem("DEVICE_NAME"), await AsyncStorage.getItem("DEVICE_ID"))
                      //fromRoute?this.addBLERoute():this.setState({enable:false})
                    }else{
                      alert(res.show)
                      //fromRoute?this.addBLERoute():this.setState({enable:false})
                    }
                  },err=>{
                    console.log(err);
                    alert(err.show)
                  })




              } else {
                alert("Please fill all the fields")
              }
            }

            console.log(this.state.imperial)

          }



        /*


        if(this.state.age<=18||this.state.age>=70){
          this.refs.toast.show("Age should be greater than 18 and less than 70", DURATION.LENGTH_LONG);
        }else if(this.state.weight<=30||this.state.weight>=200){
          this.refs.toast.show("Enter valid weight in lbs", DURATION.LENGTH_LONG);
        }else if(this.state.height<=1||this.state.height>=8){
          this.refs.toast.show("Enter valid height in feet", DURATION.LENGTH_LONG);
        }else{
          updateProfile({imagePath: this.state.upload_uri, weight:this.state.weight, age:this.state.age, height:this.state.height, gender:this.state.gender, id:this.state.userid})
          .then(async res=>{
            console.log(res);
            if(res.status){
              this.getStudentID();
              this.refs.toast.show(res.show, DURATION.LENGTH_LONG);
              this.setState({enable:false})
            }else{
              this.refs.toast.show(res.show, DURATION.LENGTH_LONG);
              this.setState({enable:false})
            }
          },err=>{
            console.log(err);
            this.refs.toast.show(err.show, DURATION.LENGTH_LONG);
          })
        }

        */


      }else{
        alert(" Please update any value")
      }
      }else{
        this.refs.toast.show("Field cant be empty or 0", DURATION.LENGTH_LONG);
      }
    } else
    {
      var { fromRoute } = this.props.navigation.state.params;
      const { navigate  } = this.props.navigation;
      console.log(this.state)

      if(this.state.gender&&this.state.age!==''&&(this.state.weight!=""||this.state.weight_in_kgs!="")&&(this.state.height_in_feet!=""||this.state.height_in_centimeter!=""||this.state.height_in_inches!="")&&this.state.upload_uri!=""){
        if(this.state.enable){

          console.log(this.state.weight_in_kgs)

          if(this.state.age<=18||this.state.age>=70){
            alert("Age should be greater than 18 and less than 70")
          } else {
             if(this.state.imperial == 0) {

              console.log(this.state.weight)

              if(this.state.height_in_inches != null && this.state.height_in_feet != null && this.state.weight != null){
                 data = {"imagePath": this.state.upload_uri, "weight": this.state.weight, "age": this.state.age, "height_in_inches": this.state.height_in_inches, "height_in_feet": this.state.height_in_feet, "height_in_centimeter": null, "id": this.state.userid, "parameter_type": this.state.imperial}

                 console.log(data)



                  updateProfile(data)
                  .then(async res=>{
                    console.log(res);
                    if(res.status){
                      this.getStudentID();
                      //this.refs.toast.show(res.show, DURATION.LENGTH_LONG);
                      alert("Updated Sucessfully")
                      console.log(await AsyncStorage.getItem("DEVICE_NAME"), await AsyncStorage.getItem("DEVICE_ID"))
                      fromRoute?this.addBLERoute():this.setState({enable:false})
                    }else{
                      alert(res.show)
                      fromRoute?this.addBLERoute():this.setState({enable:false})
                    }
                  },err=>{
                    console.log(err);
                    this.refs.toast.show(err.show, DURATION.LENGTH_LONG);
                    alert(err.show)
                  })




              } else {
                alert("Please fill all the fields")
              }
            } else {
              if(this.state.height_in_centimeter != null && this.state.weight_in_kgs != null){
                data = {"imagePath": this.state.upload_uri, "weight": this.state.weight_in_kgs, "age": this.state.age, "height_in_inches": null, "height_in_feet": null, "height_in_centimeter": this.state.height_in_centimeter, "id": this.state.userid, "parameter_type": this.state.imperial}



                  updateProfile(data)
                  .then(async res=>{
                    console.log(res);
                    if(res.status){
                      this.getStudentID();
                      //this.refs.toast.show(res.show, DURATION.LENGTH_LONG);
                      alert("Updated Sucessfully")
                      console.log(await AsyncStorage.getItem("DEVICE_NAME"), await AsyncStorage.getItem("DEVICE_ID"))
                      fromRoute?this.addBLERoute():this.setState({enable:false})
                    }else{
                      alert(res.show)
                      fromRoute?this.addBLERoute():this.setState({enable:false})
                    }
                  },err=>{
                    console.log(err);
                    alert(err.show)
                  })




              } else {
                alert("Please fill all the fields")
              }
            }

            console.log(this.state.imperial)

          }



        /*


        if(this.state.age<=18||this.state.age>=70){
          this.refs.toast.show("Age should be greater than 18 and less than 70", DURATION.LENGTH_LONG);
        }else if(this.state.weight<=30||this.state.weight>=200){
          this.refs.toast.show("Enter valid weight in lbs", DURATION.LENGTH_LONG);
        }else if(this.state.height<=1||this.state.height>=8){
          this.refs.toast.show("Enter valid height in feet", DURATION.LENGTH_LONG);
        }else{
          updateProfile({imagePath: this.state.upload_uri, weight:this.state.weight, age:this.state.age, height:this.state.height, gender:this.state.gender, id:this.state.userid})
          .then(async res=>{
            console.log(res);
            if(res.status){
              this.getStudentID();
              this.refs.toast.show(res.show, DURATION.LENGTH_LONG);
              this.setState({enable:false})
            }else{
              this.refs.toast.show(res.show, DURATION.LENGTH_LONG);
              this.setState({enable:false})
            }
          },err=>{
            console.log(err);
            this.refs.toast.show(err.show, DURATION.LENGTH_LONG);
          })
        }

        */


      }else{
        alert(" Please update any value")
      }
      }else{
        this.refs.toast.show("Field cant be empty or 0", DURATION.LENGTH_LONG);
      }


    }

  }
  onSelect(index, value){
    console.log(index)
    if(!index){
      this.setState({enable:true,gender:"Male"})
    }else{
      this.setState({enable:true, gender:"Female"})
    }
  }

  openMenu = async() => {

    const { navigate  } = this.props.navigation;
    navigate("Menu");

  }

  onUnitSelect(index, value){
    console.log(index)
    if(!index){
      this.setState({imperial:0})
      this.setState({height_in_inches:0})
      this.setState({height_in_feet:0})
      this.setState({weight:0})
    }else{
      this.setState({imperial:1})
      this.setState({height_in_centimeter:0})
      this.setState({current_weight:0})
    }

    //
    //
    // if(value===0){
    //   if(!(this.state.weight_in_kgs==null)){
    //     let weight = (this.state.weight_in_kgs*0.45359)
    //     this.setState({weight:weight})
    //   }
    //   if(!(this.state.height_in_centimeter==null)){
    //     let feet = this.state.height_in_centimeter*0.0328084
    //     console.log('feet',feet)
    //     let inches = ((feet % 1)*100)
    //     console.log('inches',inches)
    //     this.setState({height_in_feet:(feet)})
    //   }
    //   this.setState({imperial:1})
    // }
    // if(value === 1){
    //   if(!(this.state.weight === null) ){
    //     let weight_in_kgs = (this.state.weight/0.45359)
    //     this.setState({weight_in_kgs:weight_in_kgs})
    //   }
    //   if((!(this.state.height_in_feet === null)) && (!(this.state.height_in_inches === null)) ){
    //     let height_in_centimeter = this.state.height_in_feet * 30.48
    //     console.log('height_in_centimeter',this.state.height_in_centimeter)
    //     this.setState({height_in_centimeter : height_in_centimeter})
    //   }
    //   this.setState({imperial:0})
    // }
  }

  render() {
    var that=this;
    var radio_props = [
      {label: 'Male', value: 0 },
      {label: 'Female', value: 1 }
    ];
    const { height, weight, age, enable, loader, contentLoader, profile_img_url, weight_in_kgs, height_in_inches, height_in_feet, height_in_centimeter } = this.state;

    if(profile_img_url == null){
       image_path = 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg'
    } else {
       image_path = profile_img_url
    }

    console.log(this.state)

    return (
      <View style={styles.mainBody}>
        <View style={styles.header}>
          <Text style={styles.topSignupTxt}>
            {this.props.navigation?this.props.navigation.state.params.title:'Profile Details'}
          </Text>
        </View>
        {contentLoader?
          <ActivityIndicator
            animating = {this.state.contentLoader}
            color = '#bc2b78'
            size = "large"
            style = {styles.activityIndicator}
          />
          :
          <ScrollView style={{ marginTop : 20, marginBottom:0, paddingBottom:10, height:500, width:Dimensions.get('window').width }} >
            <View style={styles.signup_temp_form}>
              <View style={styles.body1a}>



                <Text style={{textAlign: 'left', alignSelf: 'stretch',marginLeft:'15%',color:'#ffffff',marginBottom:5}}>Unit: </Text>
                <RadioGroup
                selectedIndex={this.state.imperial}
                style={{ flexDirection:'row', marginLeft: 10,marginBottom:10 }}
                onSelect = {(index, value) => this.onUnitSelect(index, value)}>
                  <RadioButton value={1} >
                    <Text>Imperial</Text>
                  </RadioButton>
                  <RadioButton value={0}>
                    <Text>Metric</Text>
                  </RadioButton>
                </RadioGroup>
                <Text style={{textAlign: 'left', alignSelf: 'stretch',marginLeft:'15%',color:'#ffffff',marginBottom:5}}>Weight: </Text>

                {this.state.imperial?<TextInput
                  placeholder="Weight (lbs)"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={weight_in_kgs?String(weight_in_kgs):0}
                  placeholderTextColor='#626264'
                  style={styles.textInput_signup}
                  onChangeText={ (text) => this.setState({weight_in_kgs:text, enable:true}) }
                />:<TextInput
                  placeholder="Weight (kgs)"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={weight?String(weight):0}
                  placeholderTextColor='#626264'
                  style={styles.textInput_signup}
                  onChangeText={ (text) => this.setState({weight:text, enable:true}) }
                />}

                <Text style={{textAlign: 'left', alignSelf: 'stretch',marginLeft:'15%',color:'#ffffff',marginBottom:5}}>Height: </Text>
                {!this.state.imperial?<TextInput
                  placeholder="Height (cm)"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={height_in_centimeter?String(height_in_centimeter):0}
                  placeholderTextColor='#626264'
                  style={styles.textInput_signup}
                  onChangeText={ (text) => this.setState({height_in_centimeter:text, enable:true}) }
                />:<View style={{marginRight:40,marginLeft:40,marginTop:10,marginBottom:0, paddingTop:0,paddingBottom:0,paddingLeft:0,borderRadius:30,position:'relative',color: '#fff',width:242, height: 60}}>
                  <View style = {{flex:1,flexDirection:'row'}}>
                    <TextInput
                    placeholder="Feet"
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    value={height_in_feet?String(height_in_feet):0}
                    placeholderTextColor='#626264'
                    style={{paddingLeft:15,marginTop:0,padding:10,backgroundColor:'#202124',borderRadius:30,color: '#fff',width:110, height: 50}}
                    onChangeText={ (text) => this.setState({height_in_feet:text, enable:true}) }
                  />
                  <Text style = {{marginTop:10,color:'#ffff',fontSize:20}}> : </Text>
                  <TextInput
                    placeholder="Inches"
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    value={height_in_inches?String(height_in_inches):0}
                    placeholderTextColor='#626264'
                    style={{paddingLeft:15,marginTop:0,padding:10,backgroundColor:'#202124',borderRadius:30,color: '#fff',width:110, height: 50}}
                    onChangeText={ (text) => this.setState({height_in_inches:text, enable:true}) }
                  />
                </View>
                </View>}

                <Text style={{textAlign: 'left', alignSelf: 'stretch',marginLeft:'15%',color:'#ffffff',marginBottom:5, marginTop:20}}>Age: </Text>

                <TextInput
                  placeholder="Age"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={age?String(age):0}
                  placeholderTextColor='#626264'
                  style={styles.textInput_signup}
                  onChangeText={ (text) => this.setState({age:text, enable:true}) }
                />
                <Text style={{textAlign: 'left', alignSelf: 'stretch',marginLeft:'15%',color:'#ffffff',marginBottom:5}}>Gender: </Text>

                <RadioGroup
                selectedIndex={this.state.genderIndex}
                style={{ flexDirection:'row', marginLeft: 10 }}
                onSelect = {(index, value) => this.onSelect(index, value)}>
                  <RadioButton value={'Male'} >
                    <Text>Male</Text>
                  </RadioButton>
                  <RadioButton value={'Female'}>
                    <Text>Female</Text>
                  </RadioButton>
                </RadioGroup>
              {this.props.from==null?null
              :
              this.props.from==='tabs'?
              <TouchableOpacity onPress={() => that._savedata()}>
                <View style={styles.saveBtn2}>
                  {loader?
                    <ActivityIndicator
                      animating = {this.state.loader}
                      color = '#bc2b78'
                      size = "large"
                      style = {styles.activityIndicator}
                    />
                    :
                    <Text style={styles.saveTxtBtn}>
                      SAVE
                    </Text>
                  }
                  <Image style={{ width: 200, height: 50 }} source={{uri:'white_btn_bg'}} />
                </View>
              </TouchableOpacity>
              :
              null
            }


              </View>
            </View>
          </ScrollView>
        }



        <View style={{bottom:0,position:'absolute',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity onPress={()=>this._savedata()}>
                <View style={{zIndex:999,alignItems:'center',justifyContent:'center',height:50,width:width}}>


                    <Text style={{backgroundColor:'transparent',alignSelf:'center',fontFamily:'CircularStd-Black',color:'#fff',fontSize:19}}>Save</Text>

                </View>
                <Image style={{width:width,height:50,position:'absolute',bottom:0}} source={{ uri: 'btn_gradi_bg' }}/>
            </TouchableOpacity>
        </View>

      </View>
    );
  }
}

module.exports = ProfileDetails;
