/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
Platform,
StyleSheet,
Text,
View,
Switch,
ListView,
Image,
PixelRatio,
KeyboardAvoidingView,
ScrollView,
PermissionsAndroid,
TouchableHighlight,
TouchableOpacity,
NativeEventEmitter,
NativeModules,
ToastAndroid
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination,ParallaxImage } from 'react-native-snap-carousel';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
const {height, width} = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import PTRView from 'react-native-pull-to-refresh';
import SplashScreen from 'react-native-smart-splash-screen'
import Dimensions from 'Dimensions';
import BleManager from 'react-native-ble-manager';
import TimerMixin from 'react-timer-mixin';
import reactMixin from 'react-mixin';
import moment from "moment";
import { AsyncStorage } from "react-native";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import { addToSQLiteTable } from '../../template/SQLiteOperationsOffline.js';
import { addAsyncStorage } from '../../../config/auth.js'

import styles from './styles.js'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
function wp (percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

export default class SearchDevices extends Component<{}> {
 constructor(){
    super()

    this.state = {
      scanning:false,
      peripherals: new Map(),
      appState: '',
      heartrate: 0,
      screenchange: '0',
      time: moment().format("LTS"),
      date: moment().format("LL"),
      totsec: 2400,
      item: 0,
      array: [],
      sum: 0,
      average: 0,
      calories: 0,
      user_id:null,
      profile:[],
      device_id:null,
      device_name:null,
      height:'',
      weight:'',
      timepassed: false,
      rate: 1,
      volume: 1,
      duration: 0.0,
      currentTime: 0.0,
      paused: true,
      loader:false,
      times:0,
      selected:[],
      notSelected:false,
      list:[],
      fromRoute:''
    }

    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);

  }

  componentDidMount() {
    var { fromRoute } = this.props.navigation.state.params;

    this.setState({fromRoute:fromRoute})
    BleManager.start({showAlert: false});

    this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );
    this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan );
    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
        if (result) {
          console.log("Permission is oOK");
        } else {
          PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
            if (result) {
              console.log("User accept");
            } else {
              console.log("User refuse");
            }
          });
        }
      });
    }
  }

  componentWillUnmount() {
    this.handlerDiscover.remove();
    this.handlerStop.remove();
  }

  handleStopScan() {
    console.log('Scan is stopped');
    this.setState({ scanning: false });
  }

  startScan() {
    if (!this.state.scanning) {
      BleManager.scan([], 30, true).then((results) => {
        console.log('Scanning...');
        ToastAndroid.show('Scanning....' , ToastAndroid.SHORT);
        this.setState({scanning:true});
      });
    }else{
      ToastAndroid.show('Please turn on your bluetooth' , ToastAndroid.SHORT);
    }
  }

/* Old code
  handleDiscoverPeripheral(peripheral){
    var peripherals = this.state.peripherals;
    if (!peripherals.has(peripheral.id) && !peripherals.has(peripheral.name)){
      console.log('Got ble peripheral', peripheral);

      if(peripheral.name != null)
      {
        var sample = this.state.list;
        sample.push(peripheral);
        sample.map(ble=>{
          ble.enable=false;
        })
        this.setState({list:sample},()=>console.log('pp:', this.state.list))
      peripherals.set(peripheral.id, peripheral);

      console.log(peripherals)

      }
      this.setState({ peripherals })
    }
  }
*/

  handleDiscoverPeripheral(peripheral){
    var peripherals = this.state.peripherals;
    if (!peripherals.has(peripheral.id) && !peripherals.has(peripheral.name)){
      console.log('Got ble peripheral', peripheral);

      if(peripheral.name != null)
      {

        console.log('name',peripheral.name)
        let name = peripheral.name
        if(name!==undefined && (name.startsWith("HRM803") || name.startsWith("ID101"))){
          peripherals.set(peripheral.id, peripheral);
          var sample = this.state.list;
          sample.push(peripheral);
          sample.map(ble=>{
            ble.enable=false;
          })
          this.setState({list:sample},()=>console.log('pp:', this.state.list))
        }
        else{
          console.log('no')
        }

      }
      this.setState({ peripherals })
    }
  }


  _refresh () {

    return new Promise((resolve) => {

      setTimeout(()=>{
        resolve();
      }, 4000)


      BleManager.scan([], 3, true).then((results) => {
        console.log('Scanning...');
        ToastAndroid.show('Scanning....' , ToastAndroid.SHORT);
        //this.setState({scanning:true});
      }, err=>{
        console.log(err)
      }).catch((err)=>{
        console.log(err)
      });

    })
  }

  nextpage() {

        const { navigate } = this.props.navigation;

        navigate("Courses");
  }

  _saveBLE=async ()=>{
    var { enableBack } = this.props.navigation.state.params;
    console.log(this.state.selected[0])
    if(this.state.enable&&this.state.selected.length){
      const { navigate } = this.props.navigation;
      let USER_ID = await AsyncStorage.getItem("USER_ID");
      let USER_NAME = await AsyncStorage.getItem("USER_NAME");
      console.log(this.state.selected[0].id, this.state.selected[0].id)
      addAsyncStorage({"DEVICE_ID":this.state.selected[0].id, "DEVICE_NAME":this.state.selected[0].name, "USER_ID":await AsyncStorage.getItem("USER_ID"), "USER_NAME":await AsyncStorage.getItem("USER_NAME")})
      .then(res=>{
        console.log(res)
        if(res.status){
          ToastAndroid.show('Device Info Saved Successfully '+this.state.selected[0].name+'' , ToastAndroid.SHORT);
          navigate("DeviceConnected",{params:this.props.navigation.state.params})
        }else{
          ToastAndroid.show('Problem in storing '+this.state.selected.name[0]+'' , ToastAndroid.SHORT);
        }
      },err=>{
        console.log(err)
        ToastAndroid.show('Connection error.... Please Try To Again' , ToastAndroid.SHORT);
      })
    }else{
      ToastAndroid.show('Select any device', ToastAndroid.SHORT);
    }
  }
  onSelect(index, value){
    this.setState({selected:value, enable:true},()=>{console.log(this.state.selected, index, value)})
  }

render() {
  const {navigation} = this.props
  const resizeMode = 'center';
  const text = 'I am some centered text';
  const list = Array.from(this.state.peripherals.values());
  console.log(list)
  const dataSource = ds.cloneWithRows(list);
   console.log(dataSource);
   var { fromRoute } = this.state;
   console.log(fromRoute);
  return (

    <View style={styles.mainBody}>
      <View style={styles.chevron_left_icon}>
        <TouchableOpacity onPress={()=>{
          const { navigate } = this.props.navigation;
          navigate('Tabs')}}>
          <Icon name="chevron-left" size={25} color="#FF7E00"   />
        </TouchableOpacity>
      </View>
      <View style={styles.header}>
        <Text style={styles.topSignupTxt}>
          Connect device
        </Text>
        {String(fromRoute)==='ProfileDetails'||fromRoute==null||fromRoute=='Login'?
          <View style={{
            position:'absolute', right:20,
            paddingTop:15
          }}>
            <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('Tabs');
            }}>
              <Text style={{color:'#ff7200',
                  fontSize : 20,
                  fontWeight:'bold'
                }}>
                  Add later
                </Text>
            </TouchableOpacity>
          </View>
          :
          null
        }
      </View>
      <PTRView onRefresh={this._refresh} >
      <ScrollView
        style={{
          marginTop : 20,
          marginBottom:0,
          paddingBottom:60,

          width:Dimensions.get('window').width
        }}
      >
      <View>
      {list.length?
        <View>
        {list.map((ble, i)=>
        <View key={i}>
            <Text style={styles.connectr_name}>{ble.name}</Text>
            <View style={styles.connected_txt1Mas}>
                <Text style={styles.connected_txt1}></Text>
                <View style = {styles.container}>
                   <Switch
                      onValueChange = {()=>{
                        var array = this.state.list;
                        array.map((ble, j)=>{
                          if(i==j){
                            console.log(i+1,'is ', 'enabled',i,j)
                            array[j].enable=true;
                            var a=[];
                            a.push(ble)
                            this.setState({selected:a, enable:true})
                          }else{
                            console.log(i+1,'is ', 'disabled',i,j)
                            array[j].enable=false;
                          }
                        })

                        this.setState({list:array})
                      }}
                      value = {ble.enable}/>
                </View>
            </View>
        </View>
        )}
        </View>
        :
        <View style={styles.header}>
          <Text style={{color:'#F5F5FF', textAlign:'center', fontSize : 15, opacity:0.5, fontWeight:'bold', paddingTop:15}}>
            Turn on the bluetooth and pull down to scan
          </Text>
        </View>
      }

      </View>

      </ScrollView>

      </PTRView>
      <View style={styles.footer}>
            <TouchableOpacity onPress={()=>this._saveBLE()}>
              <View style={styles.save_view}>
                  <Text style={styles.save_btnTxt}>Save devices</Text>
              </View>

              <Image
                style={styles.save_btnImg}
                source={{ uri: 'buttonimg' }}
              />
              </TouchableOpacity>

        </View>

      </View>

  );
}
}
