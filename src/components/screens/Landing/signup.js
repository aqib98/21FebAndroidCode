import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { onSignIn,onSignOut, getAllAsyncStroage } from '../../../config/auth';
import { signin, getCourses } from '../../template/api.js'
import axios from 'axios';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from './styles.js';

FCM.on(FCMEvent.Notification, async (notif) => {
  console.log(notif);
  FCM.presentLocalNotification({
      id: Date.now().toString(),                               // (optional for instant notification)
      title: notif.fcm.title,                     // as FCM payload
      body: notif.fcm.body,                    // as FCM payload (required)
      sound: "default",                                   // as FCM payload
      priority: "high",                                   // as FCM payload
      click_action: "ACTION",                             // as FCM payload
      // badge: 10,                                          // as FCM payload IOS only, set 0 to clear badges
      // Android only
      auto_cancel: true,                                  // Android only (default true)
      icon: "ic_launcher",                                // as FCM payload, you can relace this with custom icon you put in mipmap                      // Android only
      color: "red",                                       // Android only
      vibrate: 300,                                       // Android only default: 300, no vibration if you pass 0
      lights: true,                                       // Android only, LED blinking (default false)
      show_in_foreground : true                                 // notification when app is in foreground (local & remote)
  });
  if(notif.opened_from_tray){
      console.log("Opened from Tray")
    }
});

FCM.on(FCMEvent.RefreshToken, (token) => {
  console.log(token)
});
var { width, height } = Dimensions.get('window');
export default class Signup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      showErrorUsername: false,
      showErrorPassword: false,
      errorUsernameMessage: "",
      errorPasswordMessage: "",
      loader:false
    };
  }

  componentWillMount(){
    getAllAsyncStroage()
    .then(items =>
      console.log(items)
    )
    .catch(err =>
      console.log(err)
    );
    FCM.requestPermissions().then(()=>console.log('granted')).catch(()=>console.log('notification permission rejected'));

    FCM.getFCMToken().then(token => {
        console.log(token)
        // store fcm token in your server
        this.setState({
          token : token
        })
        console.log(this.state.token);
    });
  }
  addBLERoute=async ()=>{
    const { navigate } = this.props.navigation;
    if(await AsyncStorage.getItem("DEVICE_NAME")&& await AsyncStorage.getItem("DEVICE_ID")){
      navigate('Tabs')
    }else{
      Alert.alert('Do you want to save BLE device?','You can add later also.',
        [
          {text: 'Add later', onPress: () => navigate('Tabs')},
          {text: 'Add', onPress: () => navigate('SearchDevices', {fromRoute:'Login', toRoute:'SearchDevices', enableBack:false})},
        ],
        { cancelable: false }
      )
    }
  }
/*authenticating starts*/
  _authenticateLoign = async () => {
    console.log(this.state)
    const { navigate } = this.props.navigation;
    Keyboard.dismiss();
    if(this.state.username!==""||this.state.password!=="")
    {
      this.setState({loader:true})
      signin(this.state.username, this.state.password)
      .then(response=>
      {
        console.log(response);
        if(response.status){
          this.setState({loader:false})
          ToastAndroid.show(response.show, ToastAndroid.SHORT);
          console.log("Notification Regitration started");
          axios.post("http://resoltz.azurewebsites.net/api/v1/notification/deviceregistration",{
            "user_id":response.data.id,
            "token" : this.state.token
          })
          .then((res)=>{
            console.log(res);
          })
          .catch((err)=>{
            console.log('error');
          });
          if(response.data.current_weight&&response.data.age&&response.data.height_in_feet&&response.data.gender){
            console.log("Profile completed")
            this.addBLERoute();
          }else{
            console.log("profile not completed")
            navigate("ProfileDetails", {title:'Complete your profile', fromRoute:'Login', editable:true});
          }
        }
        else{
          this.setState({loader:false})
          ToastAndroid.show(response.show, ToastAndroid.SHORT);
        }
      },error=>{
        console.log(error)
        this.setState({loader:false})
        ToastAndroid.show(error.show, ToastAndroid.SHORT);
      })
    }else if(this.state.loader){
      ToastAndroid.show('Please wait.. We are searching our database.', ToastAndroid.SHORT);
    }else{
      this.setState(
        {
          showErrorUsername:true,
          showErrorPassword:true,
          errorUsernameMessage:"Enter email",
          errorPasswordMessage:"Enter Password"
        }
      )
    }
  }
/*authenticating starts*/

  render(){
    const { navigate } = this.props.navigation;
    const {
      loader,
      username,
      password,
      showErrorUsername,
      showErrorPassword,
      errorUsernameMessage,
      errorPasswordMessage
    } = this.state;
    return(
      <View style={ styles.mainBody }>
        <View style={styles.header}>
          <Text style={styles.topSignupTxt}>
            Sign in
          </Text>
        </View>
        <View style={{
            position:'absolute', right:20,
            paddingTop:15
          }}>
            <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('Signup');
            }}>
              <Text style={{color:'#ff7200',
                  fontSize : 20,
                  fontWeight:'bold'
                }}>
                  Signup
                </Text>
            </TouchableOpacity>
          </View>
        <ScrollView style={{ marginTop : 20, marginBottom:0, paddingBottom:60, height:Dimensions.get('window').height, width:Dimensions.get('window').width }} >
          <View style={styles.body1}>
            <Text style={styles.forTxt}>For students,sign up using your school username</Text>
            <TextInput
              placeholder="Username"
              underlineColorAndroid='transparent'
              autoCorrect={false}
              placeholderTextColor='#626264'
              style={styles.textInput_login}
              onChangeText={ (text) => this.setState({username:text}) }
              onFocus={ () => this.setState({showErrorUsername:false,showErrorPassword:false}) }
            />
            {showErrorUsername?<Text style={{color:'#fb620c'}}>{errorUsernameMessage}</Text>:null}
            <TextInput
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              autoCorrect={false}
              placeholderTextColor='#626264'
              style={styles.textInput_login}
              onChangeText={ (text) => this.setState({password:text}) }
              onFocus={ () => this.setState({showErrorUsername:false,showErrorPassword:false}) }
            />
            {showErrorPassword?<Text style={{color:'#fb620c'}}>{errorPasswordMessage}</Text>:null}
            <Text style={styles.nonStudent}>Non students create an account using options below</Text>
            <TouchableOpacity  style={styles.submitFB}>
              <Icon name="facebook-f" size={30} color="#fff" style={{position:'absolute',left:25,top:17,fontSize:20}} />
              <Text style={styles.buttonText}>
                Sign up with Facebook
              </Text>
            </TouchableOpacity>
            <TouchableOpacity  style={styles.submitTW}>
              <Icon name="twitter" size={30} color="#fff" style={{position:'absolute',left:25,top:17,fontSize:20}} />
              <Text style={styles.buttonText}>
                Sign up with Twitter
              </Text>
            </TouchableOpacity>
            <TouchableOpacity  style={styles.submitGM}>
              <Icon name="google-plus" size={30} color="#fff" style={{position:'absolute',left:25,top:17,fontSize:20}} />
              <Text style={styles.buttonText}>
                Sign up with Gmail
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <View style={styles.footer}>
          <TouchableOpacity onPress={() => this._authenticateLoign() }>
            <View style={styles.save_view}>
              {loader?
                <ActivityIndicator
                  animating = {loader}
                  color = '#bc2b78'
                  size = "large"
                  style = {styles.activityIndicator}
                />
                :
                <Text style={styles.save_btnTxt}>Sign in</Text>
              }
            </View>
            <Image style={styles.save_btnImg} source={{ uri: 'buttonimg' }} />
         </TouchableOpacity>
        </View>
      </View>
    );
  }
}
