/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  ListView,
  ScrollView,
  AppState,
  TouchableOpacity, Image, ToastAndroid
} from 'react-native';
import Dimensions from 'Dimensions';
import BleManager from 'react-native-ble-manager';
import TimerMixin from 'react-timer-mixin';
import reactMixin from 'react-mixin';
import moment from "moment";
import { AsyncStorage } from "react-native";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import { addToSQLiteTable } from '../../template/SQLiteOperationsOffline.js'; 
import { addAsyncStorage } from '../../../config/auth.js'

const window = Dimensions.get('window');
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);


import PTRView from 'react-native-pull-to-refresh';

export default class ConnectToBle extends Component {


  constructor(){
    super()

    this.state = {
      scanning:false,
      selected:null,
      peripherals: new Map(),
      appState: '',
      heartrate: 0,
      screenchange: '0',
      time: moment().format("LTS"),
      date: moment().format("LL"),
      totsec: 2400,
      item: 0,
      array: [],
      sum: 0,
      average: 0,
      calories: 0,
      user_id:null,
      profile:[],
      device_id:null,
      device_name:null,
      height:'',
      weight:'',
      timepassed: false,
      rate: 1,
      volume: 1,
      duration: 0.0,
      currentTime: 0.0,
      paused: true,
      loader:false,
      times:0
    }

    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);
    
  }

  componentDidMount() {

    BleManager.start({showAlert: false});

    this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );
    this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan );
    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
        if (result) {
          console.log("Permission is OK");
        } else {
          PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
            if (result) {
              console.log("User accept");
            } else {
              console.log("User refuse");
            }
          });
        }
      });
    }
  }

  componentWillUnmount() {
    this.handlerDiscover.remove();
    this.handlerStop.remove();
  }

  handleStopScan() {
    console.log('Scan is stopped');
    this.setState({ scanning: false });
  }

  startScan() {
    if (!this.state.scanning) {
      BleManager.scan([], 30, true).then((results) => {
        console.log('Scanning...');
        ToastAndroid.show('Scanning....' , ToastAndroid.SHORT);
        this.setState({scanning:true});
      });
    }
  }

  handleDiscoverPeripheral(peripheral){
    var peripherals = this.state.peripherals;
    if (!peripherals.has(peripheral.id) && !peripherals.has(peripheral.name)){
      console.log('Got ble peripheral', peripheral);

      if(peripheral.name != null)
      {

      peripherals.set(peripheral.id, peripheral);

      console.log(peripherals)

      } 
      this.setState({ peripherals })
    }
  }

  _refresh () {
    
    return new Promise((resolve) => {

      setTimeout(()=>{        
        resolve(); 
      }, 4000)


      BleManager.scan([], 3, true).then((results) => {
        console.log('Scanning...');
        ToastAndroid.show('Scanning....' , ToastAndroid.SHORT);
        //this.setState({scanning:true});
      }, err=>{
        console.log(err)
      }).catch((err)=>{
        console.log(err)
      });

    })
  }

  nextpage() {

        const { navigate } = this.props.navigation;

        navigate("Courses");
  }

  _saveBLE=async ()=>{
    const { navigate } = this.props.navigation;
    let USER_ID = await AsyncStorage.getItem("USER_ID"); 
    let USER_NAME = await AsyncStorage.getItem("USER_NAME"); 
    if(this.state.enable){
        console.log(this.state.selected)
        addAsyncStorage({"DEVICE_ID":this.state.selected.id, "DEVICE_NAME":this.state.selected.name, "USER_ID":AsyncStorage.getItem("USER_ID"), "USER_NAME":await AsyncStorage.getItem("USER_NAME")})
        .then(res=>{
          console.log(res)
          if(res.status){
            ToastAndroid.show('Device Info Saved Successfully '+this.state.selected.name+'' , ToastAndroid.SHORT); 
            navigate("UpcomingWorkouts")
          }else{
            ToastAndroid.show('Problem in storing '+this.state.selected.name+'' , ToastAndroid.SHORT);
          }
        },err=>{
          console.log(err)
          ToastAndroid.show('Connection error.... Please Try To Again' , ToastAndroid.SHORT);
      })
    }else{
      ToastAndroid.show('Select any device', ToastAndroid.SHORT);
    } 
  }
  onSelect(index, value){
    this.setState({selected:value, enable:true},()=>{console.log(this.state.selected, index, value)})

  }
  render() {

    const list = Array.from(this.state.peripherals.values());
    const dataSource = ds.cloneWithRows(list);
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Connect Devices</Text>
        </View>


        <PTRView
          style={{backgroundColor:'#404040'}}
          onRefresh={this._refresh}
        >
          
        <ScrollView style={styles.scroll}>

          {list.length?
            <RadioGroup
              style={styles.radioGroup}
              onSelect = {(index, value) => this.onSelect(index, value)}
            >
              {list.map((ble, i)=>
              <RadioButton key={i} value={ble} style={styles.radioButton}>
                <Text>{ble.name}</Text>
              </RadioButton>
              )}
            </RadioGroup>
            :
            <View style={{flex:1, margin: 20}}>
              <Text style={{textAlign: 'center',color: 'white'}}>Turn on the bluetooth and pull down to scan</Text>
            </View>
          }
          
        </ScrollView>



        </PTRView>
        <Button
          title="Save Device"
          fontSize={20}
          disabledStyle={{backgroundColor:'#ef7e2d'}}
          backgroundColor="#ef7e2d"
          onPress={() => this._saveBLE()}
        />        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#000',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  header: {
    height: 60,
    borderColor: '#fff',
    backgroundColor: '#000000',
    borderBottomWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    color: '#fff',
    fontSize: 20,
    lineHeight: 40,
  },
  card: {
    flex: 1,
    borderColor: '#fafafa',
    backgroundColor: '#2196F3',
    borderWidth: 2,
    borderRadius: 3,
    margin: 5,
  },
  card__text: {
    color: '#fafafa',
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  loginButton: {
     position:'absolute',
     bottom: 0,
     height:60,
     backgroundColor:'#fff',
     justifyContent:'center',
     width: '100%'
   }  
});

