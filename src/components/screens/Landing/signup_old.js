import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  ToastAndroid,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

import { onSignIn } from '../../../config/auth';
import API from '../../template/constants.js';
import axios from 'axios';
import styles from './styles.js';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

export default class Signup_old extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      confirmPassword: "",
      first_name: "",
      last_name:"",
      username: "",
      showErrorEmail: false,
      showErrorPassword: false,
      showErrorConfirmPassword: false,
      showErrorFirstName: false,
      showErrorLastName:false,
      showErrorUsername: false,
      errorEmailMessage: "Check email",
      errorPasswordMessage: "Check password",
      errorConfirmPasswordMessage: "Check confirm password",
      errorUsernameMessage: "Check username",
      errorFirstNameMessage: "Check first name",
      errorLastNameMessage:"Check Last Name",
      loader:false

    };
  }
  componentWillMount(){
    console.log(this.props.navigation);
  }




/*authenticating starts*/
  _authenticateSignup = () => {
  if(this.state.first_name===""){
      console.log('empty firstname', this.state)
      this.setState(
        {
          showErrorEmail:false,
          showErrorPassword:false,
          showErrorConfirmPassword:false,
          showErrorFirstName:true,
          showErrorUserName:false,
          showErrorLastName:false
        }
      )
  }else if(this.state.last_name===""){
      console.log('empty firstname', this.state)
      this.setState(
        {
          showErrorEmail:false,
          showErrorPassword:false,
          showErrorConfirmPassword:false,
          showErrorFirstName:false,
          showErrorUserName:false,
          showErrorLastName:false,
          showErrorLastName:true
        }
      )
  }else if(this.state.username==="") {
      console.log('empty username', this.state)
      this.setState(
        {
          showErrorEmail:false,
          showErrorPassword:false,
          showErrorConfirmPassword:false,
          showErrorFirstName:false,
          showErrorUserName:true,
          showErrorLastName:false
        }
      )
  }
  else if(this.state.email==="") {
      console.log('empty email', this.state)
      this.setState(
        {
          showErrorEmail:true,
          showErrorPassword:false,
          showErrorConfirmPassword:false,
          showErrorFirstName:false,
          showErrorUserName:false,
          showErrorLastName:false
        }
      )
  }
  else if(this.state.password===""){
      console.log('empty password', this.state)
      this.setState(
        {
          showErrorEmail:false,
          showErrorPassword:true,
          showErrorConfirmPassword:false,
          showErrorFirstName:false,
          showErrorUserName:false,
          showErrorLastName:false
        }
      )
  }
  else if(this.state.confirmPassword===""){
      console.log('empty confirmPassword', this.state)
      this.setState(
        {
          showErrorEmail:false,
          showErrorPassword:false,
          showErrorConfirmPassword:true,
          showErrorFirstName:false,
          showErrorUserName:false,
          errorConfirmPasswordMessage: "Check confirm password",
          showErrorLastName:false
        }
      )
  } else {

      if(this.state.password===this.state.confirmPassword)
      {
        this.setState({loader:true});
        console.log({
              "ht":this.state.password===this.state.confirmPassword,
              "f":this.state.confirmPassword,
              "name":  this.state.first_name,
              "username": this.state.username,
              "email":  this.state.email,
              "type":  'S',
              "password":  this.state.password
          })
        axios.post(API.Signup,{
            "name":  this.state.first_name,
            "username": this.state.username,
            "email":  this.state.email,
            "type":  'S',
            "password":  this.state.password
        })
        .then((res)=>{
          if(res.status==200){
            this.setState({loader:false})
            ToastAndroid.show('Successfully Registered, Login to continue', ToastAndroid.SHORT);
            const { navigate } = this.props.navigation;
            navigate('Login')
          }else if(res.status==500)
          {
            this.setState({loader:false})
            ToastAndroid.show(res.message, ToastAndroid.SHORT);
          }
        })
        .catch((err)=>{
          this.setState({loader:false})
          ToastAndroid.show('error', ToastAndroid.SHORT);
          console.log(err,'error');
          this.resetField();

        });
      }else
      {
          console.log('Password not match', this.state)
          this.setState(
          {
            showErrorConfirmPassword:true,
            errorConfirmPasswordMessage:"Password not match"
          }
        )
      }
    }
/*const { navigate } = this.props.navigation;
          navigate('Login')*/
  }
/*authenticating ends*/
  resetField=()=>{
    console.log('resetting fields')
    this.setState({
      email: "",
      password: "",
      confirmPassword: "",
      first_name: "",
      last_name:"",
      username: "",
      showErrorEmail: false,
      showErrorPassword: false,
      showErrorConfirmPassword: false,
      showErrorFirstName: false,
      showErrorLastName:false,
      showErrorUsername: false
    },()=>console.log(this.state))
  }


  render(){
    const {
      email,
      password,
      confirmPassword,
      first_name,
      last_name,
      username,
      showErrorEmail,
      showErrorPassword,
      showErrorConfirmPassword,
      showErrorFirstName,
      showErrorLastName,
      showErrorUsername,
      errorEmailMessage,
      errorPasswordMessage,
      errorConfirmPasswordMessage,
      errorUsernameMessage,
      errorFirstNameMessage,
      errorLastNameMessage,
      loader
    } = this.state;
    const { navigate } = this.props.navigation;
    return(
      <View style={styles.mainBody}>
        <View style={styles.header}>
          <Text style={styles.topSignupTxt}>
            Sign up
          </Text>
        </View>
        <View style={{
            position:'absolute', right:20,
            paddingTop:15
          }}>
            <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('Login');
            }}>
              <Text style={{color:'#ff7200',
                  fontSize : 20,
                  fontWeight:'bold'
                }}>
                  Signin
                </Text>
            </TouchableOpacity>
          </View>
        <ScrollView style={{ marginTop : 20, marginBottom:0, paddingBottom:60, height:Dimensions.get('window').height, width:Dimensions.get('window').width }} >
          <KeyboardAvoidingView behavior="padding">
            <View style={styles.signup_temp_form}>
              <View style={styles.body1a}>
                <TextInput
                  placeholder="First Name"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  placeholderTextColor='#626264'
                  value={first_name}
                  style={styles.textInput_signup}
                  onFocus={ () => this.setState({showErrorFirstName:false}) }
                  onChangeText={ (text) => this.setState({first_name:text}) }
                />
                {showErrorFirstName?
                  <Text style={{color:'#fb620c'}}>
                    {errorFirstNameMessage}
                  </Text>
                  :
                  null
                }
                <TextInput
                  placeholder="Last Name"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  placeholderTextColor='#626264'
                  value={last_name}
                  style={styles.textInput_signup}
                  onFocus={ () => this.setState({showErrorLastName:false}) }
                  onChangeText={ (text) => this.setState({last_name:text}) }
                />
                {showErrorLastName?
                  <Text style={{color:'#fb620c'}}>
                    {errorLastNameMessage}
                  </Text>
                  :
                  null
                }
                <TextInput
                  placeholder="Username"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  placeholderTextColor='#626264'
                  value={username}
                  style={styles.textInput_signup}
                  onFocus={ () => this.setState({showErrorUserName:false}) }
                  onChangeText={ (text) => this.setState({username:text}) }
                />
                {showErrorUsername?
                  <Text style={{color:'#fb620c'}}>
                    {errorUsernameMessage}
                  </Text>
                  :
                  null
                }
                <TextInput
                  placeholder="Email@school.edu"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  placeholderTextColor='#626264'
                  value={email}
                  style={styles.textInput_signup}
                  onFocus={ () => this.setState({showErrorEmail:false}) }
                  onChangeText={ (text) => this.setState({email:text}) }
                />
                {showErrorEmail?
                  <Text style={{color:'#fb620c'}}>
                    {errorEmailMessage}
                  </Text>
                  :
                  null
                }
                <TextInput
                  placeholder="Password"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  placeholderTextColor='#626264'
                  value={password}
                  style={styles.textInput_signup}
                  onFocus={ () => this.setState({showErrorPassword:false}) }
                  onChangeText={ (text) => this.setState({password:text}) }
                />
                {showErrorPassword?
                  <Text style={{color:'#fb620c'}}>
                    {errorPasswordMessage}
                  </Text>
                  :
                  null
                }
                <TextInput
                  placeholder="Confirm Password"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  placeholderTextColor='#626264'
                  value={confirmPassword}
                  style={styles.textInput_signup}
                  onFocus={ () => this.setState({showErrorConfirmPassword:false}) }
                  onChangeText={ (text) => this.setState({confirmPassword:text}) }
                />
                {showErrorConfirmPassword?
                  <Text style={{color:'#fb620c'}}>
                    {errorConfirmPasswordMessage}
                  </Text>
                  :
                  null
                }
              </View>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
        <View style={styles.footer}>
          <TouchableOpacity onPress={ () => this._authenticateSignup() }>
            <View style={styles.save_view}>
            {loader?
                <ActivityIndicator
                  animating = {this.state.loader}
                  color = '#bc2b78'
                  size = "large"
                  style = {styles.activityIndicator}
                />
                :
              <Text style={styles.save_btnTxt}>Signup</Text>}
            </View>
            <Image
              style={styles.save_btnImg}
              source={{ uri: 'buttonimg' }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
