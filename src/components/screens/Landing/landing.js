
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  ToastAndroid,
  Keyboard,
  AsyncStorage,
  Dimensions
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { NavigationActions } from 'react-navigation';
import { onSignIn,onSignOut } from '../../../config/auth';
import { signin, getCourses } from '../../template/api.js';
import axios from 'axios';
import styles from './styles.js'
var { width, height } = Dimensions.get('window');

export default class Landing extends Component {

  constructor(props) {
    super(props);
  }

  componentWillMount(){
    console.log("--Landed--")
  }

  nextStep() {
      const { navigate } = this.props.navigation;
      navigate("Signup_new")
  }

  render(){
    const { navigate } = this.props.navigation;
    const { container } = style;
    return (
      <View style={{ backgroundColor: '#2d2e37', marginTop : 0, padding:0, height: Dimensions.get('window').height, width: Dimensions.get('window').width }}>
        <View style={{ position: 'absolute', top: 0, left: 0, width: undefined, height: undefined }}>
          <Image style={{ resizeMode: 'stretch',
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
            }}
            source={{ uri: 'signinbg' }}
          />
        </View>
        <View style = {{marginTop : '14%',justifyContent:'center',alignItems:'center'}}>
          <Image style={{ resizeMode: 'contain', width:140, height:30 }} source={{ uri: 'signinlogo' }} />
        </View>
        <View style = {{marginTop : '86%',alignItems:'center',justifyContent:'center'}}>
          <View style={{ alignItems:'center',justifyContent:'center'}}>
            <View>
              <Text style = {styles.textStyle}>Get inspired with our challenges quotes and friends workouts</Text>
            </View>
          </View>
        </View>
        <View style ={{ alignItems:'center',justifyContent:'center', position:'absolute', bottom:0, left:0, right:0}}>
          <View>
            <TouchableOpacity onPress={()=>this.nextStep()}>
              <Image style={{ resizeMode: 'contain', width:230, height:80 }} source={{ uri: 'signupbtn' }} />
            </TouchableOpacity>
          </View>
          <View style ={{ alignItems:'center',justifyContent:'center'}}>
            <Text style={styles.textStyle}>
              <Text> I have an account </Text>
              <Text style = {{ fontWeight:'bold', color:'#fb620c'}} onPress={()=>navigate("Login")}>
                Sign In
              </Text>
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
 const style = StyleSheet.create({
  container:{
    flex:1,

  },
  box:{

  }
 });
