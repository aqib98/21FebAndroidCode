import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator, Picker, DatePickerIOS
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { onSignIn,onSignOut, getAllAsyncStroage } from '../../../config/auth';
import { signin, getCourses } from '../../template/api.js'
import axios from 'axios';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from './styles.js';
import moment from "moment";

import Toast, {DURATION} from 'react-native-easy-toast'
import StatusBarBackground from './statusbar.js'

const {height, width} = Dimensions.get('window');

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');


export default class SelectDataofBirth extends Component {

  constructor(props) {
    super(props);
    this.state = { chosenDate: new Date() };

    this.setDate = this.setDate.bind(this);
  }

  componentWillMount() {
    var { date  } = this.props.navigation.state.params;

    if(date != null){
      date = date.split("/");
      let newdate = new Date(date[2], date[1]-1, date[0])
      //alert(newdate)
      this.setState({ chosenDate: newdate })
    }

  }

  setDate(newDate) {
    this.setState({chosenDate: newDate})
    //alert(this.state.chosenDate)
  }

  setdateofbirth = (date) => {

    

  let now  = new Date();
  let then = new Date(date)

  let day = then.toLocaleDateString()

  let month = then.getMonth()

  let year = then.getYear()

  let selecteddate = month +'-'+day+'-'+year

  let diff = moment(now,"DD/MM/YYYY").diff(moment(then,"DD/MM/YYYY"));

  var diffMs = (now-then); // milliseconds between now & Christmas
  var diffDays = (Math.floor(diffMs / 86400000) / 365); // days

  if(diffDays < 15 )
  {
      alert("You are not eligible to use this app")
  }
  else {
      const { navigate } = this.props.navigation;
      navigate("ProfileDetails1", {date: day})
  }

  }


  render(){
    const { navigate } = this.props.navigation;
    console.log(this.state.chosenDate)
    return(
      <View style={ styles.mainBody }>
        <StatusBarBackground style={{backgroundColor:'#ff7200'}}/>
          <View style={styles.chevron_left_icon}>
            <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('ProfileDetails1', { date: null})}}>
              <Icon name="chevron-left" size={25} color="#FF7E00"   />
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <Text style={styles.topSignupTxt}>
              Select date of birth
            </Text>
          </View>


          <View>
              <DatePickerIOS
                date={this.state.chosenDate}
                onDateChange={this.setDate}
                mode={'date'}
                style={{color: '#fff'}}
              />
          </View>


          <View style={{bottom:20,position:'absolute',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity onPress={()=>this.nextStep()}>
                  <View style={{zIndex:999,alignItems:'center',justifyContent:'center',height:50,width:width}}>


                      <Text style={{backgroundColor:'transparent',alignSelf:'center',fontFamily:'CircularStd-Black',color:'#fff',fontSize:19}}>Save and continue</Text>

                  </View>
                  <Image style={{width:width,height:50,position:'absolute',bottom:0}} source={{ uri: 'buttonimg' }}/>
              </TouchableOpacity>
          </View>


      </View>
    );
  }
}
