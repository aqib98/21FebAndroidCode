import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { onSignIn,onSignOut, getAllAsyncStroage } from '../../../config/auth';
import { signin, getCourses } from '../../template/api.js'
import axios from 'axios';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from './styles.js';
import API from '../../template/constants.js';

import Toast, {DURATION} from 'react-native-easy-toast'
import StatusBarBackground from './statusbar.js'
const {height, width} = Dimensions.get('window');

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default class UserInfo2 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      password: null,
      confirmpassword: null,
      schoolname: null,
      loader:false,
      schoolid: null,
      userid: null,
      contentLoader: false
    };
  }

  componentWillMount(){

    const { password, confirmpassword, schoolid, schoolname } = this.props.navigation.state.params;

    if(password != null) {
      this.setState({password: password})
    }

    if(confirmpassword != null) {
      this.setState({confirmpassword: confirmpassword})
    }

    if(schoolid != null) {
      this.setState({schoolid: schoolid})
    }

    if(schoolname != null) {
      this.setState({schoolname: schoolname})
    }

  }


  nextstep = async() => {



    this.setState({contentLoader: true})

    const { password, confirmpassword, schoolid } = this.state

    let USER_ID = await AsyncStorage.getItem("USER_ID");

    if(password != null && confirmpassword != null && schoolid != null) {

      if(password == confirmpassword) {

            data = {
                "password": password,
                "school_id": schoolid,
                "id": USER_ID
             }

            axios.put(API.UpdateProfile, data).then( async response => {
              if(response.status==200){
                  console.log(response)
                  const { navigate } = this.props.navigation;
                  this.setState({contentLoader: false})
                  alert('Updated')
                  navigate("ProfileFill", { userinfo: 0, profiledetails: 1, pickclass: 0 })
                }
              }).catch(err => {
                console.log(err)
              })

        /*

        const { navigate } = this.props.navigation;

        navigate("ProfileFill", { userinfo: 0, profiledetails: 1, pickclass: 0 })

        */

      } else {

        alert(" Password && Confirmpassword are not same ")

        this.setState({contentLoader: false})

      }

    } else {


      alert(" Please all Fields ")

      this.setState({contentLoader: false})
    }


  }

  gotoschool = () => {
    const { navigate } = this.props.navigation;
    navigate("PickSchool", {password: this.state.password, confirmpassword: this.state.confirmpassword})
  }

  render(){

    const { navigate } = this.props.navigation;
    const {
      loader,
      username,
      password,
      showErrorUsername,
      showErrorPassword,
      errorUsernameMessage,
      errorPasswordMessage,
      contentLoader
    } = this.state;

    return(
      <View style={ styles.mainBody }>
        <StatusBarBackground style={{backgroundColor:'#ff7200'}}/>
          <View style={styles.chevron_left_icon}>
            <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('UserInfo1')}}>
              <Icon name="chevron-left" size={25} color="#FF7E00"/>
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <Text style={styles.topSignupTxt}>
              User Info (2/2)
            </Text>
          </View>

          {contentLoader?
            <ActivityIndicator
                animating = {this.state.contentLoader}
                color = '#bc2b78'
                size = "large"
                style = {styles.activityIndicator}
            />
          :

          <View style={{marginTop: 50}}>
            <View style={styles.body1}>
            <View style={{borderBottomWidth: 1, paddingBottom: 30}}>
              <View style={{marginTop: 25, marginBottom: 25}}>
                <TextInput
                  placeholder="Password"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  placeholderTextColor='#626264'
                  value={this.state.password}
                  secureTextEntry={true}
                  style={styles.textInput_login}
                  onChangeText={ (text) => this.setState({password:text}) }
                  onFocus={ () => this.setState({showErrorUsername:false,showErrorPassword:false}) }
                />
                {showErrorUsername?<Text style={{color:'#fb620c'}}>{errorUsernameMessage}</Text>:null}
              </View>

              <View style={{marginTop: 25, marginBottom: 25 }}>
                <TextInput
                  placeholder="Confirm Password"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  placeholderTextColor='#626264'
                  secureTextEntry={true}
                  style={styles.textInput_login}
                  value={this.state.confirmpassword}
                  onChangeText={ (text) => this.setState({confirmpassword:text}) }
                  onFocus={ () => this.setState({showErrorUsername:false,showErrorPassword:false}) }
                />
                {showErrorUsername?<Text style={{color:'#fb620c'}}>{errorUsernameMessage}</Text>:null}
              </View>
            </View>

              <View style={{marginTop: 65, marginBottom: 25}}>
                <TouchableOpacity onPress={()=> this.gotoschool()}>
                <View style={{width: width, height: 80}}>
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{width: width-50, height: 50 }}>
                      {this.state.schoolname==null?
                      <Text style={{textAlign: "center", marginTop: 6, fontSize: 20, width: '100%', marginLeft: 6, color: "#fff"}}>Pick school</Text>
                      :
                      <Text style={{textAlign: "center", marginTop: 6, fontSize: 20, width: '100%', marginLeft: 6, color: "#fff"}}>{this.state.schoolname}</Text>
                      }
                    </View>
                    <View style={{width: 50, height: 50, marginTop: 10}}><Icon name="chevron-right" size={25} color="#FF7E00" /></View>
                  </View>
                </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>

        }



          <View style={{bottom:20,position:'absolute',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity onPress={()=>this.nextstep()}>
                  <View style={{zIndex:999,alignItems:'center',justifyContent:'center',height:50,width:width}}>


                      <Text style={{backgroundColor:'transparent',alignSelf:'center',fontFamily:'CircularStd-Black',color:'#fff',fontSize:19}}>Save and continue</Text>

                  </View>
                  <Image style={{width:width,height:50,position:'absolute',bottom:0}} source={{ uri: 'buttonimg' }}/>
              </TouchableOpacity>
          </View>



      </View>
    );
  }
}
