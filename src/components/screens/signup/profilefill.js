import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { onSignIn,onSignOut, getAllAsyncStroage } from '../../../config/auth';
import { signin, getCourses } from '../../template/api.js'
import axios from 'axios';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from './styles.js';

import Toast, {DURATION} from 'react-native-easy-toast'
import StatusBarBackground from './statusbar.js'
const {height, width} = Dimensions.get('window');

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default class ProfileFill extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      showErrorUsername: false,
      showErrorPassword: false,
      errorUsernameMessage: "",
      errorPasswordMessage: "",
      loader:false,
      userinfo: 0,
      profiledetails: 0,
      pickclass: 0,
    };
  }

  componentWillMount(){

    const { userinfo, profiledetails, pickclass } = this.props.navigation.state.params

    if(userinfo != null) {
      this.setState({userinfo: userinfo})
    }
    if(profiledetails != null) {
      this.setState({profiledetails: profiledetails})
    }
    if(pickclass != null) {
      this.setState({pickclass: pickclass})
    }

  }

  nextStep = () => {
    const { userinfo, profiledetails, pickclass } = this.state
    const { navigate } = this.props.navigation;

    if( userinfo == 1) {
      navigate("UserInfo1")
    } else if (profiledetails == 1) {
      navigate('ProfileDetails1', { date: null, feet: null, inches: null, height_in_metric: null})
    } else {
      navigate("Courses")
    }

  }

      backStep = () => {
          const { navigate } = this.props.navigation;
          navigate("Signup_new")
      }



  render(){
    const { navigate } = this.props.navigation;
    const {
      loader,
      username,
      password,
      showErrorUsername,
      showErrorPassword,
      errorUsernameMessage,
      errorPasswordMessage,
      userinfo,
      pickclass,
      profiledetails
    } = this.state;

    //this.state.userinfo = 1

    return(
      <View style={ styles.mainBody }>
        <StatusBarBackground style={{backgroundColor:'#ff7200'}}/>
          <View style={styles.chevron_left_icon}>
            <TouchableOpacity onPress={()=>this.backStep()}>
              <Icon name="chevron-left" size={25} color="#FF7E00"   />
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <Text style={styles.topSignupTxt}>
              School sign up
            </Text>
          </View>
            <View style={{marginTop: 20}}>
              <View style={styles.body1}>
                <View style={{}}>
                  <Text style={styles.forTxtHeader}>Welcome to the Resoltz app!</Text>
                  <Text style={styles.forTxt1}>For students,sign up using your school email For students, sign up using your school email, For students,sign up using your school email</Text>
                  <Text style={styles.forTxt2}>For students,sign up using your school email</Text>
                </View>
              </View>
            </View>

            <View style={{flex: 1,flexDirection: 'column',alignItems: 'flex-start', marginTop: 20}}>

            {!!this.state.userinfo == 1 ?
              <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('UserInfo1')}}>
              <View style={{width: width, height: 80, }}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                  <View style={{borderWidth: 3, borderRadius: 25, borderColor: "#fff", width: 50, height: 50, marginLeft: 20}}>
                    <Text style={{textAlign: "center", marginTop: 6, fontSize: 25, width: 20, marginLeft: 12, color: "#fff"}}>1</Text>
                  </View>
                  <View style={{width: 150, height: 50 }}>
                    <Text style={{textAlign: "left", fontWeight: 'bold', marginTop: 10, fontSize: 25, width: '100%', marginLeft: -36, color: "#fff"}}>User Info</Text>
                  </View>
                  <View style={{width: 50, height: 50, marginTop: 15}} >
                    <Icon name="chevron-right" size={25} color="#FF7E00"/>
                  </View>
                </View>
              </View>
              </TouchableOpacity>
              :
              <View style={{width: width, height: 80, }}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                  <View style={{borderWidth: 3, borderRadius: 25, borderColor: "#6B6464", width: 50, height: 50, marginLeft: 20}}>
                    <Text style={{textAlign: "center", marginTop: 6, fontSize: 25, width: 20, marginLeft: 12, color: "#6B6464"}}>1</Text>
                  </View>
                  <View style={{width: 250, height: 50 }}>
                    <Text style={{textAlign: "left", fontWeight: 'bold', marginTop: 10, fontSize: 25, width: '100%', marginLeft: -36, color: "#6B6464"}}>User Info</Text>
                  </View>
                </View>
              </View>
            }
            {!!this.state.profiledetails == 1 ?
              <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('ProfileDetails1', { date: null, feet: null, inches: null, height_in_metric: null})}}>
              <View style={{width: width, height: 80}}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                  <View style={{borderWidth: 3, borderRadius: 25, borderColor: "#fff", width: 50, height: 50, marginLeft: 20}}>
                    <Text style={{textAlign: "center", marginTop: 6, fontSize: 25, width: 20, marginLeft: 12, color: "#fff"}}>2</Text>
                  </View>
                  <View style={{width: 150, height: 50 }}>
                    <Text style={{textAlign: "left", marginTop: 6, fontSize: 25, width: '100%', marginLeft: -36, color: "#fff"}}>Profile Details</Text>
                  </View>
                  <View style={{width: 50, height: 50, marginTop: 15}}><Icon name="chevron-right" size={25} color="#FF7E00"/></View>
                </View>
              </View>
              </TouchableOpacity>
              :
              <View style={{width: width, height: 80}}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                  <View style={{borderWidth: 3, borderRadius: 25, borderColor: "#6B6464", width: 50, height: 50, marginLeft: 20}}>
                    <Text style={{textAlign: "center", marginTop: 6, fontSize: 25, width: 20, marginLeft: 12, color: "#6B6464"}}>2</Text>
                  </View>
                  <View style={{width: 250, height: 50 }}>
                    <Text style={{textAlign: "left", marginTop: 6, fontSize: 25, width: '100%', marginLeft: -36, color: "#6B6464"}}>Profile Details</Text>
                  </View>
                </View>
              </View>
            }
            {!!this.state.pickclass == 1 ?
              <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('Courses')}}>
              <View style={{width: width, height: 80}}>
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                  <View style={{borderWidth: 3, borderRadius: 25, borderColor: "#fff", width: 50, height: 50, marginLeft: 20}}>
                    <Text style={{textAlign: "center", marginTop: 6, fontSize: 25, width: 20, marginLeft: 12, color: "#fff"}}>3</Text>
                  </View>
                  <View style={{width: 150, height: 50 }}>
                    <Text style={{textAlign: "left", marginTop: 10, fontSize: 25, width: '100%', marginLeft: -36, color: "#fff"}}>Pick Class</Text>
                  </View>
                  <View style={{width: 50, height: 50, marginTop: 15}}><Icon name="chevron-right" size={25} color="#FF7E00"   /></View>
                  </View>
              </View>
              </TouchableOpacity>
              :
              <View style={{width: width, height: 80}}>
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                  <View style={{borderWidth: 3, borderRadius: 25, borderColor: "#6B6464", width: 50, height: 50, marginLeft: 20}}>
                    <Text style={{textAlign: "center", marginTop: 6, fontSize: 25, width: 20, marginLeft: 12, color: "#6B6464"}}>3</Text>
                  </View>
                  <View style={{width: 250, height: 50 }}>
                    <Text style={{textAlign: "left", marginTop: 10, fontSize: 25, width: '100%', marginLeft: -36, color: "#6B6464"}}>Pick Class</Text>
                  </View>
                  </View>
              </View>
            }
            </View>

          <View style={{bottom:20,position:'absolute',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity onPress={()=>this.nextStep()}>
                  <View style={{zIndex:999,alignItems:'center',justifyContent:'center',height:50,width:width}}>


                      <Text style={{backgroundColor:'transparent',alignSelf:'center',fontFamily:'CircularStd-Black',color:'#fff',fontSize:21}}>Save</Text>

                  </View>
                  <Image style={{width:width,height:50,position:'absolute',bottom:0}} source={{ uri: 'buttonimg' }}/>
              </TouchableOpacity>
          </View>



      </View>
    );
  }
}
