import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { onSignIn,onSignOut, getAllAsyncStroage } from '../../../config/auth';
import { signin, getCourses } from '../../template/api.js'
import axios from 'axios';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from './styles.js';
import { Slider } from 'react-native-elements'
import API from '../../template/constants.js';
import Toast, {DURATION} from 'react-native-easy-toast'
import StatusBarBackground from './statusbar.js'
const {height, width} = Dimensions.get('window');

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default class ProfileDetails2 extends Component {


    constructor(props) {
      super(props);
      this.state = {
        weight: null,
        weight_test:null,
        feet: null,
        height: null,
        system: 0,
        feet:null,
        inches:null,
        showBMR: false,
        contentLoader: false
      };
    }

    componentWillMount(){

      var { feet, inches, weight, system, height_in_metric, weight_in_kgs  } = this.props.navigation.state.params;

      console.log(this.props)

      if(feet != null){
        this.setState({feet: feet})
        this.setState({feet_test:feet})
      }

      if(inches != null){
        this.setState({inches: inches})
        this.setState({inches_test : inches})
      }

      if(weight != null){
        this.setState({weight: weight})
        this.setState({weight_test:weight})
      }

      if(system != null){
        this.setState({system: system})
      }

      if(height_in_metric != null){
        this.setState({height_in_metric: height_in_metric})
        this.setState({height_in_metric_test:height_in_metric})
      }

      if(weight_in_kgs != null){
        this.setState({weight_in_kgs: weight_in_kgs})
        this.setState({weight_in_kgs_test:weight_in_kgs})
      }

    }

    nextStep =async() => {

      this.setState({contentLoader: true})

      const { feet, inches, weight_in_kgs, weight, height, height_in_metric, system } = this.state

      console.log(this.state)

      let USER_ID = await AsyncStorage.getItem("USER_ID");

      if(((feet !=null || inches !=null) || height_in_metric!=null) && (weight != null || weight_in_kgs != null)) {

        if(system == 0) {
          data = { "height_in_feet": feet, "height_in_inches": inches, "current_weight" : weight, id: USER_ID, parameter_type: system}
        } else {
          data = { "height_in_centimeter": height_in_metric,  "current_weight" : weight_in_kgs, id: USER_ID, parameter_type: system}
        }

        console.log(data)



          axios.put(API.UpdateProfile, data).then( async response => {
            if(response.status==200){
                console.log(response)

                alert('Details Updated')

                const { navigate } = this.props.navigation;

                navigate("ProfileFill", { userinfo: 0, profiledetails: 0, pickclass: 1 })

              }
            }).catch(err => {
              console.log(err)
          })



      } else {
        alert('Please Fill the fields')
        this.setState({contentLoader: false})
      }


    }

    systemChange = (value) => {
      console.log('value',value)
      this.setState({system: value})
      this.setState({feet: null})
      this.setState({inches: null})
      this.setState({weight: null})
      this.setState({weight_in_kgs: null})
      this.setState({height_in_metric: null})

    }

    BmrCaliculate = () =>{

      if(this.state.system == 1 && this.state.weight_in_kgs !=null && this.state.height_in_metric!=null){
        var BMR = 10 * this.state.weight_in_kgs + 6.25 * this.state.height_in_metric - 5 * 27 + 5 // BMR formula

        this.setState({showBMR: true})

      } else if(this.state.feet !=null && this.state.inches !=null && this.state.weight !=null) {

        var height_in_cms = (this.state.feet * 30.48) + (this.state.inches * 2.54)

        var weight_in_lbs = this.state.weight*0.45359

        var BMR = 10 * weight_in_lbs + 6.25 * height_in_cms - 5 * 27 + 5 // BMR formula

        this.setState({showBMR: true})
      }
    }


  render(){




    return(
      <View style={ styles.mainBody }>
        <StatusBarBackground style={{backgroundColor:'#ff7200'}}/>
          <View style={styles.chevron_left_icon}>
            <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('ProfileDetails1' , { date: null, feet: null, inches: null, height_in_metric: null, weight: null, weight_in_kgs: null})}}>
              <Icon name="chevron-left" size={25} color="#FF7E00"   />
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <Text style={styles.topSignupTxt}>
              Profile details (2/2)
            </Text>
          </View>
          <View style={{flex: 1, flexDirection: 'column'}}>
              <View style={{height: (height-140)/4, borderBottomWidth: 1}}>
              <View style={{flex: 1, flexDirection: 'row', marginTop: 40, alignItems: 'stretch', justifyContent: 'center', width: 200, alignSelf: 'center'}}>
                <View style={{width: width/4}}>
                  <Text style={{textAlign: "center", marginTop: 10, fontSize: 15, marginLeft: 0, color: "#fff"}}>Imperial</Text>
                </View>
                <View style={{width: width/2}}>

                  <Slider
                  value={this.state.system}
                  step={1}
                  minimumValue={0}
                  maximumValue={1}
                  onValueChange={(value) => this.systemChange(value)} />

                </View>
                <View style={{width: width/4}}>
                  <Text style={{textAlign: "center", marginTop: 10, fontSize: 15,  marginLeft: 0, color: "#fff"}}>Metric</Text>
                </View>

              </View>
              </View>
             <View style={{height: (height-140)/4, borderBottomWidth: 1}}>
                <TouchableOpacity onPress={()=>{
                const { navigate } = this.props.navigation;
                navigate('SelectHeight', { feet: this.state.feet, inches: this.state.inches, system: this.state.system, weight: this.state.weight, height_in_metric: this.state.height_in_metric, weight_in_kgs: this.state.weight_in_kgs})}}>
                <View style={{width: width, height: 80}}>
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{width: width-50, height: 50 }}>
                    { ( this.state.feet !=null ) ?
                      <Text style={{textAlign: "center", marginTop: 55, fontSize: 20, width: '100%', marginLeft: 6, color: "#fff"}}> {this.state.feet}{this.state.inches} </Text>
                     :
                      ( this.state.height_in_metric !=null ) ?
                      <Text style={{textAlign: "center", marginTop: 55, fontSize: 20, width: '100%', marginLeft: 6, color: "#fff"}}> { this.state.height_in_metric } cms</Text>
                      :
                      <Text style={{textAlign: "center", marginTop: 55, fontSize: 20, width: '100%', marginLeft: 6, color: "#fff"}}> Height </Text>
                    }
                    </View>
                    <View style={{width: 50, height: 50, marginTop: 60}}><Icon name="chevron-right" size={25} color="#FF7E00"   /></View>
                  </View>
                </View>
                </TouchableOpacity>
              </View>
              <View style={{height: (height-140)/4, borderBottomWidth: 1}}>
                <TouchableOpacity onPress={()=>{
                const { navigate } = this.props.navigation;
                navigate('SelectWeight', { feet: this.state.feet, inches: this.state.inches, system: this.state.system, weight: this.state.weight, height_in_metric: this.state.height_in_metric, weight_in_kgs: this.state.weight_in_kgs})}}>
                <View style={{width: width, height: 80}}>
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{width: width-50, height: 50 }}>
                    {this.state.weight !=null?
                      <Text style={{textAlign: "center", marginTop: 55, fontSize: 20, width: '100%', marginLeft: 6, color: "#fff"}}> {this.state.weight} lbs </Text>
                     :
                     ( this.state.weight_in_kgs !=null ) ?
                      <Text style={{textAlign: "center", marginTop: 55, fontSize: 20, width: '100%', marginLeft: 6, color: "#fff"}}> {this.state.weight_in_kgs} Kgs </Text>
                     :
                      <Text style={{textAlign: "center", marginTop: 55, fontSize: 20, width: '100%', marginLeft: 6, color: "#fff"}}> Weight </Text>
                    }
                    </View>
                    <View style={{width: 50, height: 50, marginTop: 60}}><Icon name="chevron-right" size={25} color="#FF7E00" /></View>
                  </View>
                </View>
                </TouchableOpacity>
               </View>

            </View>

          <View style={{bottom:20,position:'absolute',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity onPress={()=>this.nextStep()}>
                  <View style={{zIndex:999,alignItems:'center',justifyContent:'center',height:50,width:width}}>


                      <Text style={{backgroundColor:'transparent',alignSelf:'center',fontFamily:'CircularStd-Black',color:'#fff',fontSize:19}}>Save</Text>

                  </View>
                  <Image style={{width:width,height:50,position:'absolute',bottom:0}} source={{ uri: 'buttonimg' }}/>
              </TouchableOpacity>
          </View>


      </View>
    );
  }
}
