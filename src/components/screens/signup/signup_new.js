import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator, TouchableWithoutFeedback, contentLoader
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { onSignIn,onSignOut, getAllAsyncStroage } from '../../../config/auth';
import { signin, getCourses } from '../../template/api.js'
import axios from 'axios';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from './styles.js';
import { addAsyncStorage } from '../../../config/auth.js'

import Toast, {DURATION} from 'react-native-easy-toast'
import StatusBarBackground from './statusbar.js'
const {height, width} = Dimensions.get('window');

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');


export default class Signup_new extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: null,
      contentLoader: false
    };
  }

  componentWillMount(){


  }



/*authenticating starts*/


checkEmailSignup = async (mail) => {


  if(mail != null) {

    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(mail)) {
      alert("Enter valid email address")
    }
    else {

      this.setState({contentLoader: true})

      axios.post('http://resoltz.azurewebsites.net/api/v1/signup_new', {"email": mail, "type": "S"}).then( async response => {
        if(response.status==200){
          if(response.data.message == 'New Entry')
            {
              alert("SucesFully loggied in")
              var userid = response.data.user
              AsyncStorage.setItem("USER_ID", String(userid));
              console.log(await AsyncStorage.getItem("USER_ID"))

              const { navigate } = this.props.navigation;

              navigate("ProfileFill", {userinfo: 1, profiledetails: 0, pickclass: 0})

            }else{
                alert("Already Registered, go to login page")
                this.setState({contentLoader: false})
            }
          }
        }).catch(err => {
          console.log(err)
        })

    }

  } else {

    alert("Enter valid email address")

  }


}

/*authenticating starts*/

  render(){
    const { navigate } = this.props.navigation;
    const {
      loader,
      username,
      password,
      showErrorUsername,
      showErrorPassword,
      errorUsernameMessage,
      errorPasswordMessage,
      contentLoader
    } = this.state;
    return(
      <View style={ styles.mainBody }>
          <View style={styles.chevron_left_icon}>
            <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('Landing')}}>
              <Icon name="chevron-left" size={25} color="#FF7E00"   />
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <Text style={styles.topSignupTxt}>
              Sign up
            </Text>
          </View>
          {contentLoader?
            <ActivityIndicator
                animating = {this.state.contentLoader}
                color = '#bc2b78'
                size = "large"
                style = {styles.activityIndicator}
            />
          :
          <View style={{marginTop: 20, textAlign: 'center'}}>
            <View style={styles.body1}>
              <View style={{padding: 0}}>
                <Text style={styles.forTxt}>For students,sign up using your school email</Text>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <TextInput
                  placeholder="email@school.com"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  placeholderTextColor='#626264'
                  style={styles.textInput_login}
                  onChangeText={ (text) => this.setState({email:text}) }
                />
                </TouchableWithoutFeedback>
                {showErrorUsername?<Text style={{color:'#fb620c'}}>{errorUsernameMessage}</Text>:null}
              </View>

              <View style={{marginTop: 20, borderTopWidth: 2}}>
                <Text style={styles.nonStudent}>Non students create an account using options below</Text>
                <TouchableOpacity  style={styles.submitFB}>
                  <Icon name="facebook-f" size={30} color="#fff" style={{position:'absolute',left:23,top:17,fontSize:20}} />
                  <View style = {{left:18,width:180,alignSelf:'center'}}>
                  <Text style={styles.buttonText}>
                    Sign up with Facebook
                  </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity  style={styles.submitTW}>
                  <Icon name="twitter" size={30} color="#fff" style={{position:'absolute',left:23,top:17,fontSize:20}} />
                  <View style = {{left:18,width:180,alignSelf:'center'}}>
                  <Text style={styles.buttonText}>
                    Sign up with Twitter
                  </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity  style={styles.submitGM}>
                  <Icon name="google-plus" size={30} color="#fff" style={{position:'absolute',left:23,top:17,fontSize:20}} />
                  <View style = {{left:18,width:180,alignSelf:'center'}}>
                  <Text style={styles.buttonText}>
                    Sign up with Gmail
                  </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        }

        <View style={{bottom:20,position:'absolute',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity onPress={()=>this.checkEmailSignup(this.state.email)}>
                <View style={{zIndex:999,alignItems:'center',justifyContent:'center',height:50,width:width}}>


                    <Text style={{backgroundColor:'transparent',alignSelf:'center',fontFamily:'CircularStd-Black',color:'#fff',fontSize:19}}>Save</Text>

                </View>
                <Image style={{width:width,height:50,position:'absolute',bottom:0}} source={{ uri: 'buttonimg' }}/>
            </TouchableOpacity>
        </View>



      </View>
    );
  }
}
