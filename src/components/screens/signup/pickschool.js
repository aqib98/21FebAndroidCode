import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { onSignIn,onSignOut, getAllAsyncStroage } from '../../../config/auth';
import { signin, getCourses } from '../../template/api.js'
import axios from 'axios';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from './styles.js';

import Toast, {DURATION} from 'react-native-easy-toast'
import StatusBarBackground from './statusbar.js'

const {height, width} = Dimensions.get('window');

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

import SearchInput, { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['school_name'];

export default class PickSchool extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected : null,
      schoolname: null,
      searchTerm: '',
      schools: [],
      contentLoader:true,
    };
  }
  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }
  componentWillMount(){

    console.log(this.props)

    this.getSchool()

  }



  getSchool = () => {

      fetch('http://resoltz.azurewebsites.net/api/v1/fetch_school_details', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
        }).then((response) => response.json())
          .then((responseJson) => {
            this.state.schools = responseJson.arr
            this.setState({contentLoader:false})
          })
          .catch((error) => {
            alert('There is no schools or server problom')
       });

  }

  selectschool = (id, name) => {
    this.setState({selected: id})
    this.setState({schoolname: name})
  }

  nextstep = () => {



    if(this.state.selected  != '') {

      const { navigate } = this.props.navigation

      const { password, confirmpassword } = this.props.navigation.state.params;

      //alert(password)

      navigate("UserInfo2", { password: password, confirmpassword: confirmpassword, schoolid: this.state.selected, schoolname: this.state.schoolname})

    } else {
      alert("Please select your school")
    }

  }

  render(){
    const { navigate } = this.props.navigation;
    const {
    schools, contentLoader } = this.state;

    console.log(schools)

    var emails = schools


  const filteredEmails = emails.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

  let mani = 0


    return(
      <View style={ styles.mainBody }>
          <View style={styles.chevron_left_icon}>
            <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              var { password1, confirmpassword1 } = this.props.navigation.state.params;
              navigate('UserInfo2', { password: password1, confirmpassword: confirmpassword1, schoolid: this.state.selected, schoolname: this.state.schoolname})}}>
              <Icon name="chevron-left" size={25} color="#FF7E00"   />
            </TouchableOpacity>
          </View>
          <View style={styles.header1}>
            <Text style={styles.topSignupTxt}>
              Pick school
            </Text>
          </View>

        {contentLoader?
          <ActivityIndicator
            animating = {this.state.contentLoader}
            color = '#bc2b78'
            size = "large"
            style = {styles.activityIndicator}
          />
          :
          <View>
          <View style={{alignItems: 'center', borderBottomWidth: 1}}>
            <SearchInput
              onChangeText={(term) => { this.searchUpdated(term) }}
              style={styles.textInput_search}
              placeholder="Type a message to search"
              placeholderTextColor='#626264'
              />
           </View>

           <View>
            <ScrollView>
              {filteredEmails.map(email => {
                return (
                  <TouchableOpacity onPress={()=>this.selectschool(email.id, email.school_name)} key={email.id} style={styles.emailItem}>
                    <View style={styles.searchresults}>
                      <Text style={(email.id == this.state.selected) ? styles.selected : styles.notselected}>{email.school_name}</Text>
                      {(email.id == this.state.selected)
                      ?
                      <Text style={{color: "#626264", fontSize: 14, fontWeight: 'bold', marginTop: 5}}>
                        <Icon name="circle" size={14} color="#FF7E00"   />
                      </Text>
                      :
                      <Text></Text>
                      }
                    </View>
                  </TouchableOpacity>
                )
              })}
            </ScrollView>

           </View>

           </View>

         }

          <View style={{bottom:20,position:'absolute',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity onPress={()=>this.nextstep()}>
                  <View style={{zIndex:999,alignItems:'center',justifyContent:'center',height:50,width:width}}>


                      <Text style={{backgroundColor:'transparent',alignSelf:'center',fontFamily:'CircularStd-Black',color:'#fff',fontSize:19}}>Save and continue</Text>

                  </View>
                  <Image style={{width:width,height:50,position:'absolute',bottom:0}} source={{ uri: 'buttonimg' }}/>
              </TouchableOpacity>
          </View>
      </View>
    );
  }
}
