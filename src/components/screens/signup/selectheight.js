import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator, Picker, DatePickerIOS
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { onSignIn,onSignOut, getAllAsyncStroage } from '../../../config/auth';
import { signin, getCourses } from '../../template/api.js'
import axios from 'axios';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from './styles.js';
import moment from "moment";

import Toast, {DURATION} from 'react-native-easy-toast'
import StatusBarBackground from './statusbar.js'

const {height, width} = Dimensions.get('window');

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');


export default class SelectHeight extends Component {

    constructor(props) {
      super(props);
      this.state = {
        feet: null,
        inches: null,
        system: null,
        height_in_metric: null
      };
    }

    setHeight = (feet, inches, system, height_in_metric) => {

      if((height_in_metric > 0) || (feet != null && inches != null)) {
        const { navigate } = this.props.navigation;

        console.log(height_in_metric)

        var { weight, system, weight_in_kgs  } = this.props.navigation.state.params;

        if(system == 1){
          navigate("ProfileDetails2", {feet: null, inches: null, weight: weight, height_in_metric: height_in_metric, system: system, weight_in_kgs: weight_in_kgs})
        } else {
          navigate("ProfileDetails2", {feet: feet, inches: inches, weight: weight, height_in_metric: null, system: system, weight_in_kgs: weight_in_kgs})
        }
      } else {
        alert('Please select valid height')
      }



    }

    backStep = () => {

      const { navigate } = this.props.navigation;

      var { weight, feet, inches, system , height_in_metric, weight_in_kgs } = this.props.navigation.state.params;

      navigate("ProfileDetails2", {feet: feet, inches: inches, weight: weight, system: system, height_in_metric: height_in_metric, weight_in_kgs: weight_in_kgs})

    }


    componentWillMount(){

      var { feet, inches, weight, system, height_in_metric  } = this.props.navigation.state.params;

      if(feet != null){
        this.setState({feet: feet})
      }

      if(inches != null){
        this.setState({inches: inches})
      }

      if(weight != null){
        this.setState({weight: weight})
      }

      if(system != null){
        this.setState({system: system})
      } else {
        this.setState({system: 0})
      }

      if(height_in_metric != null){
        this.setState({height_in_metric: height_in_metric})
      }

    }


  render(){
    const { navigate } = this.props.navigation;
    console.log(this.state.height_in_metric)
    var payments = [];

    for(let i = 0; i < 300; i++){

      payments.push(
          <Picker.Item label={i.toString()+" cm"} value={i.toString()}  />
      )
    }
    return(
      <View style={ styles.mainBody }>
        <StatusBarBackground style={{backgroundColor:'#ff7200'}}/>
          <View style={styles.chevron_left_icon}>
            <TouchableOpacity onPress={()=>this.backStep()}>
              <Icon name="chevron-left" size={25} color="#FF7E00"   />
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <Text style={styles.topSignupTxt}>
              Select height
            </Text>
          </View>

          {this.state.system == 0 ?
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
              <View style={{width: width/3 }}>
                <Picker
                  selectedValue={this.state.feet}
                  onValueChange={(itemValue, itemIndex) => this.setState({feet: itemValue})}>
                  <Picker.Item label="1 feet" value="1"  />
                  <Picker.Item label="2 feet" value="2"  />
                  <Picker.Item label="3 feet" value="3"  />
                  <Picker.Item label="4 feet" value="4"  />
                  <Picker.Item label="5 feet" value="5"  />
                  <Picker.Item label="6 feet" value="6"  />
                  <Picker.Item label="7 feet" value="7"  />
                  <Picker.Item label="8 feet" value="8"  />
                  <Picker.Item label="9 feet" value="9"  />
                  <Picker.Item label="10 feet" value="10"  />
                  <Picker.Item label="11 feet" value="11"  />
                  <Picker.Item label="12 feet" value="12"  />
                </Picker>
              </View>
              <View style={{width: width/3 }}>
                <Picker
                  selectedValue={this.state.inches}
                  onValueChange={(itemValue, itemIndex) => this.setState({inches: itemValue})}>
                  <Picker.Item label="1 inches" value="1"   />
                  <Picker.Item label="2 inches" value="2"  />
                  <Picker.Item label="3 inches" value="3"  />
                  <Picker.Item label="4 inches" value="4"  />
                  <Picker.Item label="5 inches" value="5"  />
                  <Picker.Item label="6 inches" value="6"  />
                  <Picker.Item label="7 inches" value="7"  />
                  <Picker.Item label="8 inches" value="8"  />
                  <Picker.Item label="9 inches" value="9"  />
                  <Picker.Item label="10 inches" value="10"  />
                  <Picker.Item label="11 inches" value="11"  />
                  <Picker.Item label="12 inches" value="12"  />
                </Picker>
              </View>
              <View style={{width: width/3 }}>
                <Picker
                  selectedValue={this.state.system}
                  onValueChange={(itemValue, itemIndex) => this.setState({system: itemValue})}>
                  <Picker.Item label="Imperial" value="1"  />
                </Picker>
              </View>
            </View>
            :
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{width: width/2 }}>
              <Picker
                selectedValue={this.state.height_in_metric}
                onValueChange={(itemValue, itemIndex) => this.setState({height_in_metric: itemValue})}>
                {payments}
              </Picker>
            </View>
            <View style={{width: width/2 }}>
              <Picker
                selectedValue={this.state.system}
                onValueChange={(itemValue, itemIndex) => this.setState({system: itemValue})}>
                <Picker.Item label="Metric" value="1"  />
              </Picker>
            </View>
            </View>
            }


          <View style={{bottom:20,position:'absolute',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity onPress={()=>this.setHeight(this.state.feet, this.state.inches, this.state.system, this.state.height_in_metric)}>
                  <View style={{zIndex:999,alignItems:'center',justifyContent:'center',height:50,width:width}}>


                      <Text style={{backgroundColor:'transparent',alignSelf:'center',fontFamily:'CircularStd-Black',color:'#fff',fontSize:19}}>Save</Text>

                  </View>
                  <Image style={{width:width,height:50,position:'absolute',bottom:0}} source={{ uri: 'buttonimg' }}/>
              </TouchableOpacity>
          </View>


      </View>
    );
  }
}
