import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { onSignIn, getAllAsyncStroage } from '../../../config/auth';
import { getProfileDetails } from '../../template/SQLiteOperationsOffline.js';
import { updateProfile } from '../../template/api.js';
import API from '../../template/constants.js';
import axios from 'axios';
import { Slider } from 'react-native-elements'
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from './styles.js';
import moment from 'moment'

import Toast, {DURATION} from 'react-native-easy-toast'
import StatusBarBackground from './statusbar.js'

const {height, width} = Dimensions.get('window');

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

import DatePicker from 'react-native-datepicker'


export default class ProfileDetails1 extends Component {


    constructor(props) {
      super(props);
      this.state = {
        date: null,
        gender: 0,
        genderatbirth: 0,
        userid: null,
        contentLoader:true
      };



    }

    componentWillMount(){


      getAllAsyncStroage()
      .then(res =>
        console.log(res)
      )
      .catch(err =>
        console.log(err)
      );
      this.getStudentID();


      var { date  } = this.props.navigation.state.params;

      console.log(this.state)

      if(date != null){
        this.state.date = date
      }
    }

    getStudentID =async()=> {

      let USER_ID = await AsyncStorage.getItem("USER_ID");
      this.setState({userid:USER_ID,enable:false, Loader:true},()=>{
        this._getStudentDetails(this.state.userid);
      });
    }
    _getStudentDetails=(id)=>{
      getProfileDetails(id).then(response=>{
        if(response.status){
          this.setState({profile:response.data},()=>{
            var gender = this.state.profile.gender
            this.setState({date: this.state.profile.dob})

            if(this.state.profile.gender == 'Male') {
              this.setState({gender: 0, genderatbirth: 0})
            } else {
              this.setState({gender: 1, genderatbirth: 1})
            }

          });
          console.log(this.state)
          this.setState({Loader:false, contentLoader:false})
        }else{
          this.setState({Loader:false, contentLoader:false})
          this.refs.toast.show(response.show, DURATION.LENGTH_LONG);
        }
      },err=>{
        console.log(err)
      })
    }

  /*

    nextStep(date, gender, genderatbirth) {

      if(date!=null&&gender!=null&&genderatbirth!=null){
      const { navigate } = this.props.navigation;
      navigate('ProfileDetails2', {})
      } else {
        alert('Please Fill All Fields')
      }

    }

  */


    nextStep = async() => {
      //const { navigate } = this.props.navigation;

      //navigate("ProfileDetails2", {feet: null, inches: null, weight: null, system: 0, height_in_metric:null })

      const { date, gender, genderatbirth, userid } = this.state

      let now  = new Date();
      let then = new Date(date)

      let day = then.toLocaleDateString()

      let month = then.getMonth()

      let year = then.getYear()

      let selecteddate = month +'-'+day+'-'+year

      let diff = moment(now,"DD/MM/YYYY").diff(moment(then,"DD/MM/YYYY"));

      var diffMs = (now-then); // milliseconds between now & Christmas
      var diffDays = (Math.floor(diffMs / 86400000) / 365); // days

      if(diffDays < 15 )
      {
          alert("You are not eligible to use this app")
      }
      else {

        this.setState({contentLoader: true})

        if(date != null && gender != null && genderatbirth != null) {

          if(gender == 0){
            var gender_text = 'Male'
          } else {
            var gender_text = "Female"
          }

               var datenow = new Date();
               var presentyear = datenow.getFullYear();
               var dateofbirth = date.split("/");
               var dateofbirthyear = dateofbirth[2]

               let USER_ID = await AsyncStorage.getItem("USER_ID");

               var age = presentyear - dateofbirthyear

                data = {
                          "dob": date,
                          "gender": gender_text,
                          "id": USER_ID,
                          "age": age
                       }

                console.log(data)

                //this.setState({contentLoader: false})

              axios.put(API.UpdateProfile, data).then( async response => {
                if(response.status==200){
                    console.log(response)
                    //this.setState({contentLoader: false})
                    const { navigate } = this.props.navigation;
                    navigate("ProfileDetails2", {feet: null, inches: null, weight: null, system: 0, height_in_metric:null })

                  }
                }).catch(err => {
                  console.log(err)
                })

              } else {
                alert('Please Fill the fields')
                this.setState({contentLoader: false})
              }

      }
    }




  render(){
    const { navigate } = this.props.navigation;
    const {
      loader,
      username,
      password,
      showErrorUsername,
      showErrorPassword,
      errorUsernameMessage,
      errorPasswordMessage,
      contentLoader
    } = this.state;
    console.log('silder value',this.state.gender)
    var { date  } = this.props.navigation.state.params;
    if(date != null){
      this.state.date = date
    }

    return(
      <View style={ styles.mainBody }>
        <StatusBarBackground style={{backgroundColor:'#ff7200'}}/>
          <View style={styles.chevron_left_icon}>
            <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('ProfileFill', { userinfo: 0, profiledetails: 1, pickclass: 0 })}}>
              <Icon name="chevron-left" size={25} color="#FF7E00"   />
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <Text style={styles.topSignupTxt}>
              Profile details (1/2)
            </Text>
          </View>

          {contentLoader?
            <ActivityIndicator
                animating = {this.state.contentLoader}
                color = '#bc2b78'
                size = "large"
                style = {styles.activityIndicator}
            />
          :

          <View style={{flex: 1, flexDirection: 'column'}}>
              <View style={{height: (height-140)/3, borderBottomWidth: 1}}>
                <TouchableOpacity onPress={()=>{}}>
                <View style={{width: width, height: 150}}>
                  <View style={{flex:1, flexDirection: 'row', justifyContent: 'center',alignItems:'center'}}>
                    <View style={{flex:1,justifyContent:'center',alignSelf:'center', marginTop: 20, marginLeft: 20 }}>
                      <DatePicker
                        style={styles.textInput_signup}
                        date={this.state.date}
                        mode="date"
                        placeholder="select date"
                        minDate="1900-01-01"
                        maxDate="2020-01-01"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                          dateInput:{
                            borderWidth: 0,
                          },
                          dateText:{
                            color: '#c7c8ca',
                            justifyContent: 'flex-start'
                          }
                        }}
                        onDateChange={(date) => {this.setState({date: date})}}
                      />
                    </View>
                    <View style = {{flex:1,justifyContent:'center',alignItems:'center',marginTop:53}}>

                    </View>
                  </View>
                </View>
                </TouchableOpacity>
              </View>
             <View style={{height: (height-140)/3, borderBottomWidth: 1}}>
             <View style={{flex: 1, alignItems: 'stretch', justifyContent: 'center', width: 200, alignSelf: 'center', marginTop: 40}}>
             <Text style={{textAlign: "center", paddingBottom: 10, fontSize: 16, width: '100%', marginLeft: 6, color: "#fff"}}> Gender Identity</Text>
               <View style={{flex: 1, flexDirection: 'row', alignItems: 'stretch', justifyContent: 'center', width: 200, alignSelf: 'center'}}>
                 <View style={{width: width/4}}>
                   <Text style={{textAlign: "center", marginTop: 10, fontSize: 15, marginLeft: 0, color: "#fff"}}>Male</Text>
                 </View>
                 <View style={{width: width/2}}>

                   <Slider
                     value={this.state.gender}
                     step={1}
                     minimumValue={0}
                     maximumValue={2}
                     minimumTrackTintColor = '#3f3f3f'
                     maximumTrackTintColor = '#3f3f3f'
                     thumbStyle = {{height:40,width:10,backgroundColor:'#ff6600'}}
                     onValueChange={(value) => this.setState({gender: value})} />

                 </View>
                 <View style={{width: width/4}}>
                   <Text style={{textAlign: "center", marginTop: 10, fontSize: 15,  marginLeft: 0, color: "#fff"}}>Female</Text>
                 </View>

               </View>
             </View>
              </View>
              <View style={{height: (height-140)/3}}>
              <View style={{flex: 1, alignItems: 'stretch', justifyContent: 'center', width: 200, alignSelf: 'center', marginTop: 40}}>
              <Text style={{textAlign: "center", paddingBottom: 10, fontSize: 16, width: '100%', marginLeft: 6, color: "#fff"}}> Gender at birth</Text>
                <View style={{flex: 1, flexDirection: 'row', alignItems: 'stretch', justifyContent: 'center', width: 200, alignSelf: 'center'}}>
                  <View style={{width: width/4}}>
                    <Text style={{textAlign: "center", marginTop: 10, fontSize: 15, marginLeft: 0, color: "#fff"}}>Male</Text>
                  </View>
                  <View style={{width: width/2}}>

                    <Slider
                      value={this.state.genderatbirth}
                      step={1}
                      minimumValue={0}
                      maximumValue={2}
                      minimumTrackTintColor = '#3f3f3f'
                      maximumTrackTintColor = '#3f3f3f'
                      thumbStyle = {{height:40,width:10,backgroundColor:'#ff6600'}}
                      onValueChange={(value) => this.setState({genderatbirth: value})} />

                  </View>
                  <View style={{width: width/4}}>
                    <Text style={{textAlign: "center", marginTop: 10, fontSize: 15,  marginLeft: 0, color: "#fff"}}>Female</Text>
                  </View>

                </View>
              </View>
               </View>
            </View>

          }

          <View style={{bottom:20,position:'absolute',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity onPress={()=>this.nextStep()}>
                  <View style={{zIndex:999,alignItems:'center',justifyContent:'center',height:50,width:width}}>


                      <Text style={{backgroundColor:'transparent',alignSelf:'center',fontFamily:'CircularStd-Black',color:'#fff',fontSize:19}}>Save and continue</Text>

                  </View>
                  <Image style={{width:width,height:50,position:'absolute',bottom:0}} source={{ uri: 'buttonimg' }}/>
              </TouchableOpacity>
          </View>


      </View>
    );
  }
}
