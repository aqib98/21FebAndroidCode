import React, { Component } from 'react';
import { getUpcomingWorkouts } from '../../template/Workout/workout.js'
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  ToastAndroid,
  Alert
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import Ionicons from 'react-native-vector-icons/Ionicons'

import { TabNavigator } from 'react-navigation';


import styles from './styles.js'



export default class UpcomingWorkouts extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      workouts:[],
      loader:null
    };
  }
  async componentWillMount () {
    let USER_ID = await AsyncStorage.getItem("USER_ID");
    this._getAllWorkouts(USER_ID)
  }
  _getAllWorkouts=(user_id)=>{
     this.setState({loader:true})
    getUpcomingWorkouts(user_id).then(response=>{
      console.log(response);
      if(response.status){
        this.setState({workouts:response.data.data, loader:false},()=>console.log(this.state.workouts));
      }else{
        this.setState({workouts:response.data, loader:false});
      }
    },err=>{
      console.log(err)
    })
  }

  logactivity() {
      const { navigate  } = this.props.navigation;
    navigate("Logactivity", { calories:'', time: '00:00', date: '', heart_rate: 0});

  }

  async trackactivity(routeData) {
    console.log("Manikant")
     if(await AsyncStorage.getItem("DEVICE_NAME")&& await AsyncStorage.getItem("DEVICE_ID")){
    const { navigate  } = this.props.navigation;
      navigate("Trackactivity");
     }else{
      this.addBLERoute(routeData);
     }
  }

  addBLERoute=async (routeData)=>{
    const { navigate } = this.props.navigation;
    Alert.alert('Add Bluetooth device','To continue the workout',
        [
          {text: 'Add later', onPress: () => console.log('cancelled the workout process')},
          {text: 'Add', onPress: () => navigate('SearchDevices', {fromRoute:routeData.routeFrom, toRoute:routeData.routeTo, enableBack:true, data:routeData.workout})},
        ],
        { cancelable: false }
      )
  }

  routing=async(routeData)=>{
    console.log(routeData)
    const { workouts, loader } = this.state;
    const { navigate  } = this.props.navigation;
    if(await AsyncStorage.getItem("DEVICE_NAME")&& await AsyncStorage.getItem("DEVICE_ID")){
      routeData.key?ToastAndroid.show('Start first workout', ToastAndroid.SHORT):workouts.length?navigate(routeData.routeTo, {params:routeData.workout}):navigate('Courses')
    }else{
      this.addBLERoute(routeData);
    }
  }
  render() {
    const {navigation} = this.props;
    const { workouts, loader } = this.state;
    return (
      <View style={styles.upcomingWorkouts_main_body}>
        <View style={styles.upcomingWorkouts_bg_img_view}>
          <Image style={styles.upcomingWorkouts_bg_img} source={{ uri: 'gradientbg' }}/>
        </View>
        <View style={styles.upcomingWorkouts_header}>
          <Text style={styles.upcomingWorkouts_heading}>
            UPCOMING WORKOUTS
          </Text>
        </View>
        <ScrollView style={styles.upcomingWorkouts_scrollView}>
        {loader?
          <ActivityIndicator
            animating = {this.state.contentLoader}
            color = '#bc2b78'
            size = "large"
            style = {styles.activityIndicator}
          />
          :
          <View style={styles.upcomingWorkouts_content_view}>
            {workouts.length?
              workouts.map((workout, i)=>{
                return(
                  <View style={styles.upcomingWorkouts_content} key={i}>
                    <Text  style={styles.tuesday_heading}>
                      TUESSDAY,DECEMBER 13TH 1
                    </Text>
                    <TouchableOpacity onPress = {()=>this.routing({routeTo:'WorkoutDetails','routeFrom':'UpcomingWorkouts',workout:workouts[0],key:i})}>
                      <Image
                        style={{height:185,
                        width:'100%'}}
                        source={{ uri:workout.thumbnail_url }}
                      />
                    </TouchableOpacity>
                    <Text  style={styles.tuesday_heading}>
                      {workout.title}
                    </Text>
                    <View style={styles.whiteline}></View>
                  </View>
                  );
              })
              :
              <Text style={styles.upcomingWorkouts_heading}>
                ITS EMPTY IN HERE.
                SIGNUP FOR A WORKOUT
                AND TO GET STARTED!
              </Text>
            }
          </View>
        }
        </ScrollView>
        <View style={styles.wat_log_track_icons}>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity
            activeOpacity={1} onPress={() => {
              const { navigate  } = this.props.navigation;
              if(workouts.length)
              this.routing({routeTo:'WorkoutDetails',routeFrom:'UpcomingWorkouts',workout:workouts[0],key:0})
              else
              navigate('Courses')
            }}>
              <Image style={{ width: 50, height: 50 }} source={{uri:'rgtarrow'}} />
              <Text style={{fontSize:10,color:'#fff'}}>
                Watch Video
              </Text>
            </TouchableOpacity>
          </View>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity
            activeOpacity={1} onPress = {()=>{var a=[]; this.trackactivity({routeTo:'Trackactivity','routeFrom':'UpcomingWorkouts',workout:[],key:0})}}>
              <Image style={{ width: 50, height: 50 }} source={{uri:'timer'}} />
              <Text style={{fontSize:10,color:'#fff'}}>
                Track activity
              </Text>
            </TouchableOpacity>
          </View>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity
            activeOpacity={1} onPress = {()=>this.logactivity()}>
              <Image style={{ width: 50, height: 50 }} source={{uri:'fingerup'}} />
              <Text style={{fontSize:10,color:'#fff'}}>
                Log activity
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.cross_icon}>
          <TouchableOpacity
            activeOpacity={1} onPress={() => {
              const { navigate  } = this.props.navigation;
              navigate('Tabs')
            }}>
            <Image style={{  width: 45, height: 45 }} source={{uri:'cross'}} />
          </TouchableOpacity>
        </View>
        <View style={styles.trans_bg}>
          <Image style={{ width:'100%', height: 100 }} source={{uri:'trans_bg'}} />
        </View>
        <View style={styles.btm_tabNavigation}>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <Image style={{ width: 32, height: 28, top:3, position:'absolute' }} source={{uri:'feedicon'}} />
            <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
              FEED
            </Text>
          </View>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'profileicon'}} />
            <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
              PROFILE
            </Text>
          </View>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <Image style={{ width: 45, height: 45, marginTop:3 }} source={{uri:'bottom_tab_middlelogo'}} />
            <Text style={{fontSize:10,color:'#fff', marginTop:12}}> </Text>
          </View>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <Image style={{ width: 28, height: 31, top:3, position:'absolute'  }} source={{uri:'notificationicon'}} />
            <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
              NOTIFICATION
            </Text>
          </View>
          <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
            <Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'btnmenuicon'}} />
            <Text style={{fontSize:10,color:'#fff', marginTop:12}}>
              MORE
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
