
import {

  StyleSheet,


} from 'react-native';
const Dimensions = require('Dimensions');
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;

const styles = StyleSheet.create({
  mainBody:{
    backgroundColor: '#2d2e37',
    marginTop : 0,
    padding:0,
    height:Dimensions.get('window').height,
    width:Dimensions.get('window').width,
    fontSize:20
  },
  header : {
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row'
  },
  body1 : {
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'column',
    position:'relative'
  } ,
  body1a : {
    alignItems:'center',
     paddingBottom:120

  } ,
  footer : {
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    position:'relative',
    bottom:0
  },
  signup_temp_form:{
   alignItems:'center'
  },
  textStyle : {
    color:'#fff',
    alignItems:'center',
    justifyContent:'center',
    fontFamily :'arial',
    fontSize:16,
    fontWeight:'bold',
      textAlign:'center', width:280,
      marginBottom:40


  },


  submitFB:{
   marginRight:40,
   marginLeft:40,
   marginTop:0,
   marginBottom:20,
   paddingTop:16,
   paddingBottom:16,
   backgroundColor:'#465897',
   borderRadius:30,
   position:'relative',
   borderColor: '#fff',
   width:242,

 },
 submitTW:{
  marginRight:40,
  marginLeft:40,
  marginTop:0,
  marginBottom:20,
  paddingTop:16,
  paddingBottom:16,
  backgroundColor:'#599ef3',
  borderRadius:30,
  position:'relative',
  borderColor: '#fff',
  width:242,

},
submitGM:{
 marginRight:40,
 marginLeft:40,
 marginTop:0,
 marginBottom:20,
 paddingTop:16,
 paddingBottom:16,
 backgroundColor:'#e15350',
 borderRadius:30,
 position:'relative',
 borderColor: '#fff',
 width:242,

},
submit:{
  height:60,
  marginBottom:20,
  width:Dimensions.get('window').width

},

 submitText:{
     color:'#fff',
     textAlign:'center',
 },
 buttonText:{
   color:'#fff',
   textAlign:'center',
   fontSize : 15,


 },
 footerText:{
   fontSize:20,
   color:'#fff',
   fontWeight:'bold',
   textAlign:'center',
   paddingTop:15
 },
 textInput_login:{
   marginRight:40,
   marginLeft:40,
   marginTop:0,
   marginBottom:20,
   paddingTop:11,
   paddingBottom:12,
   paddingLeft:25,
   backgroundColor:'#202124',
   borderRadius:30,
   position:'relative',
   color: '#fff',
   width:242,

 },
 textInput_signup:{
   marginRight:40,
   marginLeft:40,
   marginTop:0,
   marginBottom:20,
   paddingTop:11,
   paddingBottom:12,
   paddingLeft:25,
   backgroundColor:'#202124',
   borderRadius:30,
   position:'relative',
   color: '#fff',
   width:242,

 },
   forTxt:{paddingBottom:25,color:'#fff', paddingTop:25},

   nonStudent:{paddingBottom:25,color:'#fff', paddingTop:45},

   topSignupTxt:{color:'#F5F5FF',
   textAlign:'center',
   fontSize : 20,
   opacity:0.5,
   fontWeight:'bold',paddingTop:15},

   save_btnTxt : {
     fontSize:20,
     paddingTop:35,
     fontWeight:'bold',
     textAlign:'center',
     color:'#fff',
     justifyContent: 'center',
     alignItems: 'center'},

     save_btnImg:{
         resizeMode: 'contain',

         height:100,
          width:Dimensions.get('window').width,
          zIndex:0
     },

     save_view : {

       position: 'absolute',
       zIndex:999,
       width:Dimensions.get('window').width,
       justifyContent: 'center',
       backgroundColor: 'transparent'
     },

     signup_btnTxt : {
       fontSize:20,
       paddingTop:35,
       fontWeight:'bold',
       textAlign:'center',
       color:'#fff',
       justifyContent: 'center',
       alignItems: 'center'},

       signup_btnImg:{

           width:220,
           height:100,

            zIndex:999
       },

       signup_view : {

         position: 'absolute',
         zIndex:999,
         width:Dimensions.get('window').width,
         justifyContent: 'center',
         backgroundColor: 'transparent'
       },
       chevron_left_icon : {position:'absolute',paddingLeft:16,paddingTop:16,fontSize:20},
      linearGradient: {
         flex: 1,
         paddingLeft: 15,
         paddingRight: 15,
         borderRadius: 5
       },
       //workout.js
       text_workout_heading : {fontWeight:'bold',color:'#fff',fontSize:22,paddingLeft:10,paddingTop:13,fontFamily:'arial'},
       text_workout_sub_heading : {fontSize:10,paddingLeft:10,paddingBottom:10,color:'#FF7E00'},
       footer_main_view : {flex:1,flexDirection:'row',alignItems:'center',justifyContent:'center',backgroundColor:'#fff',height:70,width:Dimensions.get('window').width,bottom:0},
       icon_style : {flex:0.2,alignItems:'center',justifyContent:'center',paddingBottom:12},
       carousel_style_workout : {width: 270,height: slideHeight,paddingHorizontal: itemHorizontalMargin,paddingBottom: 18},
       carousel_style_trainers : {width: 170,height: slideHeight,paddingHorizontal: itemHorizontalMargin,paddingBottom: 18},
       carousel_style_plans : {width: 270,height: slideHeight,paddingHorizontal: itemHorizontalMargin,paddingBottom: 18},
       carousel_image_view : {flex: 1,backgroundColor: '#2d2e37'},
       carousel_image : {flex: 1,backgroundColor: '#2d2e37',resizeMode:'stretch'},
       carousel_text_view : {justifyContent: 'center',paddingTop: 20 - 8,paddingBottom: 20,paddingHorizontal: 16,backgroundColor: '#2d2e37'},
       carousel_text : {marginTop: 6,color: '#fff',fontSize: 12,fontStyle: 'italic'},

       //upcomingWorkouts.js
       upcomingWorkouts_main_body : {
                                        backgroundColor: '#2d2e37',
                                        marginTop : 0,
                                        padding:0,
                                        height: Dimensions.get('window').height,
                                        width: Dimensions.get('window').width
                                    },

        upcomingWorkouts_bg_img_view : {
                                            position: 'absolute',
                                            top: 0,
                                            left: 0,
                                            width: undefined,
                                            height: undefined,
                                        },

        upcomingWorkouts_bg_img : {
                                      resizeMode: 'stretch',
                                      height: Dimensions.get('window').height,
                                      width: Dimensions.get('window').width,
                                  },



        upcomingWorkouts_heading :{color:'#ffffff',
        textAlign:'left', marginTop:5, marginLeft:'13%',
        fontSize : 20,

        fontWeight:'bold',paddingTop:15},
        finished_heading:{
          color:'#ffffff',
          textAlign:'center', marginTop:'25%',
          fontSize : 20,
          fontWeight:'bold',paddingTop:15
        },
        wellness:{
          color:'#ffffff',
          textAlign:'center', marginTop:30,
          fontSize : 16,
        },

        upcomingWorkouts_scrollView : {
          marginTop : 5,
          marginBottom:170

        },
        allMiddleIcon:{
          position:'absolute',
          top:10,zIndex:999
        },

        upcomingWorkouts_content_view : {
            alignItems:'center'
        },
btm_tabNavigation:{
  position:'absolute',
  bottom:0,
  backgroundColor:'#414249',flex:1,flexDirection:'row',paddingTop:12,paddingBottom:2
},
trans_bg:{


  flex:1,
  flexDirection:'column',
  alignItems:'center',
  justifyContent:'center',

  position:'absolute',
  bottom:0,
  resizeMode: 'stretch',
  height:90,zIndex:2,
  width:Dimensions.get('window').width
},
cross_icon:{
  position:'absolute',
  bottom:28,
  zIndex:3,
  flex:1,
  flexDirection:'column',
  alignItems:'center',
  justifyContent:'center',
  width:Dimensions.get('window').width


},
        upcomingWorkouts_content : {
          width:'75%'
        },
         whiteline:{
           borderBottomWidth:3,borderBottomColor : '#FFF',marginBottom:7, marginTop:7
         },

        tuesday_heading:{
          fontSize:12,
          color:'#ffffff',marginTop:0
        },
        wat_log_track_icons:{
       flex:1,flexDirection:'row',paddingLeft:20,paddingRight:20,fontSize:15,paddingTop:20,
       position:'absolute', bottom:100
     },

        upcomingWorkouts_workout_img : {height:220,
        width:300},

        upcomingWorkouts_footer : {
          alignItems:'center',
          justifyContent:'center',
          flexDirection:'row',
          position:'relative',
          bottom:0
        },
        modal:{
          flex: 1,
      alignItems: 'center',
      backgroundColor: '#f7021a',
      padding: 100
    },
    textdd:{
      position:'absolute',
      top:100
    },

        btm_cross:{
          position:'absolute',
          top:450, zIndex:99,
          height:Dimensions.get('window').height,
          elevation:999995, color:'#fff'
        },

        upcomingWorkouts_footer_view : {

          position: 'absolute',
          zIndex:999,
          width:Dimensions.get('window').width,
          justifyContent: 'center',
          backgroundColor: 'transparent'
        },

        upcomingWorkouts_footer_content: {
          fontSize:20,
          paddingTop:35,
          fontWeight:'bold',
          textAlign:'center',
          color:'#fff',
          justifyContent: 'center',
          alignItems: 'center'},







});


export default styles;
