import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  ListView,
  ScrollView,
  AppState,
  TouchableOpacity, Image, ToastAndroid
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { AsyncStorage } from "react-native";
import Dimensions from 'Dimensions';
const {height,width} = Dimensions.get('window')
import BleManager from 'react-native-ble-manager';
import TimerMixin from 'react-timer-mixin';
import reactMixin from 'react-mixin';
import moment from "moment";
import { getProfileDetails } from '../../template/SQLiteOperationsOffline.js';
import { addToTable, addSensorData } from '../../template/api.js';
const window = Dimensions.get('window');
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import Video from 'react-native-video';
import { getExercises, getExerciseDetails, getSensorData, sendStatus } from '../../template/api.js';
import { addToSQLiteTable } from '../../template/SQLiteOperationsOffline.js';


const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

import KeepAwake from 'react-native-keep-awake';

export default class VideoExcierce extends Component {

  constructor(){
    super()

    this.state = {
      scanning:false,
      peripherals: new Map(),
      appState: '',
      heartrate: 0,
      screenchange: '0',
      time: moment().format("LTS"),
      date: moment().format("LL"),
      totsec: 2400,
      item: 0,
      array: [],
      sum: 0,
      average: 0,
      calories: 0,
      user_id:null,
      profile:[],
      height:'',
      weight:'',
      timepassed: false,
      rate: 1,
      volume: 1,
      duration: 0.0,
      currentTime: 0.0,
      paused: true,
      loader:false,
      times:0,timevalue:'0:0',
      started:false,seconds1: 0,
      /**/
      bleDeviceConnected:false,
      exerciseStarted:false
      /**/
    }

    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);
    this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(this);
    this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(this);
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
  }

  video: Video;

  _playpause=()=>{
    this.setState({ paused: !this.state.paused })
    if(this.state.bleDeviceConnected){
      this.setState({exerciseStarted:!this.state.exerciseStarted})
    }
    console.log('paused')
  }

  onLoad = (data) => {
    this.setState({ duration: data.duration });
    ToastAndroid.show('Loading Video Please Wait...' , ToastAndroid.SHORT);
  };

  _video(){
    this.state.min = Math.floor(data.duration / 60)+':'+Math.floor(data.duration % 60);
  }

  onProgress = (data) => {

    this.setState({ currentTime: data.currentTime });

    this.state.seconds = this.state.duration - this.state.currentTime

  //  console.log(this.state.seconds)

    var minutes = Math.floor(this.state.seconds / 60);

    var second = this.state.seconds - minutes * 60;

    this.setState({ timevalue : minutes +':' + Math.floor(second)})


  };

  onEnd = () => {
    this.setState({ paused: true })
    this.video.seek(0)
  };

  onAudioBecomingNoisy = () => {
    this.setState({ paused: true })
  };


  /*** Timer Functionality ***/

  startTimer() {
    console.log('Starting')
    if (this.timer == 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  }

  countDown() {
    // Remove one second, set state so a re-render happens.
  //  console.log(this.state.seconds1)
    let seconds = this.state.seconds1 + 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds1: seconds,
    });

    // Check if we're at zero.
    if (seconds == 0) {
      //clearInterval(this.timer);
    }
  }

  secondsToTime(secs){

    let hours = Math.floor(secs / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);
    if(minutes < 10)
    {
      minutes = '0'+minutes
    }

    if(hours < 10)
    {
      hours = '0'+hours
    }

    if(seconds < 10)
    {
      seconds = '0'+seconds
    }
    let obj = {
      "h": hours,
      "m": minutes,
      "s": seconds
    };
    return obj;
  }


  onAudioFocusChanged = (event: { hasAudioFocus: boolean }) => {
    this.setState({ paused: !event.hasAudioFocus })
  };

  getCurrentTimePercentage() {
    if (this.state.currentTime > 0) {
      return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
    }
    return 0;
  };

  renderRateControl(rate) {
    const isSelected = (this.state.rate === rate);

    return (
      <TouchableOpacity onPress={() => { this.setState({ rate }) }}>
        <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
          {rate}x
        </Text>
      </TouchableOpacity>
    );
  }

  renderResizeModeControl(resizeMode) {
    const isSelected = (this.state.resizeMode === resizeMode);

    return (
      <TouchableOpacity onPress={() => { this.setState({ resizeMode }) }}>
        <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
          {resizeMode}
        </Text>
      </TouchableOpacity>
    )
  }

  renderVolumeControl(volume) {
    const isSelected = (this.state.volume === volume);

    return (
      <TouchableOpacity onPress={() => { this.setState({ volume }) }}>
        <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
          {volume * 100}%
        </Text>
      </TouchableOpacity>
    )
  }



/**** Bluetooth Connectivity **/

 async componentWillMount() {
  var { exerciseID, user_id, workoutID, workout_day, courseID } = this.props.navigation.state.params.params;

  let device_id = await AsyncStorage.getItem("DEVICE_ID");
  let device_name = await AsyncStorage.getItem("DEVICE_NAME");
  const { navigate  } = this.props.navigation;
  console.log(device_id, device_name)
    if(!device_id||!device_name){
      navigate('SearchDevices')
    }

    this._getStudentDetails(user_id);

    KeepAwake.activate();

  }

  async componentDidMount() {
    this.setState({device_id: await AsyncStorage.getItem("DEVICE_ID")},()=>{

    })

  }
  bleConnect=()=>{
    ToastAndroid.show('Connecting to saved device....' , ToastAndroid.SHORT);
    AppState.addEventListener('change', this.handleAppStateChange);

    BleManager.start({showAlert: false});

    this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );
    this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan );
    this.handlerDisconnect = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDisconnectedPeripheral );
    this.handlerUpdate = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic );


    if (Platform.OS === 'android' && Platform.Version >= 23) {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
            if (result) {
              console.log("Permission is OK");
            } else {
              PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                  console.log("User accept");
                } else {
                  console.log("User refuse");
                }
              });
            }
      });
    }


    this.state.loader = true;

     console.log(this.state.device_id)


        BleManager.connect(this.state.device_id).then(() => {

          this.setTimeout(() => {

            BleManager.retrieveServices(this.state.device_id).then((peripheralInfo) => {
              console.log(peripheralInfo);
              var service = '0000180d-0000-1000-8000-00805f9b34fb';
              var bakeCharacteristic = '00002a37-0000-1000-8000-00805f9b34fb';

              this.setTimeout(() => {
                BleManager.startNotification(this.state.device_id, service, bakeCharacteristic).then(() => {

                  this.startTimer()

                  let timeLeftVar = this.secondsToTime(this.state.seconds1);

                  this.setState({ time: timeLeftVar });


                  this.state.loader = true;
                  ToastAndroid.show('Connected.. Click on video to start your workout' , ToastAndroid.LONG);
                  console.log('Started notification on ' + this.state.device_id);
                  this.setState({bleDeviceConnected:true});

                }).catch((error) => {

                  this.state.loader = false;
                  ToastAndroid.show('Failed to connect, Make sure your device is turned on and try again' , ToastAndroid.LONG);
                  console.log('Notification error', error);
                });
              }, 1000);
            });

          }, 1000);


        }).catch((error) => {
          console.log('Connection error', error);

          this.state.loader = false;

          ToastAndroid.show('Make sure your heart rate sensor is nearby' , ToastAndroid.LONG);
        });

  }

  handleAppStateChange(nextAppState) {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
      BleManager.getConnectedPeripherals([]).then((peripheralsArray) => {
        console.log('Connected peripherals: ' + peripheralsArray.length);
      });
    }
    this.setState({appState: nextAppState});
  }

  componentWillUnmount() {
    this.handlerDiscover.remove();
    this.handlerStop.remove();
    this.handlerDisconnect.remove();
    this.handlerUpdate.remove();
  }

  handleDisconnectedPeripheral(data) {
    let peripherals = this.state.peripherals;
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      this.setState({peripherals});
    }
    console.log('Disconnected from ' + data.peripheral);
  }

  handleUpdateValueForCharacteristic(data) {
    //  console.log('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, data.value);
    if(this.state.exerciseStarted&&!this.state.paused){
      this.setState({heartrate: data.value[1]});
      this.savedata(data.value[1])
    }
    //var { calories } = this.props.navigation.state.params;
  }

  _getStudentDetails=(id)=>{
    getProfileDetails(id).then(response=>{
      console.log(response)
      if(response.status){
        this.setState({profile:response.data},()=>{
          this.setState({height:this.state.profile.height_in_feet, age:this.state.profile.age, weight:this.state.profile.current_weight,gender:this.state.profile.gender},()=>{

          });
        });
        console.log(this.state)
        this.setState({Loader:false, contentLoader:false})
      }else{
        this.setState({Loader:false, contentLoader:false})
        ToastAndroid.show(response.show, ToastAndroid.SHORT);
      }
    },err=>{
      console.log(err)
    })
  }

  savedata(data) {

    //console.log(calories)

    this.state.array[this.state.item] = data;

    if(this.state.item == 20)
    {
      this.setState(prevState=>({times:prevState.times+20}))
    for (var i = 0; i < this.state.array.length; i++) {
        this.state.sum += parseInt(this.state.array[i]);
        console.log('Value is ' + this.state.array.length);
    }

    if (this.state.array.length > 0) {

        this.state.average = Math.round(this.state.sum/this.state.array.length);

        if(this.state.age == null) { this.state.age = 25 }

        let calories = Math.round((0.4472 * this.state.average - 0.05741 * this.state.weight + 0.074 * this.state.age - 20.4022) * 0.333 / 4.184)

      //let calories = Math.round((-55.0969 + (0.6309 * this.state.average) + (0.1988 * (this.state.weight/2.2046) + (0.2017 * this.state.age))/4.184) * 60 * 0.005)

      if(calories < 0)
      {
          calories=0
      }

        this.state.calories = calories + this.state.calories;

        console.log(this.state.age)

        console.log(this.state.calories)
        if(this.state.exerciseStarted&&!this.state.paused){
          this._storeInDB();
        }
    }

    this.state.item = 0;

    this.state.array = [];

    this.state.sum = 0;

    }
    else {

    this.state.item = this.state.item + 1;

    }

  }


    _storeInDB(){

    var { user_id, exerciseID, workoutID, courseID, workout_day } = this.props.navigation.state.params.params;
    var { average, calories, times } = this.state;
    var data={
      "user_id":user_id,
      "avg_heart_rate" : average,
      "calories" : calories,
      "exercise_id": exerciseID,
      "course_id": courseID,
      "workout_id": workoutID,
      "workout_day": workout_day
    }
    var toSQL=[user_id, exerciseID, average, times];
    addToSQLiteTable({table_name:'sensor_data', table_data:toSQL}).then(response=>{
        console.log(response);
      },error=>{
        console.log(error);
      })
    addSensorData(data).then(response=>{
        console.log(response);
      },error=>{
        console.log(error);
      })
    }



  handleStopScan() {
    console.log('Scan is stopped');
    this.setState({ scanning: false });
  }

  startScan() {
    if (!this.state.scanning) {
      BleManager.scan([], 30, true).then((results) => {
        console.log('Scanning...');
        ToastAndroid.show('Scanning....' , ToastAndroid.SHORT);
        this.setState({scanning:true});
      }).catch((error) => {
          console.log('Connection error', error);
          ToastAndroid.show('Connection error.... Please Try To Reconnect' , ToastAndroid.SHORT);
      });
    }
  }

  handleDiscoverPeripheral(peripheral){
    var peripherals = this.state.peripherals;
    if (!peripherals.has(peripheral.id)){
      console.log('Got ble peripheral', peripheral);


      peripherals.set(peripheral.id, peripheral);
      this.setState({ peripherals })
    }
  }


  loadStart() {
          //ToastAndroid.show('Loading Video Please Wait...' , ToastAndroid.SHORT);
  }

  _sendStatus(statusvalue, cal, min, sec){
    this.setState({paused:true})
    if(statusvalue == 'completed') { this.setState({loader:true}) }

    var { exerciseID, user_id, workoutID, courseID,  workout_day, workout_title  } = this.props.navigation.state.params.params;
      let temp={
        "user_id":user_id,
        "exercise_id" : exerciseID,
        "workout_id" :workoutID,
        "course_id":courseID,
        "workout_day":workout_day,
        "exercise_status" : statusvalue
      };

      sendStatus(temp).then(res=>{

      BleManager.disconnect(this.state.device_id);

      console.log('disconnect')

      const { navigate  } = this.props.navigation;

      console.log(time)

      var time = min+':'+sec

      navigate("WorkoutSummary", { workoutID: workoutID, calories: cal, time: time,  workout_title: workout_title, courseID: courseID, workout_day: workout_day , user_id:user_id, exerciseID: exerciseID });

    },
      err=>{
        console.log(err);
      })
    }

  render() {
    const flexCompleted = this.getCurrentTimePercentage() * 100;
    const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;
    var { exerciseID, streaming_uri, user_id, workoutID, courseID,  workout_day, workout_title  } = this.props.navigation.state.params.params;
    var { bleDeviceConnected, exerciseStarted } = this.state;
    return (

      <View>

      <View style={styles.container}>


        <TouchableOpacity onPress={this._playpause}>
          <Video
            ref={(ref: Video) => { this.video = ref }}
            /* For ExoPlayer */
            /* source={{ uri: 'http://www.youtube.com/api/manifest/dash/id/bf5bb2419360daf1/source/youtube?as=fmp4_audio_clear,fmp4_sd_hd_clear&sparams=ip,ipbits,expire,source,id,as&ip=0.0.0.0&ipbits=0&expire=19000000000&signature=51AF5F39AB0CEC3E5497CD9C900EBFEAECCCB5C7.8506521BFC350652163895D4C26DEE124209AA9E&key=ik0', type: 'mpd' }} */
            source={{uri: streaming_uri}}
            style={styles.fullScreen1}
            rate={this.state.rate}
            paused={this.state.paused}
            volume={this.state.volume}
            muted={this.state.muted}
            resizeMode="cover"
            onLoad={this.onLoad}
            onProgress={this.onProgress}
            onEnd={this.onEnd}
            onLoadStart={this.loadStart}
            onAudioBecomingNoisy={this.onAudioBecomingNoisy}
            onAudioFocusChanged={this.onAudioFocusChanged}
          />
          </TouchableOpacity>

        <View style={styles.controls1}>
          <TouchableOpacity onPress={this._playpause}>
            {this.state.paused?<View><FontAwesome name="play" size={35} color="#FF7E00"   /></View>:<View><FontAwesome name="pause" size={35} color="#FF7E00"   /></View>}
          </TouchableOpacity>
        </View>

        <View style={styles.controls}>
          <View style={styles.generalControls}>
            <View style={styles.rateControl}>
              <Text style={{textAlign: 'center', fontSize: 35, fontWeight: 'bold', color: 'orange'}}>{this.state.heartrate}</Text>
            </View>

            <View style={styles.resizeModeControl}>
              <Text style={{textAlign: 'center', fontSize: 35, fontWeight: 'bold', color: 'orange'}}>{this.state.timevalue}</Text>
            </View>

          </View>

          <View style={styles.trackingControls}>
            <View style={styles.progress}>
              <View style={[styles.innerProgressCompleted, { flex: flexCompleted }]} />
              <View style={[styles.innerProgressRemaining, { flex: flexRemaining }]} />
            </View>
          </View>
        </View>



      </View>



      {bleDeviceConnected?
        exerciseStarted?
        <View style={styles.loginButton}>
           <TouchableHighlight onPress={() => this._sendStatus('completed', this.state.calories, this.state.time.m, this.state.time.s) }>
              <Text style={{textAlign:'center'}}>Complete Workout</Text>
           </TouchableHighlight>
        </View>
        :<View style={styles.loginButton}>
           <TouchableHighlight onPress={() => this.setState({exerciseStarted:true, paused:false}) }>
              <Text style={{textAlign:'center'}}>Start Exercise</Text>
           </TouchableHighlight>
        </View>
        :
        <View style={styles.loginButton}>
           <TouchableHighlight onPress={() => this.bleConnect() }>
              <Text style={{textAlign:'center'}}>Connect device</Text>
           </TouchableHighlight>
        </View>}

      </View>
    );
  }
}
reactMixin(VideoExcierce.prototype, TimerMixin);

const styles = StyleSheet.create({
  container: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      position: 'relative',
    },
    fullScreen1: {

      width: width-10,
      height: 200,


      alignItems:'center'
    },
    fullScreen: {
      backgroundColor:'#ebebe0',
      width: width-10,
      height:200,

      alignItems:'center'
    },
    controls: {
      backgroundColor: 'transparent',
      borderRadius: 5,
      position: 'absolute',
      bottom: 20,
      left: 20,
      right: 20,
    },
    controls1: {
      backgroundColor: 'transparent',
      borderRadius: 5,
      position: 'absolute',
      top:70,
      left: width/2,

    },

    progress: {
      flex: 1,
      flexDirection: 'row',
      borderRadius: 3,
      overflow: 'hidden',
    },
    innerProgressCompleted: {
      height: 20,
      backgroundColor: '#cccccc',
    },
    innerProgressRemaining: {
      height: 20,
      backgroundColor: '#2C2C2C',
    },
    generalControls: {
      flex: 1,
      flexDirection: 'row',
      borderRadius: 4,
      overflow: 'hidden',
      paddingBottom: 10,
    },
    rateControl: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
    },
    volumeControl: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
    },
    resizeModeControl: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    },
    controlOption: {
      alignSelf: 'center',
      fontSize: 11,
      color: 'white',
      paddingLeft: 2,
      paddingRight: 2,
      lineHeight: 12,
    },
      loginButton: {
       bottom: 0,
       height:60,
       backgroundColor:'#fff',
       justifyContent:'center',
       width: '100%',
       marginTop: 300
     }
});
