import React, { Component } from 'react';
import { AppRegistry, TextInput, FlatList, StyleSheet,ScrollView, Text, View, TouchableHighlight, Animated , Picker, TouchableOpacity, Image, ToastAndroid } from 'react-native';
import { sendActivitylog } from '../../template/Workout/workout.js'
import DatePicker from 'react-native-datepicker'
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination,ParallaxImage } from 'react-native-snap-carousel';
import { onSignIn, isSignedIn, onSignOut, getAllAsyncStroage } from '../../../config/auth';
import { AsyncStorage } from "react-native";
import { NavigationActions } from 'react-navigation';
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import moment from 'moment'

import SplashScreen from 'react-native-smart-splash-screen'

import styles from '../Tabs/style.js'

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;
export default class LogActivity extends Component {

  constructor(props) {
    super(props);
    this.state={
      workoutdata: "", date:"", time:"00:00:00", sport:"",title:"",user_id:"",heartrate:"",avg_heart_rate:[],fromScreen:'',activity_type_visibility:true
    }


}


  componentWillMount(){

    var { time , date, heart_rate, avg_heart_rate, fromScreen, calories, completedtime  } = this.props.navigation.state.params;

    this.state.date = date

    this.state.time = time

    this.state.heartrate = heart_rate

    this.state.avg_heart_rate = avg_heart_rate

    this.state.fromScreen = fromScreen

    this.state.calories = calories

    console.log(avg_heart_rate)

  }


  previousPage() {
    const { navigate  } = this.props.navigation;
    navigate("UpcomingWorkouts");
  }

  async nextpage() {

    const { navigate  } = this.props.navigation;
    const { title, sport, time, date, user_id, calories, avg_heart_rate, completedtime } = this.state;

    let USER_ID = await AsyncStorage.getItem("USER_ID");

    console.log(USER_ID)

    if(title != ''&sport!=''&&time!=''&date!=''&completedtime!='')
    {
    console.log(this.state)

    todaydate = new Date().toISOString();

    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

    completed_date = date.split(' ')

    for (var i = 0; i < months.length; i++) {
        if (completed_date[2] == months[i]) {
            completed_month = i + 1
        }
    }


    completed_year = completed_date[3]
    completed_day = completed_date[1].slice(0, -3)

    workout_date = completed_month + '/' + completed_day + '/' + completed_year + ' ' + completedtime

    workout_date_time = new Date(workout_date).toISOString();

    console.log(workout_date_time)


    var temp={};
    temp.title=title;
    temp.activity_type=sport;
    temp.workout_id="0";
    temp.course_id="0";
    temp.user_id=USER_ID;
    temp.exercise_id="0";
    temp.completed_at=workout_date_time;
    temp.completed_on=new Date().toISOString();
    temp.distance=0;
    temp.duration=time;
    this.setState({loader:true})

    console.log(temp)

    ToastAndroid.show('Activity Details Updated Successfully' , ToastAndroid.SHORT);

    sendActivitylog(title, '0', sport, workout_date_time, todaydate, USER_ID, avg_heart_rate ).then(response=>{
        console.log(response);

        const { navigate  } = this.props.navigation;

        if(this.state.fromScreen === 'Trackactivity')
        {
          navigate("TrackActivitySummary", { calories:calories, time: time, avg_heart_rate: avg_heart_rate, workout_title: title});
        }else {
          navigate("FinishExcierce");
        }


      },error=>{
        console.log(error);
      })

  }
  else
  {
    ToastAndroid.show('Please Fill all the fields...' , ToastAndroid.SHORT);
  }

}
render() {

  todaydate = new Date().toISOString();

  return (
      <View style={styles.mainBody} >
        <ScrollView
              style={{flex:1}}
              contentContainerStyle={{paddingBottom: 50}}
              indicatorStyle={'white'}
              scrollEventThrottle={200}
              directionalLockEnabled={true}
            >
              <View style={styles.listofWrkouts1}>
                <View style={styles.chevron_left_icon}>
                  <TouchableOpacity onPress = {()=>this.previousPage()}>
                      <FontAwesome name="chevron-left" size={25} color="#FF7E00"   />
                  </TouchableOpacity>
                </View>

                <View style={styles.header}>
                      <Text style={styles.topSignupTxt}>
                        Activity
                      </Text>
                </View>

                <View>
                  <Text style = {styles.text_workout_heading}>
                    Log your activity
                  </Text>
                  <Text style = {styles.text_workout_sub_heading}>
                    ADO YOUR SPORT. TIME & RATE TO TRACK YOUR RESULTS
                  </Text>
                </View>


              </View>
              <View style={styles.feed_img_mas2a}>
                <Text style={styles.sport1}>Title :</Text>
                <View style={styles.log_act1}>

                <TextInput
                  placeholder="Title"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={String(this.state.title)}
                  placeholderTextColor='#626264'
                  style={styles.textInput_signup}
                  onChangeText={ (text) => this.setState({title:text, enable:true}) }
                />

                </View>
              </View>
              <View style={styles.feed_img_mas2a}>
                <Text style={styles.sport1}>Calories :</Text>
                <View style={styles.log_act1}>

                <TextInput
                  placeholder="Calories"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  keyboardType='numeric'
                  value={String(this.state.calories)}
                  placeholderTextColor='#626264'
                  style={styles.textInput_signup}
                  onChangeText={ (text) => this.setState({calories:text, enable:true}) }
                />

                </View>
              </View>
              <View style={styles.feed_img_mas2a}>
                <Text style={styles.sport1}>Activity Type :</Text>
                <View style={styles.log_act1}>

                <Picker style={styles.textInput_signup}
                  selectedValue={this.state.sport}
                  onValueChange={(itemValue, itemIndex) => this.setState({sport: itemValue,activity_type_visibility:false})}>
                  <Picker.Item label="SELECT FROM LIST" value="" />
                  <Picker.Item label="CrossFit" value="CrossFit" />
                  <Picker.Item label="Running" value="Running" />
                  <Picker.Item label="Weights" value="Weights" />
                  <Picker.Item label="Cycling" value="Cycling" />
                  <Picker.Item label="Yoga" value="Yoga" />
                  <Picker.Item label="Swmming" value="Swmming" />
                  <Picker.Item label="Aerobics" value="Aerobics" />
                  <Picker.Item label="Footbal" value="Footbal" />
                </Picker>

                </View>
              </View>


              <View style={styles.feed_img_mas2a}>
                <Text style={styles.sport1}>Complted Date :</Text>
                <View style={styles.log_act1}>

                <DatePicker
                  style={styles.textInput_signup}
                  date={this.state.date}
                  mode="date"
                  placeholder="select date"
                  format="dddd Do, MMMM YYYY"
                  minDate="2017-05-01"
                  maxDate={todaydate}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  showIcon={false}
                  onDateChange={(date) => {this.setState({date: date})}}
                />

                </View>
              </View>

              <View style={styles.feed_img_mas2a}>
                <Text style={styles.sport1}>Complted Time :</Text>
                <View style={styles.log_act1}>

                <DatePicker
                  style={styles.textInput_signup}
                  date={this.state.completedtime}
                  mode="time"
                  placeholder="select date"
                  format="HH:MM"
                  showIcon={false}
                  onDateChange={(time) => {this.setState({completedtime: time})}}
                />

                </View>
              </View>

              <View style={styles.feed_img_mas2a}>
                <Text style={styles.sport1}>Duration :</Text>
                <View style={styles.log_act1}>

                <DatePicker
                  style={styles.textInput_signup}
                  date={this.state.time}
                  mode="time"
                  format="HH:mm:ss"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  minuteInterval={10}
                  showIcon={false}
                  onDateChange={(time) => {this.setState({time: time});}}
                />

                </View>
              </View>


            </ScrollView>
            <View style={styles.footer}>

                <TouchableOpacity onPress = {()=>this.nextpage()}>
                  <View style={styles.save_view}>
                      <Text style={styles.save_btnTxt}>Complete Workout</Text>
                  </View>

                  <Image
                    style={styles.save_btnImg}
                    source={{ uri: 'buttonimg' }}
                  />

                </TouchableOpacity>
              </View>
            </View>
    );
}
}
